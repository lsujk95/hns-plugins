-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Czas generowania: 15 Lut 2019, 15:23
-- Wersja serwera: 10.1.26-MariaDB-0+deb9u1
-- Wersja PHP: 7.2.12-1+0~20181112102304.11+stretch~1.gbp55f215

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `ICHideNSeek`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hns_records`
--

CREATE TABLE `hns_records` (
  `STEAM_ID` varchar(32) NOT NULL,
  `NICKNAME` varchar(32) NOT NULL,
  `TYPE` int(11) NOT NULL,
  `BLOCK` int(11) NOT NULL,
  `DISTANCE` float NOT NULL,
  `STRAFES` int(11) NOT NULL,
  `PRE` float NOT NULL,
  `MAX` float NOT NULL,
  `HEIGHT` float NOT NULL,
  `SYNC` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `hns_settings`
--

CREATE TABLE `hns_settings` (
  `STEAM_ID` varchar(32) CHARACTER SET utf8 NOT NULL,
  `JUMP_BEAM` int(11) NOT NULL,
  `COLOR_CHAT` int(11) NOT NULL,
  `QUAKE_SOUNDS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `hns_records`
--
ALTER TABLE `hns_records`
  ADD PRIMARY KEY (`STEAM_ID`,`TYPE`);

--
-- Indexes for table `hns_settings`
--
ALTER TABLE `hns_settings`
  ADD PRIMARY KEY (`STEAM_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
