#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <player>
#include <validator>

#pragma semicolon 			    1
#pragma tabsize 			      0
#pragma newdecls required

/* -------------------------- */
/* ------- Constants -------- */
/* -------------------------- */

#define MAX_NAME 32

#define WHITE 0x01
#define DARKRED 0x02
#define PURPLE 0x03
#define GREEN 0x04
#define MOSSGREEN 0x05
#define LIMEGREEN 0x06
#define RED 0x07
#define GRAY 0x08
#define YELLOW 0x09
#define ORANGE 0x10
#define DARKGREY 0x0A
#define BLUE 0x0B
#define DARKBLUE 0x0C
#define LIGHTBLUE 0x0D
#define PINK 0x0E
#define LIGHTRED 0x0F
#define PERCENT 0x25

#define JUMPSTATS_NONE 	0
#define JUMPSTATS_LADDER 1
#define JUMPSTATS_LJ 2
#define JUMPSTATS_BLOCKLJ 3
#define JUMPSTATS_MULTIBH 4
#define JUMPSTATS_CJ 5
#define JUMPSTATS_WJ 6
#define JUMPSTATS_DROPBH 7
#define JUMPSTATS_BHOP 8
#define JUMPSTATS_JUMPBUG 9

#define MENU_NONE 0
#define MENU_TOP_CATEGORIES 1
#define MENU_TOP 2
#define MENU_PLAYERS 3
#define MENU_RANK 4

/* -------------------------- */
/* -------- Globals --------- */
/* -------------------------- */
// Database
Database PluginDatabase;

// Server
int g_Server_Tickrate;
int g_Beam[3];

int BeamSprite        = -1;

// Sounds
char CP_FULL_SOUND_PATH[128] = "sound/quake/holyshit_new.mp3";
char CP_RELATIVE_SOUND_PATH[128] = "*/quake/holyshit_new.mp3";

char PRO_FULL_SOUND_PATH[128] = "sound/quake/wickedsick_new.mp3";
char PRO_RELATIVE_SOUND_PATH[128] = "*/quake/wickedsick_new.mp3";

char UNSTOPPABLE_SOUND_PATH[128] = "sound/quake/unstoppable.mp3";
char UNSTOPPABLE_RELATIVE_SOUND_PATH[128] = "*/quake/unstoppable.mp3";

char GODLIKE_RAMPAGE_FULL_SOUND_PATH[128] = "sound/quake/rampage.mp3";
char GODLIKE_RAMPAGE_RELATIVE_SOUND_PATH[128] = "*/quake/rampage.mp3";

char GODLIKE_DOMINATING_FULL_SOUND_PATH[128] = "sound/quake/dominating.mp3";
char GODLIKE_DOMINATING_RELATIVE_SOUND_PATH[128] = "*/quake/dominating.mp3";

char PERFECT_FULL_SOUND_PATH[128] = "sound/quake/perfect.mp3";
char PERFECT_RELATIVE_SOUND_PATH[128] = "*/quake/perfect.mp3";

char IMPRESSIVE_FULL_SOUND_PATH[128] = "sound/quake/impressive_kz.mp3";
char IMPRESSIVE_RELATIVE_SOUND_PATH[128] = "*/quake/impressive_kz.mp3";

char GODLIKE_FULL_SOUND_PATH[128] = "sound/quake/godlike.mp3";
char GODLIKE_RELATIVE_SOUND_PATH[128] = "*/quake/godlike.mp3";

char GOLDEN_FULL_SOUND_PATH[128] = "sound/quake/ownage.mp3";
char GOLDEN_RELATIVE_SOUND_PATH[128] = "*/quake/ownage.mp3";

char W_GODLIKE_FULL_SOUND_PATH[128] = "sound/quake/woman/godlike.mp3";
char W_GODLIKE_RELATIVE_SOUND_PATH[128] = "*/quake/woman/godlike.mp3";

char W_HOLYSHIT_FULL_SOUND_PATH[128] = "sound/quake/woman/holyshit.mp3";
char W_HOLYSHIT_RELATIVE_SOUND_PATH[128] = "*/quake/woman/holyshit.mp3";

char OWNAGE3_FULL_SOUND_PATH[128] = "sound/quake/ownage3.mp3";
char OWNAGE3_RELATIVE_SOUND_PATH[128] = "*/quake/ownage3.mp3";

// Server Setup (TODO - move to convars)
float g_fBhopSpeedCap = 300.0; // Speed cap setup
float g_fMaxBhopPreSpeed = 300.0; // Max bunny hop pre

// Server Setup - Jumpstats
float g_dist_min_ladder = 100.0;
float g_dist_perfect_ladder = 155.0;
float g_dist_impressive_ladder = 165.0;
float g_dist_god_ladder = 175.0;
float g_hdist_golden_ladder = 185.0;
float g_hdist_orange_ladder = 190.0;

float g_dist_min_lj = 250.0;
float g_dist_perfect_lj = 265.0;
float g_dist_impressive_lj = 270.0;
float g_dist_god_lj = 275.0;
float g_hdist_golden_lj = 280.0;
float g_hdist_orange_lj = 285.0;

float g_dist_min_multibhop = 250.0;
float g_dist_perfect_multibhop = 260.0;
float g_dist_impressive_multibhop = 270.0;
float g_dist_god_multibhop = 280.0;
float g_hdist_golden_multibhop = 295.0;
float g_hdist_orange_multibhop = 300.0;

float g_dist_min_countjump = 250.0;
float g_dist_perfect_countjump = 260.0;
float g_dist_impressive_countjump = 270.0;
float g_dist_god_countjump = 280.0;
float g_hdist_golden_countjump = 295.0;
float g_hdist_orange_countjump = 300.0;

float g_dist_min_weird = 250.0;
float g_dist_perfect_weird = 270.0;
float g_dist_impressive_weird = 280.0;
float g_dist_god_weird = 290.0;
float g_hdist_golden_weird = 295.0;
float g_hdist_orange_weird = 300.0;

float g_dist_min_dropbhop = 250.0;
float g_dist_perfect_dropbhop = 260.0;
float g_dist_impressive_dropbhop = 270.0;
float g_dist_god_dropbhop = 280.0;
float g_hdist_golden_dropbhop = 295.0;
float g_hdist_orange_dropbhop = 300.0;

float g_dist_min_bhop = 250.0;
float g_dist_perfect_bhop = 260.0;
float g_dist_impressive_bhop = 270.0;
float g_dist_god_bhop = 280.0;
float g_hdist_golden_bhop = 295.0;
float g_hdist_orange_bhop = 300.0;

float g_dist_min_jbug = 250.0;
float g_dist_god_jbug = 500.0;

// Jumpstats - Personal
float g_js_fPersonal_CJ_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_Wj_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_DropBhop_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_Bhop_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_MultiBhop_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_Lj_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_LjBlockRecord_Dist[MAXPLAYERS] = -1.0;
float g_js_fPersonal_LadderJump_Record[MAXPLAYERS] = -1.0;
float g_js_fPersonal_JumpBug_Record[MAXPLAYERS] = -1.0;
int g_js_Personal_LjBlock_Record[MAXPLAYERS] = -1;

// Client Setup 
/**
false - Disabled
true  - Enabled
*/
bool g_bJumpBeam[MAXPLAYERS];

/**
0 - Disabled
1 - Enabled
2 - Godlikes & Records only
3 - None (except yours)
*/
int g_ColorChat[MAXPLAYERS];

/**
0 - Disabled
1 - Enabled
2 - Godlikes & Records only
3 - Records only
*/
int g_EnableQuakeSounds[MAXPLAYERS];

/**
0 - Disabled
1 - Enabled
*/
int g_bCenterPanel[MAXPLAYERS];

/**
0 - Disabled
1 - Enabled
*/
bool g_bViewWeaponModels[MAXPLAYERS];

/**
0 - Disabled
1 - Enabled
*/
bool g_bEnableSpecList[MAXPLAYERS];

//
int g_LastUpdateTime = 0;

// States
bool g_bOnGround[MAXPLAYERS];
bool g_bKickStatus[MAXPLAYERS];
bool g_bBeam[MAXPLAYERS];
bool g_bOnBhopPlattform[MAXPLAYERS];

int g_js_GroundFrames[MAXPLAYERS];
int g_js_TotalGroundFrames[MAXPLAYERS];
int g_js_DuckCounter[MAXPLAYERS];

float g_fSpawnTime[MAXPLAYERS];
float g_fAirTime[MAXPLAYERS];
float g_fJumpOffTime[MAXPLAYERS];

float g_fLandingTime[MAXPLAYERS];

// Last States
MoveType g_LastMoveType[MAXPLAYERS];
bool g_bLastOnGround[MAXPLAYERS];

float g_vLast[MAXPLAYERS][3];
float g_vCurrent[MAXPLAYERS][3];

float g_fLastSpeed[MAXPLAYERS];
float g_fLastAngles[MAXPLAYERS][3];
float g_fLastPosition[MAXPLAYERS][3];

float g_fLastPositionOnGround[MAXPLAYERS][3];
float g_fLastTimeDoubleDucked[MAXPLAYERS];

// Menus
int g_LastMenu[MAXPLAYERS];
int g_LastTopMenuType[MAXPLAYERS];
char g_LastStatsTarget[MAXPLAYERS][32];

// Slide
float g_fSlideInitSpeed[MAXPLAYERS];
float g_fSlideVelocity[MAXPLAYERS][3];
bool g_bIsDucking[MAXPLAYERS];
bool g_bResetNextSlide[MAXPLAYERS];

// Prestrafe
int g_PrestrafeFrameCounter[MAXPLAYERS];

float g_js_fPreStrafe[MAXPLAYERS];
float g_PrestrafeVelocity[MAXPLAYERS];
float g_fVelocityModifierLastChange[MAXPLAYERS];

// Buttons
int g_CurrentButton[MAXPLAYERS];
int g_LastButton[MAXPLAYERS];

float g_fCrouchButtonLastTimeUsed[MAXPLAYERS];
float g_fJumpButtonLastTimeUsed[MAXPLAYERS];

// Jumps
bool g_js_bPlayerJumped[MAXPLAYERS];
bool g_js_bPerfJumpOff[MAXPLAYERS];
bool g_js_bPerfJumpOff2[MAXPLAYERS];

bool g_bLastButtonJump[MAXPLAYERS];

bool g_bJumpBugged[MAXPLAYERS];
int g_JumpCheck2[MAXPLAYERS];

// Distances
char g_js_szLastJumpDistance[MAXPLAYERS][256];

float g_js_fJump_DistanceX[MAXPLAYERS];
float g_js_fJump_DistanceZ[MAXPLAYERS];
float g_js_fJump_Distance[MAXPLAYERS];

// Jumpstats
bool g_js_bInvalidGround[MAXPLAYERS];
bool g_bLastInvalidGround[MAXPLAYERS];

bool g_bCountJump[MAXPLAYERS];
bool g_js_bFuncMoveLinear[MAXPLAYERS];
bool g_js_bBhop[MAXPLAYERS];
bool g_js_bDropJump[MAXPLAYERS];

float g_js_fMax_Speed[MAXPLAYERS];
float g_js_fMax_Height[MAXPLAYERS];

float g_js_fJump_JumpOff_Pos[MAXPLAYERS][3];
float g_js_fJump_Landing_Pos[MAXPLAYERS][3];
float g_js_fJump_JumpOff_PosLastHeight[MAXPLAYERS];

float g_fMovingDirection[MAXPLAYERS];
float g_js_fDropped_Units[MAXPLAYERS];
float g_fFailedLandingPos[MAXPLAYERS][3];

float g_fLastJump[MAXPLAYERS] = {0.0, ...};

// Post
float g_js_fMax_Speed_Final[MAXPLAYERS];
float g_flastHeight[MAXPLAYERS];
int g_js_Last_Ground_Frames[MAXPLAYERS];

// Jumpstats - Godlike
int g_js_GODLIKE_Count[MAXPLAYERS];

// Jumpstats - Block
bool g_js_block_lj_valid[MAXPLAYERS];
bool g_js_block_lj_jumpoff_pos[MAXPLAYERS];
bool g_bLJBlock[MAXPLAYERS];

int g_BlockDist[MAXPLAYERS];
float g_fBlockHeight[MAXPLAYERS];
float g_fEdgeDistJumpOff[MAXPLAYERS];
float g_fOriginBlock[MAXPLAYERS][2][3];
float g_fDestBlock[MAXPLAYERS][2][3];

float g_fEdgeVector[MAXPLAYERS][3];
float g_fEdgePoint1[MAXPLAYERS][3];
float g_fEdgePoint2[MAXPLAYERS][3];

// Jumpstats - Multi BH
int g_js_MultiBhop_Count[MAXPLAYERS];

// Strafes
int g_js_StrafeCount[MAXPLAYERS];
int g_js_Strafe_Frames[MAXPLAYERS][25];
int g_js_Strafes_Final[MAXPLAYERS];
int g_js_Sync_Final[MAXPLAYERS];

bool g_js_Strafing_AW[MAXPLAYERS];
bool g_js_Strafing_SD[MAXPLAYERS];
bool g_bStrafeSync[MAXPLAYERS];

float g_js_Good_Sync_Frames[MAXPLAYERS];
float g_js_Sync_Frames[MAXPLAYERS];

float g_js_Strafe_Good_Sync[MAXPLAYERS][25];
float g_js_Strafe_Air_Time[MAXPLAYERS][25];
float g_js_Strafe_Max_Speed[MAXPLAYERS][25];
float g_js_Strafe_Gained[MAXPLAYERS][25];
float g_js_Strafe_Lost[MAXPLAYERS][25];

// Ladder
float g_js_AvgLadderSpeed[MAXPLAYERS];
int g_js_LadderFrames[MAXPLAYERS];
bool g_bLadderJump[MAXPLAYERS];
int g_js_LadderDirectionCounter[MAXPLAYERS];
float g_js_fLadderDirection[MAXPLAYERS];

// Measure
bool g_bMeasurePosSet[MAXPLAYERS][2];
float g_fvMeasurePos[MAXPLAYERS][2][3];
Handle g_hP2PRed[MAXPLAYERS] = { INVALID_HANDLE,... };
Handle g_hP2PGreen[MAXPLAYERS] = { INVALID_HANDLE,... };

// Teleport
float g_fTeleportValidationTime[MAXPLAYERS];

// Anty Cheat
int g_fps_max[MAXPLAYERS];
int g_aiJumps[MAXPLAYERS] 					= {0, ...};
int g_aaiLastJumps[MAXPLAYERS][30];
int g_NumberJumpsAbove[MAXPLAYERS];

float g_fafAvgPerfJumps[MAXPLAYERS] = {0.3333, ...};
float g_fafAvgSpeed[MAXPLAYERS] 		= {250.0, ...};
float g_fafAvgJumps[MAXPLAYERS] 		= {1.0, ...};

// Booster
bool g_bTouchedBooster[MAXPLAYERS];

// Jumpbug
// bool g_bHasJumpBugStarted[MAXPLAYER];
bool g_bJumpHasBugged[MAXPLAYERS];

int g_JumpBugCooldown;
bool g_PlayerJumpbugCooldown[MAXPLAYERS];

float g_fStartGroundHeight[MAXPLAYERS];
float g_fEndGroundHeight[MAXPLAYERS];

int g_StartHealth[MAXPLAYERS];
int g_EndHealth[MAXPLAYERS];

float g_fHighestSpeed[MAXPLAYERS];
float g_fLastBugSpeed[MAXPLAYERS];

// Funny Jumping
int g_PlayerSavingCount[MAXPLAYERS];

float g_fPlayerSavedPosition[MAXPLAYERS][3];
float g_fPrevPlayerSavedPosition[MAXPLAYERS][3];
float g_fPlayerSavedAngle[MAXPLAYERS][3];
float g_fPrevPlayerSavedAngle[MAXPLAYERS][3];

int g_fPlayerSavedTeam[MAXPLAYERS];

// Redie
bool g_bPlayerGhostMode[MAXPLAYERS];

/* --------------------------- */
/* -------- Logs ---------- */
/* --------------------------- */
char JumpstatsLogFile[256];
char AntiCheatLogFile[256];
char DatabaseLogFile[256];

/* --------------------------- */
/* -------- Modules ---------- */
/* --------------------------- */
#include "hns_timer/hooks.sp"
#include "hns_timer/misc.sp"
#include "hns_timer/timer.sp"
#include "hns_timer/database.sp"
#include "hns_timer/jumpstats.sp"
#include "hns_timer/menus.sp"
#include "hns_timer/commands.sp"

/* ---------------------------- */
/* ---------- Info ------------ */
/* ---------------------------- */
public Plugin myinfo =
{
	name = "HNS Timer",
	author = "UNFORGE LTD",
	description = "HNS Timer",
	version = "0.1",
	url = "http://unforge.net/" 
};

/* ---------------------------- */
/* ---------- Main ------------ */
/* ---------------------------- */
public void OnPluginStart()
{
	// Validate Server Files
	// Validate();

	// Translations
	LoadTranslations("hns_timer.phrases");

    // Logs
	char Time[64];
	char PathFormat[64];
	FormatTime(Time, sizeof(Time), "%Y%m%d", GetTime());
	
	Format(PathFormat, sizeof(PathFormat), "logs/JumpStats_%s.log", Time);
	BuildPath(Path_SM, JumpstatsLogFile, sizeof(JumpstatsLogFile), PathFormat);
	
	Format(PathFormat, sizeof(PathFormat), "logs/AntiCheat_%s.log", Time);
	BuildPath(Path_SM, AntiCheatLogFile, sizeof(AntiCheatLogFile), PathFormat);

	Format(PathFormat, sizeof(PathFormat), "logs/Database_%s.log", Time);
	BuildPath(Path_SM, DatabaseLogFile, sizeof(DatabaseLogFile), PathFormat);

	LogToFile(AntiCheatLogFile, "[PLUGIN] -- Running Server --");
	LogToFile(JumpstatsLogFile, "[PLUGIN] -- Running Server --");
	LogToFile(DatabaseLogFile, "[PLUGIN] -- Running Server --");
	
	// Set server trickrate
	float CurrTickrate = 1.0 / GetTickInterval();
	if (CurrTickrate > 65)
	{
		if (CurrTickrate < 103)
		{
			g_Server_Tickrate = 102;
		}
		else
		{
			g_Server_Tickrate = 128;
		}
	}
	else
	{
		g_Server_Tickrate= 64;
	}

	// Init modules
	InitHooks();
	InitMisc();
	InitTimer();
	InitDatabase();
	InitJumpstats();
	InitMenus();
	InitCommands();

	// Lately load
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			LoadPlayer(client);
		}
	}
}

public void OnMapStart()
{
	// Precache Beam
	AddFileToDownloadsTable("materials/sprites/bluelaser1.vmt");
	AddFileToDownloadsTable("materials/sprites/bluelaser1.vtf");
	AddFileToDownloadsTable("materials/sprites/laser.vmt");
	AddFileToDownloadsTable("materials/sprites/laser.vtf");
	AddFileToDownloadsTable("materials/sprites/halo01.vmt");
	AddFileToDownloadsTable("materials/sprites/halo01.vtf");

	BeamSprite = PrecacheModel("sprites/laserbeam.vmt");

	g_Beam[0] = PrecacheModel("materials/sprites/laser.vmt", true);
	g_Beam[1] = PrecacheModel("materials/sprites/halo01.vmt", true);
	g_Beam[2] = PrecacheModel("materials/sprites/bluelaser1.vmt", true);

	// Precache Sounds
	AddFileToDownloadsTable(UNSTOPPABLE_SOUND_PATH);
	FakePrecacheSound(UNSTOPPABLE_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", UNSTOPPABLE_SOUND_PATH);

	AddFileToDownloadsTable(PRO_FULL_SOUND_PATH);
	FakePrecacheSound(PRO_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", PRO_FULL_SOUND_PATH);

	AddFileToDownloadsTable(CP_FULL_SOUND_PATH);
	FakePrecacheSound(CP_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", CP_FULL_SOUND_PATH);

	AddFileToDownloadsTable(PRO_FULL_SOUND_PATH);
	FakePrecacheSound(PRO_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", PRO_FULL_SOUND_PATH);

	AddFileToDownloadsTable(GODLIKE_FULL_SOUND_PATH);
	FakePrecacheSound(GODLIKE_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", GODLIKE_FULL_SOUND_PATH);

	AddFileToDownloadsTable(GODLIKE_DOMINATING_FULL_SOUND_PATH);
	FakePrecacheSound(GODLIKE_DOMINATING_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", GODLIKE_DOMINATING_FULL_SOUND_PATH);

	AddFileToDownloadsTable(GODLIKE_RAMPAGE_FULL_SOUND_PATH);
	FakePrecacheSound(GODLIKE_RAMPAGE_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", GODLIKE_RAMPAGE_FULL_SOUND_PATH);

	AddFileToDownloadsTable(PERFECT_FULL_SOUND_PATH);
	FakePrecacheSound(PERFECT_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", PERFECT_FULL_SOUND_PATH);

	AddFileToDownloadsTable(IMPRESSIVE_FULL_SOUND_PATH);
	FakePrecacheSound(IMPRESSIVE_RELATIVE_SOUND_PATH);
	PrintToServer("Precaching %s sound.", IMPRESSIVE_FULL_SOUND_PATH);

	AddFileToDownloadsTable(GOLDEN_FULL_SOUND_PATH);
	FakePrecacheSound(GOLDEN_RELATIVE_SOUND_PATH); 
	PrintToServer("Precaching %s sound.", GOLDEN_FULL_SOUND_PATH);

	AddFileToDownloadsTable(W_GODLIKE_FULL_SOUND_PATH);
	FakePrecacheSound(W_GODLIKE_RELATIVE_SOUND_PATH); 
	PrintToServer("Precaching %s sound.", W_GODLIKE_FULL_SOUND_PATH);

	AddFileToDownloadsTable(W_HOLYSHIT_FULL_SOUND_PATH);
	FakePrecacheSound(W_HOLYSHIT_RELATIVE_SOUND_PATH); 
	PrintToServer("Precaching %s sound.", W_HOLYSHIT_FULL_SOUND_PATH);

	AddFileToDownloadsTable(OWNAGE3_FULL_SOUND_PATH);
	FakePrecacheSound(OWNAGE3_RELATIVE_SOUND_PATH); 
	PrintToServer("Precaching %s sound.", GOLDEN_FULL_SOUND_PATH);
}

//public void OnClientPutInServer(int client)
public void OnClientPostAdminCheck(int client)
{
	if (IsValidClient(client))
	{
		LoadPlayer(client);
	}
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
	SDKHook(client, SDKHook_TraceAttack, OnTraceAttack);
}

public void OnClientDisconnect(int client)
{
	UnloadPlayer(client);
}

public void OnPluginEnd()
{
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			UnloadPlayer(client);
		}
	}
}
