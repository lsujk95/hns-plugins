
#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <player>

#pragma semicolon 			    1
#pragma tabsize 			    0
#pragma newdecls required

public Plugin myinfo =
{
	name = "HNS Downloader & Precacher",
	author = "UNFORGE LTD",
	description = "HNS Downloader & Precacher",
	version = "0.1",
	url = "http://unforge.net/" 
};

public void OnPluginStart()
{

}

public void OnMapStart()
{
	// Ryman
	AddFileToDownloadsTable("materials/models/player/voikanaa/rayman/rayman_sheet.vtf");
	AddFileToDownloadsTable("materials/models/player/voikanaa/rayman/rayman_sheet_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/voikanaa/rayman/rayman_sheet.vmt");

	// Goku
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/belt.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/belt.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/belt_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/body.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/body.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/body_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot2.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot2.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot3.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot3.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot4.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/boot4.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/eye_lightwarp.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/eyebrow.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/eyebrow.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/face.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/face.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/face_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/facewarp.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hair.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hair.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hair_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hand.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hand.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/hand_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/iris.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/pant.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/pant.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/pant_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/shirt.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/shirt.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/shirt_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/skin_lightwarp_blue1.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/top.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/top.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/top_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/wristband.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/goku/wristband.vtf");

	// Vegeta
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/armor_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/armorc_a.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/armorc_a.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/armorc_b.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/armorc_b.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/boots_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bootsb_a.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bootsb_a.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bootsb_a_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bootsb_b.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bootsb_b.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bust_lightwarp.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bust_lightwarp_2.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bust_lightwarp_blue.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/bust_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/eye.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/eye_lightwarp.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/face.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/face.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/face_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/hair.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/hair.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/hair_lightwarp.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/hair_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/skin_busta.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/skin_busta.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/skin_lightwarp_blue1.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/spat_busta.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/spat_busta.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/vegeta/spat_busta_normal.vtf");

	// Donald Trump
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_eyes.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_eyes_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_hair.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_hair.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_hair_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_hands.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_head.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_jacket.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_pants.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_shoes.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_skin.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_skin_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_suit.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/trump/trump_suit_normal.vtf");

	// Girl IA
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/m.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/mat_003.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/mat_003.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/o.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/o.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/other.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/other.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/s.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/s.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/skin.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/skin.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/a.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/a.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/e.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/e.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/eye.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/eye.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/face.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/face.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/gray.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/gray.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/h.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/h.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/hair.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/hair.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/i.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/i.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/bbs_93x_net_2015/girl_ia/m.vmt");

	// Kirito
	AddFileToDownloadsTable("materials/models/player/cg/kirito/a.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/a.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/b.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/b.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/bb.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/bb.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/c.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/c.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/d.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/d.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/dd.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/dd.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/e.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/e.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/f.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/f.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/ff.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/ff.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/g.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/g.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/h.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/h.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/i.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/i.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/j.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/j.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/k.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/k.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/l.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/l.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/m.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/m.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/n.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/n.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/o.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/o.vtf");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/p.vmt");
	AddFileToDownloadsTable("materials/models/player/cg/kirito/p.vtf");

	// Octodad
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/bowtie.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/octodad_black.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/octodad_blue.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/tophat.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/bowtie.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/octodad_black.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/octodad_blue.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/octodad_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/tophat.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/octodad/tophat_normal.vtf");

	// Katori
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/face.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinli.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinli.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinli_n.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinlihands.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinlihands.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/qinlihandsn.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/sidai.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/sidai.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/monsterko/qinli/face.vmt");


	// Darth Vader
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_cape_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_glove_c.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_glove_c.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_glove_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_helmet_c.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_helmet_c.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_helmet_env.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_helmet_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_innercape_c.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_innercape_inv.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_innercape_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/black.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_body_c.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_body_c.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_body_evn.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_body2_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_cape_c.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_cape_c.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/vader/t_hero_jedi_darthvader_cape_inv.vmt");

	// Stormtrooper
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_body.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_body_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_body2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_hands.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_hands.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_hands_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_hands2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_helmet.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_helmet.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_helmet_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/stormtrooper/stormtrooper_body.vmt");

	// Terminator
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/eye_glow.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/t0096_0.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/t0096_0.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/t0096_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/black.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/black.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/black_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/t-600/eye_glow.vmt");

	// Agent Smith
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/shades.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/shades_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_arms.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_arms_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_hands.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_hands.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smith_hands_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithbody.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithbody.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithbody-normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithhead.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithhead.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithhead-normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshades.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshades2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshades3.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshoe.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshoe.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/smithshoe-normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/urbantemp.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/agent_smith/urbantemp.vtf");
	
	// Shrek
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekbody.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekbody.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekbody_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekbody2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekhead.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekhead.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrekhead_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shrek/shrek_eyes.vmt");

	// Doomguy
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/head_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/helmet_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/helmet_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/helmet_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/helmet_s.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/legs_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/legs_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/legs_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/legs_s.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/torso_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/torso_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/torso_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/torso_s.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/torso2_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/visor_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/head_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/doomguy/head_d.vtf");

	// Putin
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_suit.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_suit.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_suit_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/clip_chrome.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/clip_chrome.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/dt_leather.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/hands.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/hands.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/putin.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/putin.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/smithhead.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/smithhead.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/suit.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/suit.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_gloves.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_gloves.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/putin/arms_gloves_normal.vtf");

	// Spiderman
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_head_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_legs.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_legs.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_legs_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_head.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/spiderman/slow_head.vtf");

	// Kim Jong Un
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/body.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/body.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/body_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/cloth.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/cloth.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/cloth_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/badge.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/kim_jong_un/badge.vtf");

	// Skeleton
	AddFileToDownloadsTable("materials/models/player/kuristaja/skeleton/skeleton.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/skeleton/skeleton_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/skeleton/skeleton.vmt");

	// Snowman
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/carrot.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/hat_path2.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/hat2.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/ice.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/ice_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/snowman2.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/stones.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/wood_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/wood2.vmt");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/s.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/snowman_d.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/snowman/snowman_n.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/shared/detail_cloth4.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/shared/detail_grit.vtf");
	AddFileToDownloadsTable("materials/models/player/custom_player/kodua/shared/detail_snow.vtf");

	// Slimer
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_slimer.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_slimer_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_slimer_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_teeth.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_tongue.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_slime.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/slimer/slow_slimer.vmt");

	// Fat Santa
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa_cloth.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa_cloth.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa_cloth_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa_detail.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/santa_fix/slow_santa.vtf");

	// Christmas Miku
	AddFileToDownloadsTable("materials/models/player/kuristaja/christmas_miku/miku_textures.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/christmas_miku/miku_textures2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/christmas_miku/miku_textures3.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/christmas_miku/miku_textures.vmt");

	// Picolas Cage
	AddFileToDownloadsTable("materials/models/player/kuristaja/picolas_cage/picolas_cage_final_dark.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/picolas_cage/picolas_cage_final_dark_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/picolas_cage/picolas_cage_final_dark.vmt");

	// Harry Potter
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_mouth.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_cloak.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_cloak.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_cloak_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_glasses.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_glasses.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_haira.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_haira.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_haira_alp.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_haira_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_head.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_head.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_head_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_uniform.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_uniform.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3harry_uniform_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_eye_green.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_eye_green.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_handswhite.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_handswhite.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_handswhite_normal.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/harry_potter/hp3gen_mouth.vmt");

	// Deadpool
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_body_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_body_color.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_color.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_metal.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpoolsword_color.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_body_color.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_body_norm.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_detail.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_color.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_exp2.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpool_misc_norm.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpoolsword_color.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/deadpool/deadpoolsword_norm.vtf");

	// Banana Joe
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_body.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_eyes.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_limbs.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_mouth.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_mouth2.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_body.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_body_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_eyes.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_limbs.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_limbs_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/banana_joe/slow_mouth.vtf");


	// Shepard
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/arms.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/body.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/eye.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/face.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/head.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/red.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/white.vmt");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/body.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/body_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/eye.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/eye_bump.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/face.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/face_mask.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/head.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/head_mask.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/red.vtf");
	AddFileToDownloadsTable("materials/models/player/kuristaja/shepard/white.vtf");

	// Thanos
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_body_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_body_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_body_n.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_gauntlet_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_gauntlet_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_gauntlet_n.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_head_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_head_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_head_n.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_helmet_d.vmt");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_helmet_d.vtf");
	AddFileToDownloadsTable("materials/models/player/kaesar2018/thanos/t_m_lrg_jim_helmet_n.vtf");

	// Weapons
	// Abbys Greatsword
	AddFileToDownloadsTable("materials/models/weapons/ventoz/Abyss_Greatsword/abyss_greatsword_d.vmt");
	AddFileToDownloadsTable("materials/models/weapons/ventoz/Abyss_Greatsword/abyss_greatsword_d.vtf");
	AddFileToDownloadsTable("materials/models/weapons/ventoz/Abyss_Greatsword/abyss_greatsword_n.vtf");
	AddFileToDownloadsTable("materials/models/weapons/ventoz/Abyss_Greatsword/green.vtf");
	AddFileToDownloadsTable("materials/models/weapons/ventoz/Abyss_Greatsword/painted_silver_ldr.vtf");

	// Adidas Baseball Bat
	AddFileToDownloadsTable("materials/models/weapons/eminem/adidas_baseball_bat/gloss.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/adidas_baseball_bat/normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/adidas_baseball_bat/adidasbat.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/adidas_baseball_bat/diffuse.vtf");

	// Blue Screwdriver
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue_normal.vtf");

	// Player Screwdriver
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/toilet.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/white_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/blue_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/player_screwdriver/toilet.vmt");

	// Tomahawk
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_alpha_s.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_blade_nml.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_blade_s.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_nml.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_s.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_alpha_col.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_alpha_col.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_blade_col.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_blade_col.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_col.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/_-gmtl_t6_wpn_zmb_tomahawk_col.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/bo2_tomahawk/mtl_t6_wpn_zmb_tomahawk_alpha_nml.vtf");

	// Candy Cane
	AddFileToDownloadsTable("materials/yog/candycane/candycane.vmt");
	AddFileToDownloadsTable("materials/yog/candycane/diffuse.vtf");
	AddFileToDownloadsTable("materials/yog/candycane/gloss.vtf");
	AddFileToDownloadsTable("materials/yog/candycane/normal.vtf");

	// Fidget Spinner
	AddFileToDownloadsTable("materials/models/weapons/eminem/fidget_spinner/fidget_spinner.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/fidget_spinner/fidget_spinner_e.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/fidget_spinner/fidget_spinner.vmt");

	// Freedom Staff
	AddFileToDownloadsTable("materials/models/weapons/c_items/c_tw_eagle.vtf");
	AddFileToDownloadsTable("materials/models/weapons/c_items/c_tw_eagle_gold.vmt");
	AddFileToDownloadsTable("materials/models/weapons/c_items/c_tw_eagle.vmt");

	// Machete
	AddFileToDownloadsTable("materials/models/weapons/eminem/machete/machete.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/machete/machete.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/machete/machete_exp.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/machete/machete_normal.vtf");

	// Hammer
	AddFileToDownloadsTable("materials/models/weapons/eminem/hammer/sandvik.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/hammer/sandvik_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/hammer/sandvik.vmt");

	// HL2 Crowbar
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/crowbar_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/head.vtf");
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/head_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/head_uvw.vmt");
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/crowbar_cyl.vmt");
	AddFileToDownloadsTable("materials/models/weapons/V_crowbar/crowbar_cyl.vtf");

	// Master Sword (Zelda)
	AddFileToDownloadsTable("materials/models/weapons/eminem/master_word/master_word_basecolor.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/master_word/master_word_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/master_word/master_word.vmt");

	// Police Baton
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/tonfa.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/tonfa.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/tonfa_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/hand.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/hand.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/police_baton/hand_normal.vtf");

	// Quake Champions Logo
	AddFileToDownloadsTable("materials/models/weapons/eminem/quake_champions_logo/quake_champions_logo_basecolor.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/quake_champions_logo/quake_champions_logo_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/quake_champions_logo/quake_champions_logo.vmt");

	// Sharpened Volcano Fragment
	AddFileToDownloadsTable("materials/models/workshop/weapons/c_models/c_rift_fire_axe/c_rift_fire_axe.vmt");
	AddFileToDownloadsTable("materials/models/workshop/weapons/c_models/c_rift_fire_axe/c_rift_fire_axe.vtf");
	AddFileToDownloadsTable("materials/models/workshop/weapons/c_models/c_rift_fire_axe/c_rift_fire_axe_normal.vtf");

	// Minecraft Axe Wood
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-00.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-00.vtf");

	// Minecraft Axe Stone
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-01.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-01.vtf");

	// Minecraft Axe Iron
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-02.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-02.vtf");

	// Minecraft Axe Gold
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-03.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-03.vtf");

	// Minecraft Axe Diamond
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-04.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/axe-04.vtf");

	// Minecraft Hoe Wood
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-00.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-00.vtf");

	// Minecraft Hoe Stone
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-01.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-01.vtf");

	// Minecraft Hoe Iron
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-02.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-02.vtf");

	// Minecraft Hoe Gold
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-03.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-03.vtf");

	// Minecraft Hoe Diamond
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-04.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/hoe-04.vtf");

	// Minecraft Pickaxe Wood
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-00.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-00.vtf");

	// Minecraft Pickaxe Stone
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-01.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-01.vtf");

	// Minecraft Pickaxe Iron
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-02.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-02.vtf");

	// Minecraft Pickaxe Gold
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-03.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-03.vtf");

	// Minecraft Pickaxe Diamond
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-04.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/pickaxe-04.vtf");

	// Minecraft Shovel Wood
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-00.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-00.vtf");


	// Minecraft Shovel Stone
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-01.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-01.vtf");

	// Minecraft Shovel Iron
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-02.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-02.vtf");


	// Minecraft Shovel Gold
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-03.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-03.vtf");


	// Minecraft Shovel Diamond
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-04.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/shovel-04.vtf");

	// Minecraft Sword Wood
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-00.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-00.vtf");


	// Minecraft Sword Stone
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-01.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-01.vtf");


	// Minecraft Sword Iron
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-02.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-02.vtf");

	// Minecraft Sword Gold
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-03.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-03.vtf");


	// Minecraft Sword Diamond
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-04.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/mc_items/sword-04.vtf");


	// Grenade Pack (Fire, Flare, Frost, Infect)
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb2.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb2.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/thumb2_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_finger.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_finger.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_finger_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_glove.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_glove.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_glove_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_skin.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_skin.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/view_skin_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/complete.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/complete.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/complete_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/frag.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/frag.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/frag_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one2.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one2.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_one2_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_two.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_two.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/gren_two_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand2.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand2.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_pack/hand2_normal.vtf");

	// Shield Grenade
	AddFileToDownloadsTable("materials/models/weapons/eminem/shield_grenade/holyhandgrenadeskin.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/shield_grenade/holyhandgrenadeskin.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/shield_grenade/handfinal256.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/shield_grenade/handfinal256.vtf");


	// Grenade Snowman
	AddFileToDownloadsTable("materials/models/weapons/counter-strike-source/eminem/v_models/hands/v_hands.vtf");
	AddFileToDownloadsTable("materials/models/weapons/counter-strike-source/eminem/v_models/hands/v_hands_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/counter-strike-source/eminem/v_models/hands/v_hands.vmt");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_snowman/snowman_u.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_snowman/snowman_u_normal.vtf");
	AddFileToDownloadsTable("materials/models/weapons/eminem/grenade_snowman/snowman_u.vmt");


	// Pets
	AddFileToDownloadsTable("materials/models/amaton/courier/venoling/venoling.vmt");
	AddFileToDownloadsTable("materials/models/amaton/courier/venoling/venoling.vtf");
	AddFileToDownloadsTable("materials/models/amaton/courier/venoling/venoling_normal.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_dire_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_dire_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_elemental_masks1.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_elemental_masks2.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_gold_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_gold_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_gold_masks1.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_gold_masks2.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_ice_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_ice_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_lava_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_lava_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_masks1.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_masks2.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_normal.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_platinum_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_platinum_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_radiant_color.vmt");
	AddFileToDownloadsTable("materials/models/courier/baby_rosh/babyrosh_radiant_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/drodo/drodo.vmt");
	AddFileToDownloadsTable("materials/models/courier/drodo/drodo_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/drodo/drodo_mask1.vtf");
	AddFileToDownloadsTable("materials/models/courier/drodo/drodo_mask2.vtf");
	AddFileToDownloadsTable("materials/models/courier/drodo/drodo_normal.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling.vmt");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_gold_color.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_gold_mask1.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_gold_mask2.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_golden.vmt");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_mask1.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_mask2.vtf");
	AddFileToDownloadsTable("materials/models/courier/huntling/huntling_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/baekho/baekho.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/baekho/baekho_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/baekho/baekho_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/baekho/baekho_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/baekho/baekho_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/billy_bounceback/billy_bounceback.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/billy_bounceback/billy_bounceback_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/billy_bounceback/billy_bounceback_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/billy_bounceback/billy_bounceback_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/billy_bounceback/billy_bounceback_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/captain_bamboo/captain_bamboo.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/captain_bamboo/captain_bamboo_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/captain_bamboo/captain_bamboo_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/captain_bamboo/captain_bamboo_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/captain_bamboo/captain_bamboo_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/courier_mvp_redkita/courier_mvp_redkita.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/courier_mvp_redkita/courier_mvp_redkita_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/courier_mvp_redkita/courier_mvp_redkita_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/courier_mvp_redkita/courier_mvp_redkita_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/courier_mvp_redkita/courier_mvp_redkita_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/duskie/duskie.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/duskie/duskie_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/duskie/duskie_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/duskie/duskie_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/duskie/duskie_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/faceless_rex/faceless_rex.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/faceless_rex/faceless_rex_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/faceless_rex/faceless_rex_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/faceless_rex/faceless_rex_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/faceless_rex/faceless_rex_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lgd_golden_skipper/lgd_golden_skipper.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/lgd_golden_skipper/lgd_golden_skipper_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lgd_golden_skipper/lgd_golden_skipper_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lgd_golden_skipper/lgd_golden_skipper_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lgd_golden_skipper/lgd_golden_skipper_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lilnova/lilnova.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/lilnova/lilnova_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lilnova/lilnova_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lilnova/lilnova_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/lilnova/lilnova_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/shagbark/shagbark.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/shagbark/shagbark_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/shagbark/shagbark_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/shagbark/shagbark_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/shagbark/shagbark_normal.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_bluepaw.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_bluepaw_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_red_panda.vmt");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_red_panda_color.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_red_panda_mask1.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_red_panda_mask2.vtf");
	AddFileToDownloadsTable("materials/models/items/courier/snaggletooth_red_panda/snaggletooth_red_panda_normal.vtf");
	AddFileToDownloadsTable("materials/models/npc/bulbasaur/fushigidanedh.vmt");
	AddFileToDownloadsTable("materials/models/npc/bulbasaur/fushigidanedh.vtf");
	AddFileToDownloadsTable("materials/models/npc/bulbasaur/fushigidaneeyedh.vmt");
	AddFileToDownloadsTable("materials/models/npc/bulbasaur/fushigidaneeyedh.vtf");
	AddFileToDownloadsTable("materials/models/npc/golbat/golbatdh.vmt");
	AddFileToDownloadsTable("materials/models/npc/golbat/golbatdh.vtf");
	AddFileToDownloadsTable("materials/models/npc/golbat/golbateyedh.vmt");
	AddFileToDownloadsTable("materials/models/npc/golbat/golbateyedh.vtf");
	AddFileToDownloadsTable("materials/models/npc/mew/mewbodydh.vmt");
	AddFileToDownloadsTable("materials/models/npc/mew/mewbodydh.vtf");
	AddFileToDownloadsTable("materials/models/npc/mew/meweyedh.vmt");
	AddFileToDownloadsTable("materials/models/npc/mew/meweyedh.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glacia.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glacia.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glacia_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glacia_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye5.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye5_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye6.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye6_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye7.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye7_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciaeye_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/glaceon/glaciamouth_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunders.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunders.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunders_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunders_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye5.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye5_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye6.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye6_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye7.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye7_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/jolteon/thunderseye_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_illum.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blacky_shinyillum.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye1_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye1_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye2_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye2_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye5.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye5_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye5_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye5_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye6.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye6_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye7.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye7_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye7_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye7_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye8.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye8_illum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye8_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye8_shinyillum.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye_illum.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackyeye_shinyillum.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/umbreon/blackymouth_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showers.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showers.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showers_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showers_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye5.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye5_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye6.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye6_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye7.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye7_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showerseye_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersfillet.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersfillet.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersfillet_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersfillet_shiny.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth.vtf");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth1.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth1_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth2.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth2_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth3.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth3_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth4.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth4_shiny.vmt");
	AddFileToDownloadsTable("materials/models/rtbmodels/pokemon/vaporeon/showersmouth_shiny.vtf");
	AddFileToDownloadsTable("materials/models/items/cs_gift.vmt");
	AddFileToDownloadsTable("materials/models/items/cs_gift.vtf");
}