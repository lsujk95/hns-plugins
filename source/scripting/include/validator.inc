#if defined _validator_included
 #endinput
#endif
#define _validator_included

#define VALIDATOR_MAX_ADDR 64

char Validator_IPs[][VALIDATOR_MAX_ADDR] = 
{
	"51.77.34.138",
    "51.77.34.138",
    "51.77.34.138",
    "51.77.34.138",
};

int Validator_Ports[] = 
{
	30024,
    30029,
    30020,
    30021,
};

stock void Validate()
{
    // F Check
    bool IsLoaded = false;
    char ServerIP[VALIDATOR_MAX_ADDR];
    int IP = GetConVarInt(FindConVar("hostip"));
    int Port = GetConVarInt(FindConVar("hostport"));

    Format(ServerIP, sizeof(ServerIP), "%d.%d.%d.%d", (IP >> 24) & 0x000000FF, (IP >> 16) & 0x000000FF, (IP >> 8) & 0x000000FF, IP & 0x000000FF);

    for (int CurrIP = 0; CurrIP < sizeof(Validator_IPs); CurrIP++)
    {
        if (!strcmp(Validator_IPs[CurrIP], ServerIP) && (Validator_Ports[CurrIP] == 0 || Validator_Ports[CurrIP] == Port))
        {
            IsLoaded = true;
            break;
        }
    }

    if (!IsLoaded)
    {
        SetFailState("This plugin can be loaded!");	
    }
}
