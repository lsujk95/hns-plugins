#if defined _ums_core_included
 #endinput
#endif
#define _ums_core_included

// Users System
native int GetServerID();
native void GetServerPrefix(char[] text, int size);

native bool IsPlayerSystemUser(int client);
native int GetPlayerGroupColor(int client);
native bool GetPlayerGroupText(int client, char[] text, int size); // true if is in any group

// Products System
native int GetPlayerProductValue(int client, int key);
native bool GivePlayerProduct(int client, int key, int value, int type = 2); // type: 1 - time or 2 - amount
native bool RemovePlayerProduct(int client, int key);

// Bans System
native int IsPlayerGagged(int client); // retuns -1 if not or time
native int IsPlayerMuted(int client); // retuns -1 if not or time

// Rank System
native bool IncreasePlayerRankPoints(int client, int amount); // true if valid amount
native bool DecreasePlayerRankPoints(int client, int amount); // true if valid amount