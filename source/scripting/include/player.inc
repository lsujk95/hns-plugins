#if defined _player_included
 #endinput
#endif
#define _player_included

#define MAX_ARMOR 250

stock int GetPlayerHealth(int client) { return GetEntProp(client, Prop_Send, "m_iHealth"); }
stock int GetPlayerMaxHealth(int client) { return GetEntProp(client, Prop_Data, "m_iMaxHealth"); }
stock int GetPlayerArmor(int client) { return GetEntProp(client, Prop_Send, "m_ArmorValue");}
stock int GetPlayerCash(int client) { return GetEntProp(client, Prop_Send, "m_iAccount"); }
stock int GetPlayerActiveWeapon(int client) { return GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon"); }
stock int GetPlayerClipAmmo(int client) { return GetEntProp(GetPlayerActiveWeapon(client), Prop_Send, "m_iClip1"); }
stock int GetPlayerReserveAmmo(int client) { return GetEntProp(GetPlayerActiveWeapon(client), Prop_Send, "m_iPrimaryReserveAmmoCount"); }
stock int GetPlayerWaterLevel(int client){ return GetEntProp(client, Prop_Data, "m_nWaterLevel"); }
stock float GetPlayerSpeed(int client) { return GetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue"); }
stock float GetPlayerGravity(int client) { return GetEntPropFloat(client, Prop_Data, "m_flGravity"); }

stock int GetPlayerVisibility(int client)
{
    int TempR, TempG, TempB, TempA;
    GetEntityRenderColor(client, TempR, TempG, TempB, TempA);

    return TempA;
}

stock void GetPlayerColor(int client, int r, int g, int b)
{
    int TempA;
    GetEntityRenderColor(client, r, g, b, TempA);
}

stock void SetPlayerHealth(int client, int amount) { SetEntProp(client, Prop_Send, "m_iHealth", amount); }
stock void SetPlayerMaxHealth(int client, int amount) { SetEntProp(client, Prop_Data, "m_iMaxHealth", amount); }
stock void SetPlayerArmor(int client, int amount) { SetEntProp(client, Prop_Send, "m_ArmorValue", amount); }
stock void SetPlayerCash(int client, int amount) { SetEntProp(client, Prop_Send, "m_iAccount", amount); }
stock void SetPlayerActiveWeapon(int client, int entity) { SetEntPropEnt(client, Prop_Send, "m_hActiveWeapon", entity); }
stock void SetPlayerClipAmmo(int client, int amount) { SetEntProp(GetPlayerActiveWeapon(client), Prop_Send, "m_iClip1", amount); }
stock void SetPlayerReserveAmmo(int client, int amount) { SetEntProp(GetPlayerActiveWeapon(client), Prop_Send, "m_iPrimaryReserveAmmoCount", amount);}
stock void SetPlayerSpeed(int client, float amount) { SetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue", amount); }
stock void SetPlayerGravity(int client, float amount) { SetEntPropFloat(client, Prop_Data, "m_flGravity", amount); }

stock void SetPlayerColor(int client, int r, int g, int b)
{
    int TempR, TempG, TempB, TempA;

    GetEntityRenderColor(client, TempR, TempG, TempB, TempA);
    SetEntityRenderColor(client, r, g, b, TempA);
}

stock void SetPlayerVisibility(int client, int amount)
{
	int TempR, TempG, TempB, TempA;

	GetEntityRenderColor(client, TempR, TempG, TempB, TempA);

	SetEntityRenderMode(client, RENDER_TRANSCOLOR);
	SetEntityRenderColor(client, TempR, TempG, TempB, amount);
}

stock bool IsValidClient(int client, bool CheckIsAlive = false)
{
	if (client <= 0) 					return false;
	if (client >= MAXPLAYERS) 			return false;
	if (!IsClientConnected(client)) 	return false;
	if (!IsClientInGame(client)) 		return false;
	if (IsClientSourceTV(client)) 		return false;
	if (IsFakeClient(client))			return false;
	
	if (CheckIsAlive)
	{
		return IsPlayerAlive(client);
	}
	
	return true;
}

stock void PerformBlind(int target, int color[4] = {0, 0, 0, 0}, int duration = 1, int holdtime = -1, int type = 0)
{
	int targets[2];
	UserMsg g_FadeUserMsgId;

	targets[0] = target;
	g_FadeUserMsgId = GetUserMessageId("Fade");
	
	// int duration = 1536;
	// int holdtime = 1536;
	
	// FFADE_IN 0x0001 // Just here so we don't pass 0 into the function
	// FFADE_OUT 0x0002 // Fade out (not in)
	// FFADE_MODULATE 0x0004 // Modulate (don't blend)
	// FFADE_STAYOUT 0x0008 // ignores the duration, stays faded out until new ScreenFade message received
	// FFADE_PURGE 0x0010 // Purges all other fades, replacin
	
	int flags;
	if (color[3] == 0)
	{
		flags = (0x0001 | 0x0008);
	}
	else
	{
		flags = (0x0002 | 0x0010);
	}
	
	int tempColor[4];

	tempColor[0] = color[0];
	tempColor[1] = color[1];
	tempColor[2] = color[2];
	tempColor[3] = color[3];

	if (type == 1)
	{
		tempColor[0] = GetRandomInt(0,255);
		tempColor[1] = GetRandomInt(0,255);
		tempColor[2] = GetRandomInt(0,255);
	}
	
	Handle message = StartMessageEx(g_FadeUserMsgId, targets, 1);
	if (GetUserMessageType() == UM_Protobuf)
	{
		Protobuf pb = UserMessageToProtobuf(message);
		pb.SetInt("duration", duration);
		pb.SetInt("hold_time", holdtime);
		pb.SetInt("flags", flags);
		pb.SetColor("clr", tempColor);
	}
	else
	{
		BfWrite bf = UserMessageToBfWrite(message);
		bf.WriteShort(duration);
		bf.WriteShort(holdtime);
		bf.WriteShort(flags);		
		bf.WriteByte(color[0]);
		bf.WriteByte(color[1]);
		bf.WriteByte(color[2]);
		bf.WriteByte(color[3]);
	}
	
	EndMessage();
}

stock bool IsInSpheralRange(int entity, int target, float range)
{
    float EntityOrigin[3];
    float TargetOrigin[3];

    if (entity < MAXPLAYERS) GetClientAbsOrigin(entity, EntityOrigin);
     else GetEntPropVector(entity, Prop_Send, "m_vecOrigin", EntityOrigin);
    
    if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
     else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);
	 

    if (GetVectorDistance(EntityOrigin, TargetOrigin) < range)
    {
        return true; 
    }
    return false;
}

stock bool IsInSpheralRangeFromPoint(float point[3], int target, float range)
{
    float TargetOrigin[3];
    
    if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
     else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);
	
    if (GetVectorDistance(point, TargetOrigin) < range)
    { 
        return true; 
    }
    return false;
}

stock bool IsInCylindricalRange(int entity, int target, float range, float z = 0.0)
{
	float EntityOrigin[3];
	float TargetOrigin[3];

	if (z <= 0.0) z = range;

	if (entity < MAXPLAYERS) GetClientAbsOrigin(entity, EntityOrigin);
	 else GetEntPropVector(entity, Prop_Send, "m_vecOrigin", EntityOrigin);

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	float TempEntityOrigin[3];
	float TempTargetOrigin[3];

	TempEntityOrigin[0] = EntityOrigin[0];
	TempEntityOrigin[1] = EntityOrigin[1];
	TempEntityOrigin[2] = 0.0;

	TempTargetOrigin[0] = TargetOrigin[0];
	TempTargetOrigin[1] = TargetOrigin[1];
	TempTargetOrigin[2] = 0.0;

	if 
	(
		GetVectorDistance(TempEntityOrigin, TempTargetOrigin) < range &&
		TargetOrigin[2] < EntityOrigin[2] + z &&
		TargetOrigin[2] > EntityOrigin[2] - z
	)
	{
		return true; 
	}

	return false;
}

stock bool IsInCylindricalRangeFromPoint(float point[3], int target, float range, float z = 0.0)
{
	float TargetOrigin[3];

	if (z <= 0.0) z = range;

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	float TempPointOrigin[3];
	float TempTargetOrigin[3];

	TempPointOrigin[0] = point[0];
	TempPointOrigin[1] = point[1];
	TempPointOrigin[2] = 0.0;

	TempTargetOrigin[0] = TargetOrigin[0];
	TempTargetOrigin[1] = TargetOrigin[1];
	TempTargetOrigin[2] = 0.0;

	if 
	(
		GetVectorDistance(TempPointOrigin, TempTargetOrigin) < range &&
		TargetOrigin[2] < point[2] + z &&
		TargetOrigin[2] > point[2] - z
	)
	{
		return true; 
	}

	return false;
}

stock bool IsOver(int entity, int target)
{
	float EntityOrigin[3];
	float TargetOrigin[3];

	if (entity < MAXPLAYERS) GetClientAbsOrigin(entity, EntityOrigin);
	 else GetEntPropVector(entity, Prop_Send, "m_vecOrigin", EntityOrigin);

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	if (EntityOrigin[2] <= TargetOrigin[2])
	{
		return true;
	}

	return false;
}

stock bool IsOverPoint(float point[3], int target)
{
	float TargetOrigin[3];

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	if (point[2] <= TargetOrigin[2])
	{
		return true;
	}

	return false;
}

stock bool IsInDirect(int entity, int target)
{
	float EntityOrigin[3];
	float TargetOrigin[3];

	if (entity < MAXPLAYERS) GetClientAbsOrigin(entity, EntityOrigin);
	 else GetEntPropVector(entity, Prop_Send, "m_vecOrigin", EntityOrigin);

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	float TempVector[3];
	float TempAngle[3];

	MakeVectorFromPoints(EntityOrigin, TargetOrigin, TempVector);
	GetVectorAngles(TempVector, TempAngle);
	
	Handle trace = TR_TraceRayFilterEx(EntityOrigin, TempAngle, MASK_SHOT, RayType_Infinite, RangeFilter);    
	if (TR_DidHit(trace))
	{
		float HitOrigin[3];
		TR_GetEndPosition(HitOrigin, trace);

		if ((GetVectorDistance(EntityOrigin, HitOrigin, false) + 20) >= GetVectorDistance(EntityOrigin, TargetOrigin))
		{
			delete trace;
			return true;
		}
	}
	delete trace;
	return false;
}

stock bool IsOnFront(int entity, int target)
{
	float EntityOrigin[3];
	float EntityAngle[3];
	float TargetOrigin[3];

	float MovedPoint[3];

	if (entity < MAXPLAYERS) GetClientAbsOrigin(entity, EntityOrigin);
	 else GetEntPropVector(entity, Prop_Send, "m_vecOrigin", EntityOrigin);

	if (entity < MAXPLAYERS) GetClientEyeAngles(entity, EntityAngle);
	 else GetEntPropVector(entity, Prop_Data, "m_angRotation", EntityAngle);  
	 //else GetEntPropVector(entity, Prop_Send, "m_vecAngles", EntityAngle);

	if (target < MAXPLAYERS) GetClientAbsOrigin(target, TargetOrigin);
	 else GetEntPropVector(target, Prop_Send, "m_vecOrigin", TargetOrigin);

	EntityAngle[1] = DegToRad(EntityAngle[1] - 90.0); 

	MovedPoint[0] = EntityOrigin[0] + 20.0 * Cosine(EntityAngle[1]); 
	MovedPoint[1] = EntityOrigin[1] + 20.0 * Sine(EntityAngle[1]);

	float Distance = (MovedPoint[0] - EntityOrigin[0]) * (TargetOrigin[1] - EntityOrigin[1]) - (MovedPoint[1] - EntityOrigin[1]) * (TargetOrigin[0] - EntityOrigin[0]); 
	if (Distance > 0.0)
	{
		return true;
	}

	return false;
}
//  true - pozwala na uderzenie
// false - nie pozwala
// entityt to aktualny entity
// data poda na data
stock bool RangeFilter(entity, contentsMask)
{
    if (entity < MAXPLAYERS || !IsValidEntity(entity))
    {
        return false;
    }
    
    return true;
}  

stock bool EnemyFilter(entity, contentsMask, data)
{
    if (entity == data) return false;
    
    return true;
}

stock bool WorldFilter(entity, contentsMask, data)
{
    if (entity == data || IsValidClient(entity)) return false;
    
    return true;
}

stock bool StaticWorldFilter(entity, contentsMask, data)
{
    if (entity > 0) return false;
    
    return true;
}