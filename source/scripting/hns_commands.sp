
#include <sourcemod>
// #include <cstrike>
// #include <sdkhooks>
// #include <sdktools>
#include <player>

#pragma semicolon 			    1
#pragma tabsize 			    0
#pragma newdecls required

public Plugin myinfo =
{
	name = "HNS Commands",
	author = "UNFORGE LTD",
	description = "HNS Commands",
	version = "0.1",
	url = "http://unforge.net/" 
};

public void OnPluginStart()
{
	// Translations
	LoadTranslations("hns_cmds.phrases");

	// Commands
	RegConsoleCmd("sm_cmds", cmdPlayerCommands);
	RegConsoleCmd("sm_cmd", cmdPlayerCommands);
	RegConsoleCmd("sm_komendy", cmdPlayerCommands);
}

/* ======================================================= */
/* =============== Commands & Menus ====================== */
/* ======================================================= */
public Action cmdPlayerCommands(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
    OpenPlayerCommandsMenu(client);

	return Plugin_Handled;
}

void OpenPlayerCommandsMenu(int client)
{
    char MenuText[128];

    Menu menu = new Menu(mPlayerCommandsMenu);
    menu.SetTitle("%T:", "Cmds_Title", client);
    
	 Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_0", client);
    menu.AddItem("options", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_1", client);
    menu.AddItem("top", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_2", client);
    menu.AddItem("stats", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_3", client);
    menu.AddItem("rank", MenuText);
	
	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_4", client);
    menu.AddItem("players", MenuText);
	
	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_5", client);
    menu.AddItem("store", MenuText);
	
	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_6", client);
    menu.AddItem("ljblock", MenuText);
	
	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_7", client);
    menu.AddItem("measure", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_8", client);
    menu.AddItem("bhopcheck", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_9", client);
    menu.AddItem("knife", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_10", client);
    menu.AddItem("ws", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_11", client);
    menu.AddItem("gloves", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_12", client);
    menu.AddItem("skins", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_13", client);
    menu.AddItem("jackpot", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_14", client);
    menu.AddItem("tp", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_15", client);
    menu.AddItem("redie", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_16", client);
    menu.AddItem("fov", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_17", client);
    menu.AddItem("toggleknife", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "Cmds_CMD_18", client);
    menu.AddItem("distbug", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mPlayerCommandsMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		char Command[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

		Format(Command, sizeof(Command), "sm_%s", ChoosedMenu);
		FakeClientCommand(param1, Command);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}