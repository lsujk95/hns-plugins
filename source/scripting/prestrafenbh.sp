
#include <sourcemod>
// #include <cstrike>
// #include <sdkhooks>
#include <sdktools>
#include <player>

#pragma semicolon 			    1
#pragma tabsize 			    0
#pragma newdecls required

// Convars
// ConVar TickRate;
ConVar IsPrestrafeEnabled;
ConVar ShouldLoadRules;
ConVar BHopSpeedLimit;

// Server
int Server_Tickrate;

// Data
float PlayerLastAngles[MAXPLAYERS][3];
float PrestrafeVelocity[MAXPLAYERS];
float VelocityModifierLastChange[MAXPLAYERS];
int PrestrafeFrameCounter[MAXPLAYERS];

int PlayerFlags[MAXPLAYERS][2];
int PlayerButtons[MAXPLAYERS][2];

bool IsPlayerInBunnyHop[MAXPLAYERS];
int PlayerJumpChanges[MAXPLAYERS];
int PlayerJumpCounter[MAXPLAYERS];
int PlayerJumpButtons[MAXPLAYERS][2];

Handle hResetBH[MAXPLAYERS];

public Plugin myinfo =
{
	name = "IC HNS",
	author = "UNFORGE LTD",
	description = "IC HNS",
	version = "0.1",
	url = "http://unforge.net/" 
};


public void OnPluginStart()
{
	// TickRate = CreateConVar("ic_hns_tickrate", "0", "Overrides automatic tickrate. (0 - off, any - tickrate)", FCVAR_NOTIFY);
	IsPrestrafeEnabled = CreateConVar("ic_hns_prestrafe", "1", "Enables prestrafe (0 - off, 1 - on)", FCVAR_NOTIFY);
	ShouldLoadRules = CreateConVar("ic_hns_rules", "1", "Setup HNS server (0 - off, 1 - on)", FCVAR_NOTIFY);
	BHopSpeedLimit = CreateConVar("ic_hns_bhmaxspeed", "300.0", "Maximum BH speed (0 - off, any - on)", FCVAR_NOTIFY);

	AutoExecConfig(true, "ic_hns");

	// Check tickrate
    Server_Tickrate = RoundFloat(1.0 / GetTickInterval());
    PrintToServer("---- SERVER TICKRATE: %d ----", Server_Tickrate);
}


public void OnConfigsExecuted()
{
	if (ShouldLoadRules.IntValue)
	{
		SetCvar("sv_staminalandcost", "0.0"); 
		SetCvar("sv_staminajumpcost", "0.0"); 
		SetCvar("sv_gravity", "800"); 
		SetCvar("sv_airaccelerate", "100.0"); 
		SetCvar("sv_maxspeed", "320"); 
		SetCvar("sv_wateraccelerate", "10.0"); 
		SetCvar("sv_friction", "5.0"); 
		SetCvar("sv_accelerate", "6.5"); 
		SetCvar("sv_maxvelocity", "2000.0"); 
		SetCvar("sv_enablebunnyhopping", "1"); 
		SetCvar("sv_autobunnyhopping", "0"); 
		SetCvar("sv_clamp_unsafe_velocities", "0"); 
		SetCvar("sv_jump_impulse", "301.993377"); 
		SetCvar("sv_accelerate_use_weapon_speed", "0"); 
		SetCvar("sv_ladder_scale_speed", "1.0"); 
	}
}

public void OnClientPutInServer(int client)
{
    if (IsValidClient(client))
    {
        SetClientDefaults(client);

		PlayerJumpChanges[client] = 0;

		PlayerJumpButtons[client][0] = 0;
		PlayerJumpButtons[client][1] = 0;

		IsPlayerInBunnyHop[client] = false;
    }
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
    if (!IsValidClient(client, true)) return Plugin_Continue;

	PlayerFlags[client][0] = GetEntityFlags(client);
	PlayerButtons[client][0] = GetClientButtons(client);

	/*
	float EyeAngleChange = angles[1] - PlayerLastAngles[client][1];

	if (EyeAngleChange < -180) EyeAngleChange += 360;
    if (EyeAngleChange > 180) EyeAngleChange -= 360;

	if (EyeAngleChange < 0) PrintToChatAll("RIGHT");
	 else if (EyeAngleChange > 0) PrintToChatAll("LEFT");
	*/

	BPSpeedLimiter(client);
    Prestrafe(client, angles[1], buttons);

    PlayerLastAngles[client] = angles;
	PlayerFlags[client][1] = PlayerFlags[client][0];
	PlayerButtons[client][1] = PlayerButtons[client][0];

	return Plugin_Continue;
}

/* ========================= */
/* ======== TIMERS ========= */
/* ========================= */
public Action tResetBH(Handle timer, int client)
{
	// Reset player BH state
	IsPlayerInBunnyHop[client] = false;

	// Reset player jumps counter
	PlayerJumpCounter[client] = 0;

	// Reset timer handle
	hResetBH[client] = null;

	return Plugin_Stop;
}

/* =========================== */
/* ======== FUNCTINOS ======== */
/* =========================== */
void BPSpeedLimiter(int client)
{
	// if (!IsValidClient(client, true)) return;

	// Is player in air
	if (!(PlayerFlags[client][1] & FL_ONGROUND) && !(PlayerFlags[client][0] & FL_ONGROUND))
	{
		if (IsPlayerInBunnyHop[client] && PlayerJumpCounter[client] == 1)
		{
			// Set direction
			if (PlayerButtons[client][0] & IN_MOVELEFT)
			{
				PlayerJumpButtons[client][0] = IN_MOVELEFT;
			}
			else if (PlayerButtons[client][0] & IN_MOVERIGHT)
			{
				PlayerJumpButtons[client][0] = IN_MOVERIGHT;
			}
			
			// Is direction changed
			if (PlayerJumpButtons[client][1] & IN_MOVELEFT && PlayerJumpButtons[client][0] & IN_MOVERIGHT)
			{
				PlayerJumpChanges[client]++;
				PrintToChatAll("R %d change", PlayerJumpChanges[client]);
			}
			else if (PlayerJumpButtons[client][1] & IN_MOVERIGHT && PlayerJumpButtons[client][0] & IN_MOVELEFT)
			{
				PlayerJumpChanges[client]++;
				PrintToChatAll("L %d change", PlayerJumpChanges[client]);	
			}
			
			// Is too many direction changes
			if (PlayerJumpChanges[client] > 1)
			{
				IsPlayerInBunnyHop[client] = false;
				PrintToChatAll("Too many changes for BH");
			}

			// Set last keys
			PlayerJumpButtons[client][1] = PlayerJumpButtons[client][0];
		}
	}

	// Is player jump from ground
	if (PlayerFlags[client][1] & FL_ONGROUND && !(PlayerFlags[client][0] & FL_ONGROUND) && PlayerButtons[client][0] & IN_JUMP) 
	{
		if (hResetBH[client] != null)
		{
			KillTimer(hResetBH[client]);
			hResetBH[client] = null;
		}

		// Last jump keys
		PlayerJumpButtons[client][0] = 0;
		PlayerJumpButtons[client][1] = 0;

		// Reset jump changes
		PlayerJumpChanges[client] = 0;

		// Player is in BH at start
		IsPlayerInBunnyHop[client] = true;

		// Increment jump counter
		PlayerJumpCounter[client]++;
	}

	// Is player fall
	if (!(PlayerFlags[client][1] & FL_ONGROUND) && PlayerFlags[client][0] & FL_ONGROUND)
  	{
		if (IsPlayerInBunnyHop[client] && hResetBH[client] == null)
		{
			hResetBH[client] = CreateTimer(0.1, tResetBH, client);
	  	}
	}

	if (IsPlayerInBunnyHop[client])
	{
		float CurrentSpeed = GetSpeed(client);

		float CurrVelocity[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", CurrVelocity);

		if (BHopSpeedLimit.FloatValue > 0.0 && CurrentSpeed > BHopSpeedLimit.FloatValue)
		{
			NormalizeVector(CurrVelocity, CurrVelocity);
			ScaleVector(CurrVelocity, BHopSpeedLimit.FloatValue);
			
			TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, CurrVelocity);
		}
	}
}

void Prestrafe(int client, float angles, int &buttons)
{
	// if (!IsValidClient(client, true) || !(GetEntityFlags(client) & FL_ONGROUND)) return;
	if (!(GetEntityFlags(client) & FL_ONGROUND)) return;

	bool Forward;
	bool IsTurningRight;
	bool IsTurningLeft;

	int MaxFrameCount;

	float DefaultKnifeSpeed = 1.0;
	float MaxKnifeSpeed = 1.104;
	float DefaultUspSpeed= 1.041667;
	float MaxUspSpeed = 1.15;
	float UnarmedSpeed = 0.96154;
	float SpeedIncrease;
    float SpeedDecrease;
	float Speed = GetSpeed(client);

	char ClassName[128];
	GetClientWeapon(client, ClassName, sizeof(ClassName));
	TrimString(ClassName);

	if 
	(
		!IsPrestrafeEnabled.IntValue ||
		(
			!StrEqual(ClassName, "weapon_hkp2000") &&
			!StrEqual(ClassName, "weapon_usp_silencer") &&
			!StrEqual(ClassName, "weapon_bayonet") &&
			StrContains(ClassName, "weapon_knife") &&
			StrContains(ClassName, "weapon")
		)
	)
	{
		if (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultUspSpeed);
		}

		else if ((StrContains(ClassName, "weapon_knife")) != -1 || (StrContains(ClassName, "weapon_bayonet") != -1))
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultKnifeSpeed);
		}

		else
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", UnarmedSpeed);
		}

		return;
	}


	// Get turning Direction
	// Fixed for  -180 180 wrap
	float EyeAngleChange = angles - PlayerLastAngles[client][1];

	if(EyeAngleChange < -180)  
    {
		EyeAngleChange += 360;
    }

    if (EyeAngleChange > 180)
	{
        EyeAngleChange -= 360;
    }

	
	if(EyeAngleChange < 0)
    {
		IsTurningRight = true;
    }
	else if(EyeAngleChange > 0)
    {
     	IsTurningLeft = true;   
    }

	// Get moving Direction
	if (GetClientMovingDirection(client,false) > 0.0)
    {
		Forward = true;
    }

	float  flVelMd = GetEntPropFloat(client, Prop_Send, "m_flVelocityModifier");
    if 
    (
        (
            StrEqual(ClassName, "weapon_knife") ||
	        StrEqual(ClassName, "weapon_bayonet") ||
	        StrContains(ClassName, "weapon_knife_") != -1
        ) &&
	    flVelMd > MaxKnifeSpeed + 0.007
    )
	{
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", MaxKnifeSpeed - 0.001);
	}

	// No mouse movement?
	if (!IsTurningRight && !IsTurningLeft)
	{
		float diff;
		diff = GetEngineTime() - VelocityModifierLastChange[client];
		if (diff > 0.2)
		{
			if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
            {
				PrestrafeVelocity[client] = DefaultUspSpeed;
            }
            else
            {
				PrestrafeVelocity[client] = DefaultKnifeSpeed;
            }

            VelocityModifierLastChange[client] = GetEngineTime();
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", PrestrafeVelocity[client]);
		}
		return;
	}

	if (((buttons & IN_MOVERIGHT) || (buttons & IN_MOVELEFT)) && Speed > 248.9)
	{
		// Tickrate depending values
		if (Server_Tickrate == 64)
		{
			MaxFrameCount = 45;
			SpeedIncrease = 0.0015;
			if ((PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer"))))
				SpeedIncrease = 0.001;
			SpeedDecrease = 0.0045;
		}

		if (Server_Tickrate == 102)
		{
			MaxFrameCount = 60;
			SpeedIncrease = 0.0011;
			if ((PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer"))))
				SpeedIncrease = 0.001;
			SpeedDecrease = 0.0045;

		}

		if (Server_Tickrate == 128)
		{
			MaxFrameCount = 75;
			SpeedIncrease = 0.0009;
			if ((PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer"))))
				SpeedIncrease = 0.001;
			SpeedDecrease = 0.0045;
		}

		if (((buttons & IN_MOVERIGHT && IsTurningRight || IsTurningLeft && !Forward)) || ((buttons & IN_MOVELEFT && IsTurningLeft || IsTurningRight && !Forward)))
		{
			PrestrafeFrameCounter[client]++;
			// Add Speed if Prestrafe frames are less than max frame count
			if (PrestrafeFrameCounter[client] < MaxFrameCount)
			{
				// Increase Speed
				PrestrafeVelocity[client]+= SpeedIncrease;
				// Usp
				if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
				{
					if (PrestrafeVelocity[client] > MaxUspSpeed)
						PrestrafeVelocity[client] -= 0.007;
				}
				else
				{
					if (PrestrafeVelocity[client] > MaxKnifeSpeed)
					{
						if (PrestrafeVelocity[client] > MaxKnifeSpeed+0.007)
							PrestrafeVelocity[client] = MaxKnifeSpeed-0.001;
						else
							PrestrafeVelocity[client] -= 0.007;
					}
				}
				PrestrafeVelocity[client] += SpeedIncrease;
			}
			else
			{
				// Decrease Speed
				PrestrafeVelocity[client] -= SpeedDecrease;
				PrestrafeFrameCounter[client] = PrestrafeFrameCounter[client] - 2;

				// Usp reset 250.0 Speed
				if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
				{
					if (PrestrafeVelocity[client] < DefaultUspSpeed)
					{
						PrestrafeFrameCounter[client] = 0;
						PrestrafeVelocity[client] = DefaultUspSpeed;
					}
				}
				else
					// Knife reset 250.0 Speed
					if (PrestrafeVelocity[client] < DefaultKnifeSpeed)
					{
						PrestrafeFrameCounter[client] = 0;
						PrestrafeVelocity[client] = DefaultKnifeSpeed;
					}
			}
		}
		else
		{
			PrestrafeVelocity[client] -= 0.04;
			if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
			{
				if (PrestrafeVelocity[client] < DefaultUspSpeed)
					PrestrafeVelocity[client] = DefaultUspSpeed;
			}
			else
			if (PrestrafeVelocity[client] < DefaultKnifeSpeed)
				PrestrafeVelocity[client] = DefaultKnifeSpeed;
		}

		// Set VelocityModifier
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", PrestrafeVelocity[client]);
		VelocityModifierLastChange[client] = GetEngineTime();
	}
	else
	{
		if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
        {
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultUspSpeed);
        }
        else
        {
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultKnifeSpeed);
        }

        PrestrafeFrameCounter[client] = 0;
	}
}

/*
public DoValidTeleport(client, Float:origin[3],Float:angles[3],Float:vel[3])
{
	if (!IsValidClient(client, true)) return;

	g_fTeleportValidationTime[client] = GetEngineTime();
	TeleportEntity(client, origin, angles, vel);
}
*/

void SetClientDefaults(int client)
{
	PrestrafeVelocity[client] = 1.0;
    PrestrafeFrameCounter[client] = 0;

	PlayerFlags[client][0] = 0;
	PlayerFlags[client][1] = 0;

	PlayerButtons[client][0] = 0;
	PlayerButtons[client][1] = 0;

	PlayerJumpCounter[client] = 0;
}

float GetSpeed(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);

    return SquareRoot(Pow(Velocity[0], 2.0) + Pow(Velocity[1], 2.0));
}

float GetClientMovingDirection(int client, bool ladder)
{
	float Velocity[3];
    float EyeAngles[3];
    float ViewDirection[3];
	
    GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", Velocity);
	GetClientEyeAngles(client, EyeAngles);

	if(EyeAngles[0] > 70.0) EyeAngles[0] = 70.0;
	if(EyeAngles[0] < -70.0) EyeAngles[0] = -70.0;

	if (ladder)
    {
		GetEntPropVector(client, Prop_Send, "m_vecLadderNormal", ViewDirection);
    }
    else
    {
		GetAngleVectors(EyeAngles, ViewDirection, NULL_VECTOR, NULL_VECTOR);
    }

	NormalizeVector(Velocity, Velocity);
	NormalizeVector(ViewDirection, ViewDirection);

	float Direction = GetVectorDotProduct(Velocity, ViewDirection);
	if (ladder)
    {
		Direction = Direction * -1;
    }

    return Direction;
}

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}