#include <sourcemod>
#include <cstrike>
#include <sdkhooks>
#include <sdktools>
#include <player>
#include <validator>

#pragma semicolon 			    1
#pragma tabsize 			    0
#pragma newdecls required

#define MAX_SHINY_COLORS 54

// Database
Database Connection;

// Convars
ConVar CvarsSetup;
ConVar KnifeDamage;
ConVar DamageCooldown;
ConVar BeepLastPlayer;
ConVar Immortality;
ConVar HeadSplashDamage;

ConVar FunnyJumpingEnabled;
ConVar FJLastVoteRound;
ConVar FJMininumVotes;


ConVar RulesLink;

// Map
int MapRound;
int FJRequests;
int FJVoteState;
int FJNRequests;

bool IsFJRequested[MAXPLAYERS];
int FJVotes[MAXPLAYERS];
bool IsVotedForNormal[MAXPLAYERS];

// Data
int PlayerFlags[MAXPLAYERS][2];
int PlayerButtons[MAXPLAYERS][2];

int PlayerDamageCooldown[MAXPLAYERS];
bool PlayerRulesAccepted[MAXPLAYERS];

int PlayerHeadSplashCooldown[MAXPLAYERS];

// Immortality
int ImmortalityCooldown = 0;

// Beep
int BeepingClient = 0;

// Sprites
int BeamSprite        = -1;
int HaloSprite        = -1;

int ShinyPlayerColorSequence[MAX_SHINY_COLORS][4] =
{
	{255, 218, 66, 255}, // YELLOW 
	{255, 202, 66, 255},
	{255, 186, 67, 255},
	{255, 171, 67, 255},
	{255, 155, 68, 255},
	{255, 139, 68, 255},
	{255, 124, 69, 255},
	{255, 108, 69, 255},
	{255, 92, 70, 255},
	{255, 77, 71, 255}, // RED 
	{255, 92, 70, 255},
	{255, 108, 69, 255},
	{255, 124, 69, 255},
	{255, 139, 68, 255},
	{255, 155, 68, 255},
	{255, 171, 67, 255},
	{255, 186, 67, 255},
	{255, 202, 66, 255},
	{255, 218, 66, 255},// YELLOW
	{234, 203, 87, 255},
	{214, 188, 108, 255},
	{193, 174, 129, 255},
	{173, 159, 150, 255},
	{152, 144, 171, 255},
	{132, 130, 192, 255},
	{111, 115, 213, 255},
	{91, 100, 234, 255},
	{71, 86, 255, 255},	// BLUE 
	{91, 100, 234, 255},
	{111, 115, 213, 255},
	{132, 130, 192, 255},
	{152, 144, 171, 255},
	{173, 159, 150, 255},
	{193, 174, 129, 255},
	{214, 188, 108, 255},
	{214, 188, 108, 255},
	{255, 218, 66, 255}, // YELLOW 
	{236, 222, 66, 255},
	{218, 226, 67, 255},
	{200, 230, 67, 255},
	{182, 234, 68, 255},
	{164, 238, 68, 255},
	{146, 242, 69, 255},
	{128, 246, 69, 255},
	{110, 250, 70, 255},
	{92, 255, 71, 255}, // GREEN
 	{110, 250, 70, 255},
	{128, 246, 69, 255},
	{146, 242, 69, 255},
	{164, 238, 68, 255},
	{182, 234, 68, 255},
	{200, 230, 67, 255},
	{218, 226, 67, 255},
	{236, 222, 66, 255},
};

public Plugin myinfo =
{
	name = "HNS Rules",
	author = "UNFORGE LTD",
	description = "HNS Rules",
	version = "0.1",
	url = "http://unforge.net/" 
};

public void OnPluginStart()
{
	// Validate Server Files
	// Validate();

	// Translations
	LoadTranslations("hns_rules.phrases");

	// Database
	Connection = ConnectToDatabase();

	// ConVars
	CvarsSetup = CreateConVar("hns_rules_cvars", "2", "Setup server cvars (0 - off, 1 - Basic, 2 - HNS)", FCVAR_NOTIFY);
	KnifeDamage = CreateConVar("hns_unify_knife_damage", "65.0", "Override knife damage (0 - off, any - damage)", FCVAR_NOTIFY);
	DamageCooldown = CreateConVar("hns_damage_cooldown", "3", "Knife attack cooldown (0 - off, any - cooldown)", FCVAR_NOTIFY);
	BeepLastPlayer = CreateConVar("hns_beep_last_player", "1", "Should last player beep (0 - off, 1 - on)", FCVAR_NOTIFY);
	Immortality = CreateConVar("hns_immortality", "10", "Immortality on round start (0 - off, any - on)", FCVAR_NOTIFY);
	HeadSplashDamage = CreateConVar("hns_headsplash_dmg", "8.0", "Head splash damage per 0.1 second (0 - off, any - on)", FCVAR_NOTIFY);

	FunnyJumpingEnabled = CreateConVar("hns_fj_enabled", "1.0", "Is funny jumping mode is enabled (0 - off, 1 - on)", FCVAR_NOTIFY);
	FJLastVoteRound = CreateConVar("hns_fj_vote_round", "4.0", "After this round players cannot vote for fj (0 - off, 1 - on)", FCVAR_NOTIFY);
	FJMininumVotes = CreateConVar("hns_fj_min", "0.8", "Minimum precentage of players to start FJ", FCVAR_NOTIFY); 

	RulesLink = CreateConVar("hns_rules", "https://infinitycore.pl/forum/19-regulaminy/", "Rules link", FCVAR_NOTIFY); 

	AutoExecConfig(true, "hns_rules");

	// Events
	HookEvent("player_death", 		OnPlayerDeath);
    HookEvent("player_spawn",   	OnPlayerSpawn, EventHookMode_Post);
	HookEvent("round_start", 		OnRoundStart);	
	HookEvent("round_end", 			OnRoundEnd);	

	// Commands
	RegConsoleCmd("sm_fj",		cmdFunnyJumpingMode,   	"[HNS] Funny Jumping Mode Request/Menu");
	RegConsoleCmd("sm_fjn",		cmdNormalMode,   		"[HNS] Voting for normal mode");
	RegConsoleCmd("sm_rules",	cmdRules,   			"[HNS] Show rules links");

	RegAdminCmd("sm_pmove", 	cmdPlayerMove, 			ADMFLAG_GENERIC);

    
	// Late load
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
			SDKHook(client, SDKHook_Touch, OnTouch);
		}
	}

	// Timers
	CreateTimer(0.1, tDamageCooldown, _, TIMER_REPEAT);
	CreateTimer(0.1, tBeepTimer, _, TIMER_REPEAT);
	CreateTimer(1.0, tImmortalTimer, _, TIMER_REPEAT);
	CreateTimer(0.1, tNicknameTimer, _, TIMER_REPEAT);
	CreateTimer(0.1, tHeadSplashTimer, _, TIMER_REPEAT);
}

public void OnConfigsExecuted()
{
	if (CvarsSetup.IntValue == 1)
	{
		SetCvar("sv_staminalandcost", "0.0"); 
		SetCvar("sv_staminajumpcost", "0.0"); 
		SetCvar("sv_gravity", "800"); 
		SetCvar("sv_airaccelerate", "100.0"); 
		SetCvar("sv_maxspeed", "320"); 
		SetCvar("sv_wateraccelerate", "10.0"); 
		SetCvar("sv_friction", "5.0"); 
		SetCvar("sv_accelerate", "6.5"); 
		SetCvar("sv_maxvelocity", "2000.0"); 
		SetCvar("sv_enablebunnyhopping", "1"); 
		SetCvar("sv_autobunnyhopping", "0"); 
		SetCvar("sv_clamp_unsafe_velocities", "0"); 
		SetCvar("sv_jump_impulse", "301.993377"); 
		SetCvar("sv_accelerate_use_weapon_speed", "0"); 
		SetCvar("sv_ladder_scale_speed", "1.0"); 
		SetCvar("sv_timebetweenducks", "0.0"); 
	}
	else if (CvarsSetup.IntValue == 2)
	{
		SetCvar("sv_staminalandcost", "0.0"); 
		SetCvar("sv_staminajumpcost", "0.0"); 
		SetCvar("sv_gravity", "800"); 
		SetCvar("sv_airaccelerate", "300.0");  // 128 // 1000 // 13337
		SetCvar("sv_maxspeed", "320"); 
		SetCvar("sv_wateraccelerate", "10.0"); 
		SetCvar("sv_friction", "5.0"); // 3.0 // 4.5 // 5.0
		SetCvar("sv_accelerate", "6.5"); // 6 // 6.5 // xx 
		SetCvar("sv_maxvelocity", "3500.0"); 
		SetCvar("sv_enablebunnyhopping", "1"); // 0
		SetCvar("sv_autobunnyhopping", "0"); 
		SetCvar("sv_clamp_unsafe_velocities", "0"); 
		SetCvar("sv_jump_impulse", "301.993377"); 
		SetCvar("sv_accelerate_use_weapon_speed", "0");  // 0
		SetCvar("sv_ladder_scale_speed", "1.0"); 
		SetCvar("sv_timebetweenducks", "0.0"); 
		
	}
}

public void OnMapStart()
{
	// Globals
	MapRound = 0;
	FJRequests = 0;
	FJNRequests = 0;
	FJVoteState = 0;

	for (int client = 0; client < MAXPLAYERS; client++)
	{
		IsFJRequested[client] = false;
		FJVotes[client] = 0;
		IsVotedForNormal[client] = false;
	}

	// Sprite
	BeamSprite = PrecacheModel("sprites/laserbeam.vmt");
	HaloSprite = PrecacheModel("sprites/glow01.vmt");

	// Sound
    PrecacheSound("buttons/button17.wav", true);
}

public void OnClientPutInServer(int client)
{
    if (IsValidClient(client))
    {
		// Default Data
		PlayerFlags[client][0] = 0;
		PlayerFlags[client][1] = 0;

		PlayerButtons[client][0] = 0;
		PlayerButtons[client][1] = 0;

		PlayerDamageCooldown[client] = 0;

		PlayerRulesAccepted[client] = false;

		PlayerHeadSplashCooldown[client] = 0;

		// Hooks
        SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
		SDKHook(client, SDKHook_Touch, OnTouch);
    }
}

public Action OnTouch(int client, int target)
{
	if (HeadSplashDamage.FloatValue <= 0.0)
	{
		return Plugin_Continue;
	}

	// Disable headsplash in warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
	{
		return Plugin_Continue;
	}

	if (ImmortalityCooldown > 0) return Plugin_Continue;

	if 
	(
		IsValidClient(client, true) && 
		IsValidClient(target, true) && 
		GetClientTeam(client) != GetClientTeam(target) && 
		GetClientTeam(client) == CS_TEAM_T && 
		GetEntityMoveType(target) != MOVETYPE_LADDER
	)
	{
		float PlayerOrigin[3];
		float TargetOrigin[3];

		GetClientAbsOrigin(client, PlayerOrigin);
		GetClientAbsOrigin(target, TargetOrigin);

		if (PlayerOrigin[2] > TargetOrigin[2])
		{	
			float MinDistance = 71.0;
			float Distance = PlayerOrigin[2] - TargetOrigin[2];

			if (GetEntityFlags(target) & FL_DUCKING)
			{
				MinDistance = 53.0;
			}

			if (MinDistance < Distance)
			{
				if (PlayerHeadSplashCooldown[target] == 0)
				{
					SDKHooks_TakeDamage(target, client, client, HeadSplashDamage.FloatValue, DMG_GENERIC, 0);
					PlayerHeadSplashCooldown[target] = 1;
				}
			}
		}
	}

	return Plugin_Continue;
}

public Action OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	MapRound++;

	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			PlayerDamageCooldown[client] = 0;
			PlayerHeadSplashCooldown[client] = 0;

			if (GetClientTeam(client) == CS_TEAM_T)
			{
				SetPlayerColor(client, 255, 150, 150);
			}

			if (GetClientTeam(client) == CS_TEAM_CT)
			{
				SetPlayerColor(client, 150, 150, 255);
			}
		}
	}

	BeepingClient = 0;

	if (Immortality.IntValue > 0)
	{
		ImmortalityCooldown = Immortality.IntValue + 1; // +1 fix
		PrintToChatAll(" [\x07HNS\x01] %t", "Immortal_ON");
	}
}

public Action OnPlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
   
    if (IsValidClient(client, true))
    {  
		if (!PlayerRulesAccepted[client])
		{
			if (GetClientStatus(client) > 0)
			{
				PlayerRulesAccepted[client] = true;
			}
			else
			{
				OpenRulesMenu(client);
			}
		}

		PlayerHeadSplashCooldown[client] = 0;
	
		// No Block FJ
		if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
		{
			SetEntData(client, FindSendPropInfo("CBaseEntity", "m_CollisionGroup"), 2, 4, true);
		}

		if (GetClientTeam(client) == CS_TEAM_T)
		{
			SetPlayerColor(client, 255, 150, 150);
		}

		if (GetClientTeam(client) == CS_TEAM_CT)
		{
			SetPlayerColor(client, 150, 150, 255);
		}
		
		/*
		char ModelName[128];
		char ArmsName[128];

		GetEntPropString(client, Prop_Data, "m_ModelName", ModelName, PLATFORM_MAX_PATH);
		PrintToServer("[MODEL] %s", ModelName);

		GetEntPropString(client, Prop_Send, "m_szArmsModel", ArmsName, PLATFORM_MAX_PATH);
		PrintToServer("[ARMS] %s", ArmsName);
		*/
    }					
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype)
{
	// Disable weapon damage and "no fall damage" in warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
	{
		if (damagetype == 4100)
		{
			damage = 0.0;
			return Plugin_Changed;
		}

		return Plugin_Continue;
	}

	if (IsValidClient(victim))
	{
		if (ImmortalityCooldown > 0)
		{
			damage = 0.0;
			return Plugin_Changed;
		}
	}

	if (IsValidClient(attacker) && IsValidClient(victim) && GetClientTeam(attacker) != GetClientTeam(victim))
	{
		// Knifes if (damagetype == 4100)
		// Granades if (damagetype == 64)
		// Fall if (damagetype == 32)
		// Taser if (damagetype == 4352)
		// Headshot if (damagetype == 1073745922)

		if (damagetype == 4100)
		{
			if (DamageCooldown.IntValue && PlayerDamageCooldown[victim])
			{
				damage = 0.0;
				PrintToChat(attacker, " [\x07HNS\x01] %t", "Immortal_Info_1", PlayerDamageCooldown[victim] / 10.0);
				return Plugin_Changed;
			}
			else
			{
				if (DamageCooldown.IntValue)
				{
					PlayerDamageCooldown[victim] = DamageCooldown.IntValue * 10;
					SetPlayerColor(victim, 150, 255, 150);
					PrintToChat(victim, " [\x07HNS\x01] %t", "Immortal_Info_2", DamageCooldown.FloatValue);
				}

				if (KnifeDamage.FloatValue)
				{
					damage = KnifeDamage.FloatValue;
					return Plugin_Changed;
				}
			}
			
		}
	}

	return Plugin_Continue;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
    if (!IsValidClient(client, true)) return Plugin_Continue;

	PlayerFlags[client][0] = GetEntityFlags(client);
	PlayerButtons[client][0] = GetClientButtons(client);

	// Disable weapon attack in warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
	{
		float CurrentTime = GetGameTime();

		int ActiveWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
        if(IsValidEntity(ActiveWeapon)) 
		{
      		SetEntPropFloat(ActiveWeapon, Prop_Send, "m_flNextPrimaryAttack", CurrentTime + 9001.0);
        	SetEntPropFloat(ActiveWeapon, Prop_Send, "m_flNextSecondaryAttack", CurrentTime + 9001.0);
		}

		buttons &= ~(IN_ATTACK | IN_ATTACK2);
		return Plugin_Changed;
	}

	PlayerFlags[client][1] = PlayerFlags[client][0];
	PlayerButtons[client][1] = PlayerButtons[client][0];

	return Plugin_Continue;
}

public Action OnPlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int victim 	= GetClientOfUserId(GetEventInt(event, "userid"));

	if (IsValidClient(victim))
	{
		PlayerDamageCooldown[victim] = 0;
	}
}

public Action OnRoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	BeepingClient = 0;
}

// Menus
void OpenRulesMenu(int client)
{
    char MenuText[32];

    Menu menu = new Menu(mRulesMenu);
    menu.SetTitle("%T", "Rules_Title", client);
   
    Format(MenuText, sizeof(MenuText), "%T", "Rules_Yes", client);
    menu.AddItem("yes", MenuText);
    
	Format(MenuText, sizeof(MenuText), "%T", "Rules_Yes_Save", client);
    menu.AddItem("save", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Rules_No", client);
    menu.AddItem("no", MenuText);
	
	menu.ExitButton = false;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mRulesMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "yes"))
        {
			PlayerRulesAccepted[param1] = true;
        }
        else if (!strcmp(ChoosedMenu, "no"))
        { 
			KickClientEx(param1, "%T", "Rules_Kicked", param1);
        }
        else if (!strcmp(ChoosedMenu, "save"))
        {
			SetClientStatus(param1, 1);
			PlayerRulesAccepted[param1] = true;
        }
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// Timers
public Action tImmortalTimer(Handle timer)
{	
	if (ImmortalityCooldown > 0)
	{
		ImmortalityCooldown--;

		if (ImmortalityCooldown == 0)
		{
			PrintToChatAll(" [\x07HNS\x01] %t", "Immortal_OFF");
		}
	}
}

public Action tNicknameTimer(Handle timer)
{
	static int Cooldown[MAXPLAYERS];
	static char NicknameToPrint[MAXPLAYERS][64];
	static int Type[MAXPLAYERS];

	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client, true))
		{
			float PlayerEyeOrigin[3];
			float PlayerAngle[3];

			GetClientEyePosition(client, PlayerEyeOrigin);
			GetClientEyeAngles(client, PlayerAngle);

			Handle TargetTrace = TR_TraceRayFilterEx(PlayerEyeOrigin, PlayerAngle, MASK_SHOT, RayType_Infinite, PlayerFilter, client);
			if (TR_DidHit(TargetTrace))
			{
				int Target = TR_GetEntityIndex(TargetTrace);

				if (Target > 0)
				{
					int ClientTeam = GetClientTeam(client);
					int TargetTeam = GetClientTeam(Target);

					GetClientName(Target, NicknameToPrint[client], 64);
										
					if (ClientTeam == TargetTeam)
					{
						Type[client] = 0;
					}
					else
					{
						Type[client] = 1;
					}

					Cooldown[client] = 5;
				}
			}
			delete TargetTrace;

			if (Cooldown[client] > 0)
			{				
				if (Type[client] == 0)
				{
					SetHudTextParams(-1.0, 0.45, 0.2, 100, 100, 255, 180, 0, 0.0, 0.0, 0.0);
				}
				else
				{
					SetHudTextParams(-1.0, 0.45, 0.2, 255, 100, 100, 180, 0, 0.0, 0.0, 0.0);
				}
				ShowHudText(client, -1, NicknameToPrint[client]);

				Cooldown[client]--;
			}
		}
	}
}

bool PlayerFilter(int entity, int contentsMask, any data)
{
    if (entity == data || !IsValidClient(entity, true)) return false;
    
    return true;
}

public Action tHeadSplashTimer(Handle timer)
{
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client, true))
		{
			if (PlayerHeadSplashCooldown[client] > 0)
			{
				PlayerHeadSplashCooldown[client]--;
			}						
		}
	}
}

public Action tDamageCooldown(Handle timer)
{
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			if (DamageCooldown.IntValue && PlayerDamageCooldown[client])
			{
				PlayerDamageCooldown[client]--;

				if (!PlayerDamageCooldown[client])
				{
					if (GetClientTeam(client) == CS_TEAM_T)
					{
						SetPlayerColor(client, 255, 150, 150);
					}

					if (GetClientTeam(client) == CS_TEAM_CT)
					{
						SetPlayerColor(client, 150, 150, 255);
					}

					PrintToChat(client, " [\x07HNS\x01] %t!", "Immortal_Removed");
				}
			}
		}
	}
}

public Action tBeepTimer(Handle timer)
{
	static int Tick = 0;

	// Don't beep in warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
	{
		return Plugin_Continue;
	}

	if (BeepLastPlayer.IntValue)
	{
		int AliveCTCount = 0;
		int AliveTCount = 0;
		int LastAliveT = 0;
		int ClientCount = 0;

		// 

		for (int client = 0; client < MAXPLAYERS; client++)
		{
			if (IsValidClient(client))
			{
				if (IsPlayerAlive(client))
				{
					if (GetClientTeam(client) == CS_TEAM_CT)
					{
						AliveCTCount++;
					}
					else if (GetClientTeam(client) == CS_TEAM_T)
					{
						AliveTCount++;
						LastAliveT = client;
					}
				}

				ClientCount++;
			}
		}

		if (ClientCount > 3 && AliveCTCount > 0 && AliveTCount == 1)
		{
			BeepingClient = LastAliveT;
		}

		if (IsValidClient(BeepingClient))
		{
			if (IsPlayerAlive(BeepingClient))
			{
				if (AliveCTCount == 1)
				{
					SetPlayerColor(BeepingClient, ShinyPlayerColorSequence[Tick%MAX_SHINY_COLORS][0], ShinyPlayerColorSequence[Tick%MAX_SHINY_COLORS][1], ShinyPlayerColorSequence[Tick%MAX_SHINY_COLORS][2]);
				}

				if (Tick % 20 == 0)
				{
					float PlayerOrigin[3];
					GetClientAbsOrigin(BeepingClient, PlayerOrigin);

					PlayerOrigin[2] += 45.0;

					EmitAmbientSound("buttons/button17.wav", PlayerOrigin, BeepingClient, SNDLEVEL_RAIDSIREN);

					TE_SetupBeamRingPoint(PlayerOrigin, 10.0, 500.0, BeamSprite, HaloSprite, 0, 15, 0.5, 5.0, 0.0, {192, 48, 40, 255}, 10, 0); 
					TE_SendToAll(); 
				}
			}	
			else
			{
				BeepingClient = 0;
			}
		}
	}

	Tick++;
	return Plugin_Continue;
}

public float GetSpeed(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);

	return SquareRoot(Pow(Velocity[0], 2.0) + Pow(Velocity[1], 2.0));
}

public float GetVelocity(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);

	return SquareRoot(Pow(Velocity[0], 2.0)+Pow(Velocity[1], 2.0) + Pow(Velocity[2], 2.0));
}

void SetCvar(char[] scvar, char[] svalue)
{
	Handle cvar = FindConVar(scvar);
	SetConVarString(cvar, svalue, true);
}
 
/* ======================================================= */
/* =============== Commands & Menus ====================== */
/* ======================================================= */
public Action cmdPlayerMove(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
    OpenPlayerMoveMenu(client);

	return Plugin_Handled;
}

void OpenPlayerMoveMenu(int client)
{
    char MenuText[128];

    Menu menu = new Menu(mPlayerMoveTeam);
    menu.SetTitle("%T:", "Move_Player_Title", client);
    
    Format(MenuText, sizeof(MenuText), "%T", "Move_Player_CT", client);
    menu.AddItem("to_ct", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Move_Player_T", client);
    menu.AddItem("to_t", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mPlayerMoveTeam(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "to_ct"))
        {
			char nickname[32];
			char index[8];

			Menu menu2 = new Menu(mPlayerMoveCT);
			menu2.SetTitle("%T:", "Move_Players", param1);
		
			for (int Target = 0; Target < MAXPLAYERS; Target++)
			{
				if (IsValidClient(Target) && GetClientTeam(Target) == CS_TEAM_T)
				{
					GetClientName(Target, nickname, sizeof(nickname));
					IntToString(Target, index, sizeof(index));
					
					menu2.AddItem(index, nickname);
				}
			}

			menu2.ExitButton = true;
			menu2.Display(param1, MENU_TIME_FOREVER);
        }
		else if (!strcmp(ChoosedMenu, "to_t"))
        {
			char nickname[32];
			char index[8];

			Menu menu2 = new Menu(mPlayerMoveT);
			menu2.SetTitle("%T:", "Move_Players", param1);
		
			for (int Target = 0; Target < MAXPLAYERS; Target++)
			{
				if (IsValidClient(Target) && GetClientTeam(Target) == CS_TEAM_CT)
				{
					GetClientName(Target, nickname, sizeof(nickname));
					IntToString(Target, index, sizeof(index));
					
					menu2.AddItem(index, nickname);
				}
			}

			menu2.ExitButton = true;
			menu2.Display(param1, MENU_TIME_FOREVER);
        }
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public int mPlayerMoveT(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

		int Target = StringToInt(ChoosedMenu);

		if (IsValidClient(Target))
		{
			ChangeClientTeam(Target, CS_TEAM_T);
		}
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
			OpenPlayerMoveMenu(param1);
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public int mPlayerMoveCT(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

		int Target = StringToInt(ChoosedMenu);

		if (IsValidClient(Target))
		{
			ChangeClientTeam(Target, CS_TEAM_CT);
		}
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
			OpenPlayerMoveMenu(param1);
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

/* ======================================================= */
/* =================== Database ========================== */
/* ======================================================= */
Database ConnectToDatabase()
{
	char error[128];
	Database conn = SQL_Connect("hns_rules", false, error, sizeof(error));
		
	if (conn == null) 
	{
		PrintToServer("[HNS RULES]: Could not connect to Database: %s", error);
		return null;
	}
	
	PrintToServer("[HNS RULES]: Database connected. ");
	SQL_FastQuery(conn, "CREATE TABLE IF NOT EXISTS `hns_rules` (`steamid` VARCHAR(32) NOT NULL, `status` INT(11), PRIMARY KEY (`steamid`));");
	PrintToServer("[HNS RULES]: Table creating if not exist. "); 	

	return conn;
}

void SetClientStatus(int client, int status)
{
	if (Connection == null)	
	{
		PrintToServer("[HNS RULES]: Could not connect."); 
		return ;
	}

	char auth[32];
	char query[256];
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

	Format(query, sizeof(query), "REPLACE INTO `hns_rules` VALUES ('%s', %d);", auth, status);
	SQL_FastQuery(Connection, query);
}

int GetClientStatus(int client)
{
	if (Connection == null)	
	{
		PrintToServer("[HNS RULES]: Could not connect."); 
		return 0;
	}
	
	char auth[32];
	char nick[64];
	char error[256];
	char query[256];
	DBResultSet hQuery;

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
	
	Format
	(
		query, 
		sizeof(query), 
		"SELECT `status` FROM `hns_rules` WHERE `steamid` = '%s';", 
		auth
	);
	hQuery = SQL_Query(Connection, query);
	
	if (hQuery == null)
	{
		SQL_GetError(Connection, error, sizeof(error));
		PrintToServer("[HNS RULES]: Failed to query (error: %s)(%s)[%s]", error, nick, auth);
		
		delete hQuery;
		return 0;
	}

	if (SQL_FetchRow(hQuery))
	{
		int Result = SQL_FetchInt(hQuery, 0);
		
		delete hQuery;
		return Result;
	}
	
	delete hQuery;
	return 0;
}


/* ======================================================= */
/* =============== Funny Jump Mode ======================= */
/* ======================================================= */
public Action cmdFunnyJumpingMode(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	if (!FunnyJumpingEnabled.IntValue) return Plugin_Handled;
	
	if (GameRules_GetProp("m_bWarmupPeriod") == 1) 
	{
		FakeClientCommandEx(client, "sm_fjm");
	}
	else
	{
		if (FJVoteState == 1)
		{
			OpenFjVoteMenu(client); // Revote
			return Plugin_Handled;
		}

		if (FJVoteState == 2)
		{
			PrintToChat(client, "[\x07HNS\x01] %t", "FJ_Vote_Been_Ended");
			return Plugin_Handled;
		}

		if (MapRound <= FJLastVoteRound.IntValue)
		{
			if (!IsFJRequested[client])
			{
				FJRequests++;
				IsFJRequested[client] = true;
			}
			else
			{
				PrintToChat(client, "[\x07HNS\x01] %t", "FJ_Info_2", FJRequests, RoundFloat(GetClientCount() * FJMininumVotes.FloatValue));
			}

			if (FJRequests >= RoundFloat(GetClientCount() * FJMininumVotes.FloatValue))
			{
				OpenFjVoting(5.0);
				PrintToChatAll(" [\x07HNS\x01] %t", "FJ_Success", 5);
			}
			else
			{
				PrintToChat(client, " [\x07HNS\x01] %t", "FJ_Info_1", FJRequests, RoundFloat(GetClientCount() * FJMininumVotes.FloatValue));
			}
		}
		else
		{
			PrintToChat(client, "[\x07HNS\x01] %t", "FJ_Max_Round", FJLastVoteRound.IntValue);
		}
	}

	return Plugin_Handled;
}

void OpenFjVoting(float delay)
{
	FJVoteState = 1;

	CreateTimer(delay, tOpenFjVotingTimer, _);
	PrintToChatAll("[\x07HNS\x01] %t", "FJ_Vote_Starts", RoundFloat(delay));
}

public Action tOpenFjVotingTimer(Handle timer, int entity)
{
	CreateTimer(20.0, tFjVotingTimer, _);

	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			OpenFjVoteMenu(client);
		}
	}

	PrintToChatAll("[\x07HNS\x01] %t", "FJ_Vote_Started");
}

public Action tFjVotingTimer(Handle timer, int entity)
{
	FJVoteState = 2;


	int YesVotes = 0;
	int NoVotes = 0;
	int ShortTime = 0;
	int MiddleTime = 0;
	int LongTime = 0;

	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
		{
			switch (FJVotes[client])
			{
				case 1:
				{
					NoVotes++;
				}
				case 2:
				{
					ShortTime++;
					YesVotes++;
				}
				case 3:
				{
					MiddleTime++;
					YesVotes++;
				}
				case 4:
				{
					LongTime++;
					YesVotes++;
				}
			}
		}
	}

	if (NoVotes > YesVotes)
	{
		PrintToChatAll("[\x07HNS\x01] %t", "FJ_Vote_Result_No", YesVotes, NoVotes);
	}
	else
	{
		int Time = 15;
		if (LongTime >= MiddleTime && LongTime >= ShortTime)
		{
			Time = 15;
		}
		else if (MiddleTime >= LongTime && MiddleTime >= ShortTime)
		{
			Time = 10;
		}
		else if (ShortTime >= LongTime && ShortTime >= MiddleTime)
		{
			Time = 5;
		}

		PrintToChatAll("[\x07HNS\x01] %t", "FJ_Vote_Result_Yes", YesVotes, NoVotes);
		PrintToChatAll("[\x07HNS\x01] %t", "FJ_Vote_Result_Time", Time);

		ServerCommand("mp_warmuptime %d", Time * 60);
		ServerCommand("mp_warmup_start");
	}
}

void OpenFjVoteMenu(int client)
{
	if (FJVoteState == 0)
	{
		PrintToChat(client, "[\x07HNS\x01] %t", "FJ_Vote_Not_Started");
		return;
	}

	if (FJVoteState == 2)
	{
		PrintToChat(client, "[\x07HNS\x01] %t", "FJ_Vote_Been_Ended");
		return;
	}

    char MenuText[128];

    Menu menu = new Menu(mFjVoteMenu);
    menu.SetTitle("%T", "FJ_Title", client);
    
    Format(MenuText, sizeof(MenuText), "%T", "FJ_Vote_No", client);
    menu.AddItem("no", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "FJ_Vote_5", client);
    menu.AddItem("5", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "FJ_Vote_10", client);
    menu.AddItem("10", MenuText);

	Format(MenuText, sizeof(MenuText), "%T", "FJ_Vote_15", client);
    menu.AddItem("15", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mFjVoteMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "no"))
        {
			FJVotes[param1] = 1;
        }
		else if (!strcmp(ChoosedMenu, "5"))
        {
			FJVotes[param1] = 2;
        }
		else if (!strcmp(ChoosedMenu, "10"))
        {
			FJVotes[param1] = 3;
        }
		else if (!strcmp(ChoosedMenu, "15"))
        {
			FJVotes[param1] = 4;
        }
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public Action cmdRules(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;

	char Link[128];

	RulesLink.GetString(Link, sizeof(Link));

	PrintToChat(client, " [\x07HNS\x01] %t \x06%s\x01", "Rules_Link_A", Link);

	return Plugin_Handled;
}

public Action cmdNormalMode(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	if (!FunnyJumpingEnabled.IntValue) return Plugin_Handled;
	if (GameRules_GetProp("m_bWarmupPeriod") != 1) return Plugin_Handled;

	if (IsVotedForNormal[client])
	{
		PrintToChat(client, "[\x07HNS\x01] %t", "NM_Info_2", FJNRequests, RoundFloat(GetClientCount() * FJMininumVotes.FloatValue));
	}
	else
	{
		FJNRequests++;
		IsVotedForNormal[client] = true;

		if (FJNRequests >= RoundFloat(GetClientCount() * FJMininumVotes.FloatValue))
		{
			TurnOnNormalMode(5.0);
			PrintToChatAll(" [\x07HNS\x01] %t", "NM_Success", 5);
		}
		
		PrintToChat(client, " [\x07HNS\x01] %t", "NM_Info_1", FJNRequests, RoundFloat(GetClientCount() * FJMininumVotes.FloatValue));
	}

	return Plugin_Handled;
}

void TurnOnNormalMode(float delay)
{
	CreateTimer(delay, tTurnOnNormalMode, _);
}

public Action tTurnOnNormalMode(Handle timer, int entity)
{
	ServerCommand("mp_warmup_end");
}