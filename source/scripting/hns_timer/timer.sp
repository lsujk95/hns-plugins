void InitTimer()
{
    // Timers
    CreateTimer(0.1, tShowHUD, _, TIMER_REPEAT);
    CreateTimer(1.0, tAntiCheat, _, TIMER_REPEAT);
	CreateTimer(1.0, tJumpBugCooldown, _, TIMER_REPEAT);
}
   
public Action tJumpBugCooldown(Handle timer)
{
	if (g_JumpBugCooldown > 0)
	{
		g_JumpBugCooldown--;
	}

    for (int client = 0; client < MAXPLAYERS; client++)
    {
        if (IsValidClient(client))
        {
            if (g_PlayerJumpbugCooldown[client] > 0)
            {
                g_PlayerJumpbugCooldown[client]--;
            }
        }
    }
}

// Used to draw center hud
public Action tShowHUD(Handle timer)
{
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client))
        {
            // Center Panel
            if (g_bCenterPanel[client])
            {
                int Target = client;

                if (!(IsPlayerAlive(client) || g_bPlayerGhostMode[client]))
                {
                    Target = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
                }
                
                if (IsValidClient(Target, true) || (IsValidClient(Target) && g_bPlayerGhostMode[client]))
                {
                    // Draw panel
                    DrawCenterPanel(client, Target);
                }

                // Bhop plattform & movement direction
                if (g_bOnGround[client])
                {
                    g_js_TotalGroundFrames[client]++;
                }
                else
                {
                    if(g_bLadderJump[client])
                    {
                        g_js_fLadderDirection[client] += GetClientMovingDirection(client, true);
                        g_js_LadderDirectionCounter[client]++;						
                    }		
                    g_fMovingDirection[client] += GetClientMovingDirection(client, false);
                    g_js_TotalGroundFrames[client] = 0;				
                }
                if (g_js_TotalGroundFrames[client] > 1 && g_bOnBhopPlattform[client])	
                {
                    g_bOnBhopPlattform[client] = false;	
                }
            }

            // Spectate List
            if (g_bEnableSpecList[client])
            {
                char SpecList[256];
                Format(SpecList, sizeof(SpecList), "%T:", "Spec_List", client);

                int SpecTarget = client;
                if (!(IsPlayerAlive(client) || g_bPlayerGhostMode[client]))
                {
                    SpecTarget = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
                }

                for (int Target = 0; Target < MAXPLAYERS; Target++)
                {
                    if (IsValidClient(Target) && !IsPlayerAlive(Target))
                    {
                        int TargetSpecTarget = GetEntPropEnt(Target, Prop_Send, "m_hObserverTarget");

                        if (SpecTarget == TargetSpecTarget)
                        {
                            if (GetClientTeam(Target) == CS_TEAM_SPECTATOR && (GetUserFlagBits(Target) & ADMFLAG_GENERIC))
                            {
                                continue;
                            }  

                            char Nickname[64];
                            GetClientName(Target, Nickname, sizeof(Nickname));

                            Format(SpecList, sizeof(SpecList), "%s\n - %s", SpecList, Nickname);
                        }
                    }
                }

			    SetHudTextParams(0.01, 0.02, 0.2, 100, 255, 100, 180, 0, 0.0, 0.0, 0.0);
			    ShowHudText(client, -1, SpecList);
            }
                
            // Checks
            SurfCheck(client);
            MovementCheck(client);

		}
    }
}

public Action BhopCheck(Handle timer, int client)
{
	if (!g_js_bBhop[client])
    {
		g_js_GODLIKE_Count[client] = 0;
    }
}

public Action tAntiCheat(Handle timer)
{
    for (int client = 0; client < MAXPLAYERS; client++)
	{
        if (IsValidClient(client))
        {
            BhopPatternCheck(client);
        }
    }
}