void InitCommands()
{
    RegConsoleCmd("sm_options",     cmdSettings, "[HNS] User settings");
    RegConsoleCmd("sm_opcje",       cmdSettings, "[HNS] User settings");

    RegConsoleCmd("sm_settings",       cmdSettings, "[HNS] User settings");
    RegConsoleCmd("sm_options",       cmdSettings, "[HNS] User settings");
    RegConsoleCmd("sm_setup",       cmdSettings, "[HNS] User settings");

    RegConsoleCmd("sm_toggleweapon",  cmdToggleWeaponModels, "[HNS] Hides/Unhides your weapon model");
    RegConsoleCmd("sm_toggleknife",    cmdToggleWeaponModels, "[HNS] Hides/Unhides your weapon model");

    RegConsoleCmd("sm_top",         cmdTop, "[HNS] Jump top"); 
    RegConsoleCmd("sm_jumptop",     cmdTop, "[HNS] Jump top"); 

    RegConsoleCmd("sm_players",     cmdPlayers, "[HNS] Players list"); 
    RegConsoleCmd("sm_gracze",     cmdPlayers, "[HNS] Players list"); 
    RegConsoleCmd("sm_stats",       cmdStats, "[HNS] Jump stats");    
    RegConsoleCmd("sm_rank",        cmdRank, "[HNS] Jump ranks"); 
    RegConsoleCmd("sm_jumpstats",   cmdStats, "[HNS] Jump stats"); 

    RegConsoleCmd("sm_ljblock",     cmdLJBlock, "[HNS] Registers a lj block");
	RegConsoleCmd("sm_measure",     cmdMeasure, "[HNS] Allows you to measure the distance between 2 points");
	RegConsoleCmd("sm_dist",        cmdMeasure, "[HNS] Allows you to measure the distance between 2 points");
    RegConsoleCmd("sm_bhopcheck",   cmdBHopeCheck,   "[HNS] Checks bhop stats for a given player");

    RegConsoleCmd("sm_fjm",             cmdFunnyJump,   "[HNS] Funny Jumping Menu");
    RegConsoleCmd("sm_funnyjumpmenu",   cmdFunnyJump,   "[HNS] Funny Jumping Menu");
    RegConsoleCmd("sm_funnymenu",       cmdFunnyJump,   "[HNS] Funny Jumping Menu");

    RegConsoleCmd("sm_redie",           cmdRedie,   "[HNS] GhostMode"); 
    RegConsoleCmd("sm_ghost",           cmdRedie,   "[HNS] GhostMode");

    RegConsoleCmd("sm_fov",             cmdFOV,     "[HNS] Setup FOV");

	RegAdminCmd("sm_nc", cmdNoClip,  ADMFLAG_GENERIC);

}

// Stats
public Action cmdPlayers(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
    
    OpenPlayersMenu(client);

	return Plugin_Handled;
}

void OpenPlayersMenu(int client)
{
    // Last menu info
    g_LastMenu[client] = MENU_PLAYERS;

    char auth[32];
    char nickname[32];

    Menu menu = new Menu(mPlayersMenu);
    menu.SetTitle("%T:", "Menu_Players", client);
   
    for (int Target = 0; Target < MAXPLAYERS; Target++)
    {
        if (IsValidClient(Target))
        {
            GetClientName(Target, nickname, sizeof(nickname));
            GetClientAuthId(Target, AuthId_Steam2, auth, sizeof(auth), true);

            menu.AddItem(auth, nickname);
        }
    }

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mPlayersMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        Format(g_LastStatsTarget[param1], 64, "%s", ChoosedMenu); 
        OpenStatsMenu(param1, ChoosedMenu);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}


public Action cmdRank(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
    
    OpenRankMenu(client);

	return Plugin_Handled;
}

public int mRankMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
        OpenRankMenu(param1);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public Action cmdStats(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
	if (args > 1)
	{
        ReplyToCommand(client, "%t", "Stats_Usage", MOSSGREEN, WHITE);
        return Plugin_Handled;
	}
    
    char auth[32];

    if (args == 1)
    {
       	char buffer[100];
        GetCmdArg(1, buffer, sizeof(buffer));

        int target = FindTarget(client, buffer, true);
        if (target == -1)
        {
            // ReplyToCommand(client, "%t", "Stats_InvalidParam", MOSSGREEN, WHITE);
            return Plugin_Handled;
        }

        GetClientAuthId(target, AuthId_Steam2, auth, sizeof(auth), true);
        Format(g_LastStatsTarget[client], 64, "%s", auth);

        OpenStatsMenu(client, auth);
        return Plugin_Handled;
    }

    GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
    Format(g_LastStatsTarget[client], 64, "%s", auth); 

    OpenStatsMenu(client, auth);
	return Plugin_Handled;
}

public int mStatsMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
        OpenStatsMenu(param1, g_LastStatsTarget[param1]);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
            if (g_LastMenu[param1] == MENU_PLAYERS)
            {
                OpenPlayersMenu(param1);
            }
            else
            {
                OpenTop20Menu(param1, g_LastTopMenuType[param1]);
            }
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// Top
public Action cmdTop(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
    OpenTopMenu(client);

	return Plugin_Handled;
}

void OpenTopMenu(int client)
{
    char MenuText[32];

    g_LastMenu[client] = MENU_TOP_CATEGORIES;

    Menu menu = new Menu(mTopMenu);
    menu.SetTitle("Top 20:");
   
    Format(MenuText, sizeof(MenuText), "%T", "Top20_Longjump", client);
    menu.AddItem("lj", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_BlockLongjump", client);
    menu.AddItem("blocklj", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_Bhop", client);
    menu.AddItem("bhop", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_Multibhop", client);
    menu.AddItem("multibh", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_Weirdjump", client);
    menu.AddItem("wj", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_Ladderjump", client);
    menu.AddItem("ladder", MenuText);
    
    Format(MenuText, sizeof(MenuText), "%T", "Top20_DropBhop", client);
    menu.AddItem("dropbh", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_Countjump", client);
    menu.AddItem("cj", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Top20_JumpBug", client);
    menu.AddItem("jb", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mTopMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "lj"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_LJ;
            OpenTop20Menu(param1, JUMPSTATS_LJ);
        }
        else if (!strcmp(ChoosedMenu, "blocklj"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_BLOCKLJ;
            OpenTop20Menu(param1, JUMPSTATS_BLOCKLJ);   
        }
        else if (!strcmp(ChoosedMenu, "bhop"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_BHOP;
            OpenTop20Menu(param1, JUMPSTATS_BHOP);
        }
        else if (!strcmp(ChoosedMenu, "multibh"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_MULTIBH;
            OpenTop20Menu(param1, JUMPSTATS_MULTIBH);
        }
        else if (!strcmp(ChoosedMenu, "wj"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_WJ;
            OpenTop20Menu(param1, JUMPSTATS_WJ);
        }
        else if (!strcmp(ChoosedMenu, "dropbh"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_DROPBH;
            OpenTop20Menu(param1, JUMPSTATS_DROPBH);
        }
        else if (!strcmp(ChoosedMenu, "cj"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_CJ;
            OpenTop20Menu(param1, JUMPSTATS_CJ);
        }
        else if (!strcmp(ChoosedMenu, "ladder"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_LADDER;
            OpenTop20Menu(param1, JUMPSTATS_LADDER);
        }
        else if (!strcmp(ChoosedMenu, "jb"))
        {
            g_LastTopMenuType[param1] = JUMPSTATS_JUMPBUG;
            OpenTop20Menu(param1, JUMPSTATS_JUMPBUG);
        }
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public int mTopCategoryMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
        char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));
        Format(g_LastStatsTarget[param1], 64, "%s", ChoosedMenu); 
        
        OpenStatsMenu(param1, ChoosedMenu);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
            OpenTopMenu(param1);
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// LJ block
public Action cmdLJBlock(int client, int args)
{
	if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))) return Plugin_Handled;
	
    OpenLJBlockMenu(client);

	return Plugin_Handled;
}

void OpenLJBlockMenu(int client)
{
    char MenuText[32];

    Menu menu = new Menu(mLJBlockMenu);
    menu.SetTitle("%T", "BlockMenuTitle", client);
   
    Format(MenuText, sizeof(MenuText), "%T", "BlockMenu_SelectDest", client);
    menu.AddItem("set", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "BlockMenu_ResetDest", client);
    menu.AddItem("reset", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mLJBlockMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "set"))
        {
            BlockJump(param1);
        }
        else if (!strcmp(ChoosedMenu, "reset"))
        {
            g_bLJBlock[param1] = false;
        }

        OpenLJBlockMenu(param1);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// Measure
public Action cmdMeasure(int client, int args)
{
	if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))) return Plugin_Handled;
	
    OpenMeasureMenu(client);

	return Plugin_Handled;
}

void OpenMeasureMenu(int client)
{
    char MenuText[32];

    Menu menu = new Menu(mMeasureMenu);
    menu.SetTitle("%T", "MeasureMenuTitle", client);
   
    Format(MenuText, sizeof(MenuText), "%T", "Measure_Point1", client);
    menu.AddItem("point1", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Measure_Point2", client);
    menu.AddItem("point2", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Measure_FindDist", client);
    menu.AddItem("find", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "Measure_Reset", client);
    menu.AddItem("reset", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mMeasureMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "point1"))
        {
			GetPos(param1, 0);
        }
        else if (!strcmp(ChoosedMenu, "point2"))
        {
            GetPos(param1, 1);
        }
        else if (!strcmp(ChoosedMenu, "find"))
        {
            if(g_bMeasurePosSet[param1][0] && g_bMeasurePosSet[param1][1])
			{
                float vDist = GetVectorDistance(g_fvMeasurePos[param1][0],g_fvMeasurePos[param1][1]);
                float vHightDist = (g_fvMeasurePos[param1][0][2] - g_fvMeasurePos[param1][1][2]);

                PrintToChat(param1, "%t", "Measure1", MOSSGREEN, WHITE, vDist, vHightDist);
                Beam(param1, g_fvMeasurePos[param1][0], g_fvMeasurePos[param1][1], 4.0, 2.0, 0, 0, 255);
            }
            else
            {
            	PrintToChat(param1, "%t", "Measure2", MOSSGREEN, WHITE);    
            }
        }
        else if (!strcmp(ChoosedMenu, "reset"))
        {
			ResetPos(param1);
        }

        OpenMeasureMenu(param1);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
		ResetPos(param1);
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

// Bunny Hop Stats
public Action cmdBHopeCheck(int client, int args)
{
	if (args < 1)
	{
        ReplyToCommand(client, "%t", "Macrodox_Usage", MOSSGREEN,WHITE);
        return Plugin_Handled;
	}

	char arg[65];
	GetCmdArg(1, arg, sizeof(arg));

	char target_name[MAX_TARGET_LENGTH];
	int target_list[MAXPLAYERS];
    int target_count; 
    bool tn_is_ml;

	if 
    ( 
        (
            target_count = ProcessTargetString(
                arg,
                client,
                target_list,
                MAXPLAYERS,
                COMMAND_FILTER_NO_IMMUNITY,
                target_name,
                sizeof(target_name),
                tn_is_ml
            )
        ) <= 0
    )
	{
	    PrintToConsole(client, "%t", "Macrodox_InvalidParam");
	    return Plugin_Handled;
	}

	if (target_count > 3) 
    {
        PrintToChat(client, "%t", "MacrodoxOutput", MOSSGREEN,WHITE);
    }

    for (int i = 0; i < target_count; i++)
	{
	    if (target_count > 3)
        {
	        PerformStats(client, target_list[i], true);
        }
	    else
        {
	        PerformStats(client, target_list[i], false);
        }
	}
 
	return Plugin_Handled;
}

// Settings
public Action cmdSettings(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
    OpenSettingsMenu(client);

	return Plugin_Handled;
}

void OpenSettingsMenu(int client)
{
    char MenuText[128];

    Menu menu = new Menu(mSettingsMenu);
    menu.SetTitle("%T:", "Settings_Title", client);
    
    if (g_bCenterPanel[client])
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_CenterPanel_Title", client, "Settings_CenterPanel_On", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_CenterPanel_Title", client, "Settings_CenterPanel_Off", client);
    }
    menu.AddItem("center_panel", MenuText);

    if (g_bJumpBeam[client])
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Beam_Title", client, "Settings_Beam_On", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Beam_Title", client, "Settings_Beam_Off", client);
    }
    menu.AddItem("beam", MenuText);
    if (g_bStrafeSync[client])
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Sync_Title", client, "Settings_Sync_On", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Sync_Title", client, "Settings_Sync_Off", client);
    }
    menu.AddItem("sync", MenuText);
    if (g_ColorChat[client] == 1)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_ColorChat_Title", client, "Settings_ColorChat_On", client);
    }
    else if (g_ColorChat[client] == 2)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_ColorChat_Title", client, "Settings_ColorChat_Godlike_Records", client);
    }
    else if (g_ColorChat[client] == 3)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_ColorChat_Title", client, "Settings_ColorChat_Records", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_ColorChat_Title", client, "Settings_ColorChat_Off", client);       
    }
    menu.AddItem("color_chat", MenuText);

    if (g_EnableQuakeSounds[client] == 1)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_QuakeSounds_Title", client, "Settings_QuakeSounds_On", client);
    }
    else if (g_EnableQuakeSounds[client] == 2)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_QuakeSounds_Title", client, "Settings_QuakeSounds_Godlike_Records", client);
    }
    else if (g_EnableQuakeSounds[client] == 3)
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_QuakeSounds_Title", client, "Settings_QuakeSounds_None_Except", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_QuakeSounds_Title", client, "Settings_QuakeSounds_Off", client);
    }
    menu.AddItem("quake_sounds", MenuText);

    /*
        if (g_bViewWeaponModels[client])
        {
            Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_VWM_Title", client, "Settings_VWM_On", client);
        }
        else
        {
            Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_VWM_Title", client, "Settings_VWM_Off", client);
        }
        menu.AddItem("toggle_w_models", MenuText);
    */
    if (g_bEnableSpecList[client])
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Spec_Title", client, "Settings_Spec_On", client);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T: %T", "Settings_Spec_Title", client, "Settings_Spec_Off", client);
    }
    menu.AddItem("spec_list", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mSettingsMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
		char ChoosedMenu[32];
		GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

        if (!strcmp(ChoosedMenu, "center_panel"))
        {
            g_bCenterPanel[param1] = !g_bCenterPanel[param1];
        }
        else if (!strcmp(ChoosedMenu, "beam"))
        {
            g_bJumpBeam[param1] = !g_bJumpBeam[param1];
        }
        else if (!strcmp(ChoosedMenu, "sync"))
        {
            g_bStrafeSync[param1] = !g_bStrafeSync[param1];
        }
        else if (!strcmp(ChoosedMenu, "color_chat"))
        {
            switch(g_ColorChat[param1])
            {
                case 0:     { g_ColorChat[param1]++;    }
                case 1:     { g_ColorChat[param1]++;    }
                case 2:     { g_ColorChat[param1]++;    }
                case 3:     { g_ColorChat[param1] = 0;  }
                default:    { g_ColorChat[param1] = 0;  }
            }
        }
        else if (!strcmp(ChoosedMenu, "quake_sounds"))
        {
            switch(g_EnableQuakeSounds[param1])
            {
                case 0:     { g_EnableQuakeSounds[param1]++;    }
                case 1:     { g_EnableQuakeSounds[param1]++;    }
                case 2:     { g_EnableQuakeSounds[param1]++;    }
                case 3:     { g_EnableQuakeSounds[param1] = 0;  }
                default:    { g_EnableQuakeSounds[param1] = 0;  }     
            }
        }
        else if (!strcmp(ChoosedMenu, "toggle_w_models"))
        {
            ToggleWeaponModels(param1);
        }
        else if (!strcmp(ChoosedMenu, "spec_list"))
        {
            g_bEnableSpecList[param1] = !g_bEnableSpecList[param1];
        }

        SaveClientSettings(param1);
        OpenSettingsMenu(param1);
	}
    else if (action == MenuAction_Cancel)
	{
		if (param2 == MenuCancel_ExitBack) 
		{
        }
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public Action cmdFunnyJump(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;
	
    OpenFunnyJumpingMenu(client);

	return Plugin_Handled;
}

void OpenFunnyJumpingMenu(int client)
{
	// Don't beep in warmup
	if (GameRules_GetProp("m_bWarmupPeriod") != 1) 
	{
        PrintToChat(client, "Ta komenda jest dostępna tylko w trybie Funny Jumping!");
        return;
	}

    char MenuText[32];

    Menu menu = new Menu(mFunnyJumpingMenu);
    menu.SetTitle("%T:", "FJ_Menu_Title", client);
   
    Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Save", client);
    menu.AddItem("save", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Load", client);
    menu.AddItem("load", MenuText);

    Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Restore", client);
    menu.AddItem("prev", MenuText);

	MoveType PlayerMove = GetEntityMoveType(client);
    if (PlayerMove == MOVETYPE_NOCLIP)
	{
      Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Noclip_A", client);
      menu.AddItem("noclip", MenuText);
    }
    else
    {
      Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Noclip_D", client);
      menu.AddItem("noclip", MenuText);
    }

    if (GetClientTeam(client) == CS_TEAM_SPECTATOR)
    {
        Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Respawn", client);
        menu.AddItem("resp", MenuText);
    }
    else
    {
        Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Spectate", client);
        menu.AddItem("spec", MenuText);
    }

    Format(MenuText, sizeof(MenuText), "%T", "FJ_Menu_Vote_Normal", client);
    menu.AddItem("nmvote", MenuText);

	menu.ExitButton = true;
	menu.Display(client, MENU_TIME_FOREVER);
}

public int mFunnyJumpingMenu(Menu menu, MenuAction action, int param1, int param2)
{
	if (action == MenuAction_Select)
	{
        if (GameRules_GetProp("m_bWarmupPeriod") != 1) 
        {
            PrintToChat(param1, "%t", "FJ_Menu_Info");
        }
        else
        {
            char ChoosedMenu[32];
            GetMenuItem(menu, param2, ChoosedMenu, sizeof(ChoosedMenu));

            if (!strcmp(ChoosedMenu, "load"))
            {
                if (g_PlayerSavingCount[param1] > 0)
                {
                    // Reset Jump Bug
                    ResetJumpBug(param1);

                    // Reset Jump for be sure
                    ResetJump(param1);

                    // Reset Slide
                    g_bResetNextSlide[param1] = true;

                    // Do teleport
                    DoValidTeleport(param1, g_fPlayerSavedPosition[param1], g_fPlayerSavedAngle[param1], NULL_VECTOR);
                }
            }
            else if (!strcmp(ChoosedMenu, "save"))
            {
                g_fPrevPlayerSavedPosition[param1][0] = g_fPlayerSavedPosition[param1][0];
                g_fPrevPlayerSavedPosition[param1][1] = g_fPlayerSavedPosition[param1][1];
                g_fPrevPlayerSavedPosition[param1][2] = g_fPlayerSavedPosition[param1][2];

                g_fPrevPlayerSavedAngle[param1][0] = g_fPlayerSavedAngle[param1][0];
                g_fPrevPlayerSavedAngle[param1][1] = g_fPlayerSavedAngle[param1][1];
                g_fPrevPlayerSavedAngle[param1][2] = g_fPlayerSavedAngle[param1][2];

                float TempAngle[3];
                float PlayerOrigin[3];

                TempAngle[0] = 89.0;
                TempAngle[1] = 0.0;
                TempAngle[2] = 0.0;

                GetClientAbsOrigin(param1, g_fPlayerSavedPosition[param1]);
                // GetClientEyePosition(param1, g_fPlayerSavedPosition[param1]);
                GetClientEyeAngles(param1, g_fPlayerSavedAngle[param1]);

                Handle GroundTrace = TR_TraceRayFilterEx(PlayerOrigin, TempAngle, MASK_SHOT, RayType_Infinite, WorldFilter, param1);
                if (TR_DidHit(GroundTrace))
                {
                    float GroundOrigin[3];
                    TR_GetEndPosition(GroundOrigin, GroundTrace);
                    
                    // g_fPlayerSavedPosition[param1][0] = GroundOrigin[0];
                    // g_fPlayerSavedPosition[param1][1] = GroundOrigin[1];
                    // g_fPlayerSavedPosition[param1][2] = GroundOrigin[2];
                }
                delete GroundTrace;

                g_PlayerSavingCount[param1]++;
            }
            else if (!strcmp(ChoosedMenu, "prev"))
            {
                if (g_PlayerSavingCount[param1] > 1)
                {
                    g_fPlayerSavedPosition[param1][0] = g_fPrevPlayerSavedPosition[param1][0];
                    g_fPlayerSavedPosition[param1][1] = g_fPrevPlayerSavedPosition[param1][1];
                    g_fPlayerSavedPosition[param1][2] = g_fPrevPlayerSavedPosition[param1][2];

                    g_fPlayerSavedAngle[param1][0] = g_fPrevPlayerSavedAngle[param1][0];
                    g_fPlayerSavedAngle[param1][1] = g_fPrevPlayerSavedAngle[param1][1];
                    g_fPlayerSavedAngle[param1][2] = g_fPrevPlayerSavedAngle[param1][2];
                }
            }
            else if (!strcmp(ChoosedMenu, "noclip"))
            {
                ToggleNoClip(param1);
            }
            else if (!strcmp(ChoosedMenu, "spec"))
            {
                g_fPlayerSavedTeam[param1] = GetClientTeam(param1);
                FakeClientCommand(param1, "jointeam 1");
            }
            else if (!strcmp(ChoosedMenu, "resp"))
            {
                char Command[64];
                Format(Command, sizeof(Command), "jointeam %d", g_fPlayerSavedTeam[param1]);
                FakeClientCommand(param1, Command);
            }
            else if (!strcmp(ChoosedMenu, "nmvote"))
            {
                FakeClientCommand(param1, "sm_fjn");
            }

            OpenFunnyJumpingMenu(param1);
        }
	}
    else if (action == MenuAction_Cancel)
	{
	}
	else if (action == MenuAction_End)
	{
		delete menu;
	}
}

public Action cmdNoClip(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;

    ToggleNoClip(client);

	return Plugin_Handled;
}

public Action cmdRedie(int client, int args)
{
	if (!IsValidClient(client)) return Plugin_Handled;

    Redie(client);

	return Plugin_Handled;
}

public Action cmdFOV(int client, int args)
{
    if (args != 1)
	{
		ReplyToCommand(client, "Usage: sm_fov <value> (30 - 110)");
		return Plugin_Handled;
	}
	
	char buffer[128];
	
	GetCmdArg(1, buffer, sizeof(buffer));
	int fov = StringToInt(buffer);

    if (fov < 30 || fov > 110)
    {
        ReplyToCommand(client, "Usage: sm_fov <value> (30 - 110)");
		return Plugin_Handled;
    }

    SetEntProp(client, Prop_Send, "m_iFOV", fov);
	SetEntProp(client, Prop_Send, "m_iDefaultFOV", fov);

	return Plugin_Handled;
}

public Action cmdToggleWeaponModels(int client, int args)
{
	ToggleWeaponModels(client);
	if (g_bViewWeaponModels[client])
    {
		PrintToChat(client, "%t", "EnableModelsInfo", MOSSGREEN, WHITE);
    }
	else
    {
		PrintToChat(client, "%t", "DisableModelsInfo", MOSSGREEN, WHITE);
    }

	return Plugin_Handled;
}

    