void InitMisc()
{
    
}

public void LoadPlayer(int client)
{
	SetClientDefaults(client);
	LoadClientSettings(client);
	LoadClientRecords(client);
	UpdatePlayerNickname(client);
}

public void UnloadPlayer(int client)
{
	// Measure reset
	ResetPos(client);

	// Anty Cheat
	g_aiJumps[client] = 0;
	g_fafAvgJumps[client] = 5.0;
	g_fafAvgSpeed[client] = 250.0;
	g_fafAvgPerfJumps[client] = 0.3333;
}

// Set client defaults
public void SetClientDefaults(int client)
{
	// Defaults
	g_bKickStatus[client] = false;
	g_js_bPlayerJumped[client] = false;
	g_bOnBhopPlattform[client] = false;
	g_js_bFuncMoveLinear[client] = false;

	g_js_Last_Ground_Frames[client] = 11;
	g_js_MultiBhop_Count[client] = 1;

	g_PrestrafeFrameCounter[client] = 0;
	g_PrestrafeVelocity[client] = 1.0;
	g_js_GroundFrames[client] = 0;
	g_js_fJump_JumpOff_PosLastHeight[client] = -1.012345;
	g_js_Good_Sync_Frames[client] = 0.0;
	g_js_Sync_Frames[client] = 0.0;
	g_js_GODLIKE_Count[client] = 0;

	// Slide
	g_bResetNextSlide[client] = false;
	g_bIsDucking[client] = false;

	// Ghost Mode
	g_bPlayerGhostMode[client] = false;

	g_fTeleportValidationTime[client] = GetEngineTime()-1.01;
	if (g_fTeleportValidationTime[client]  < 0.0)
	{
		g_fTeleportValidationTime[client]  = 0.0;
	}

	Format(g_js_szLastJumpDistance[client], 256, "%T", "HUD_ZeroUnits", client); //#948d8d

	for (int i = 0; i < 30; i++)
	{
		g_aaiLastJumps[client][i] = -1;
	}

	// Client options
	g_bJumpBeam[client] = false;
	g_ColorChat[client] = 1;
	g_EnableQuakeSounds[client] = 1;
	g_bStrafeSync[client] = false;
	g_bCenterPanel[client] = true;
	g_bViewWeaponModels[client] = true;
	g_bEnableSpecList[client] = false;

	// Jumpbug
	g_bJumpHasBugged[client] = false;
	g_fStartGroundHeight[client] = 0.0;
	g_fEndGroundHeight[client] = 0.0;

	g_PlayerJumpbugCooldown[client] = 0;

	g_StartHealth[client] = 0;
	g_EndHealth[client] = 0;

	// Reset Funny Jump
	g_PlayerSavingCount[client] = 0;

	g_fPlayerSavedPosition[client][0] = 0.0;
	g_fPlayerSavedPosition[client][1] = 0.0;
	g_fPlayerSavedPosition[client][2] = 0.0;

	g_fPrevPlayerSavedPosition[client][0] = 0.0;
	g_fPrevPlayerSavedPosition[client][1] = 0.0;
	g_fPrevPlayerSavedPosition[client][2] = 0.0;

	g_fPlayerSavedAngle[client][0] = 0.0;
	g_fPlayerSavedAngle[client][1] = 0.0;
	g_fPlayerSavedAngle[client][2] = 0.0;

	g_fPlayerSavedTeam[client] = 0;

	g_fPrevPlayerSavedAngle[client][0] = 0.0;
	g_fPrevPlayerSavedAngle[client][1] = 0.0;
	g_fPrevPlayerSavedAngle[client][2] = 0.0;
}

public void ResetJumpBug(int client)
{
	g_bJumpHasBugged[client] = false;
	g_fStartGroundHeight[client] = 0.0;
	g_fEndGroundHeight[client] = 0.0;

	g_StartHealth[client] = 0;
	g_EndHealth[client] = 0;
}

public void ResetSlide(int client)
{
	g_fSlideInitSpeed[client] = 0.0;
	g_fSlideVelocity[client][0] = 0.0;
	g_fSlideVelocity[client][1] = 0.0;
	g_fSlideVelocity[client][2] = 0.0;
}

public void ResetJump(int client)
{
	Format(g_js_szLastJumpDistance[client], 256, "%T", "HUD_Invalid", client, g_js_fJump_Distance[client]); // default color was #948d8d
	g_js_GroundFrames[client] = 0;
	g_bBeam[client] = false;
	g_js_bPerfJumpOff[client] = false;
	g_js_bPerfJumpOff2[client] = false;
	g_js_bPlayerJumped[client] = false;

    /*
	if(client != g_ProBot && client != g_TpBot)
    {
		Call_KZTimer_OnJumpstatInvalid(client);
	}
    */
}

// Can be used until Valve restore HTML support in hint
void PrintHintTextFix(int client, const char[] msg)
{
    char sBuffer[2048];
    //Format(sBuffer, sizeof(sBuffer), "</font>%s\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", msg);
    Format(sBuffer, sizeof(sBuffer), "</font>%s<font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font><font></font>", msg);
    
   

    Protobuf hMessage = view_as<Protobuf>(StartMessageOne("TextMsg", client));
    //rotobuf hMessage = view_as<Protobuf>(StartMessageAll("TextMsg", USERMSG_RELIABLE));
    
    if(hMessage)
	{
        hMessage.SetInt("msg_dst", 4);
        hMessage.AddString("params", "#SFUI_ContractKillStart");
        hMessage.AddString("params", sBuffer);
        hMessage.AddString("params", NULL_STRING);
        hMessage.AddString("params", NULL_STRING);
        hMessage.AddString("params", NULL_STRING);
        hMessage.AddString("params", NULL_STRING);
        
        EndMessage();
    }
}

// Draws client center panel
void DrawCenterPanel(int client, int target)
{
    // Check spec mode
    if (client != target)
    {
        int ObserverMode = GetEntPropEnt(client, Prop_Send, "m_iObserverMode");
        
		/*if (ObserverMode != 4 && ObserverMode != 5)
        {
            return;
        }*/
    }

	char MessageText[512];

    // Get keys
    int Buttons;
    char Result[32];
    Buttons = g_LastButton[target];

    if (Buttons & IN_MOVELEFT) Format(Result, sizeof(Result), "<b>%T</b>: A", "HUD_Keys", client);
     else Format(Result, sizeof(Result), "<b>%T:</b> _", "HUD_Keys", client);

    if (Buttons & IN_FORWARD) Format(Result, sizeof(Result), "%s W", Result);
     else Format(Result, sizeof(Result), "%s _", Result);
    
    if (Buttons & IN_BACK) Format(Result, sizeof(Result), "%s S", Result);
     else Format(Result, sizeof(Result), "%s _", Result);
    
    if (Buttons & IN_MOVERIGHT) Format(Result, sizeof(Result), "%s D", Result);
     else Format(Result, sizeof(Result), "%s _", Result);
    
    if (Buttons & IN_DUCK || ((GetEngineTime() - g_fCrouchButtonLastTimeUsed[target]) < 0.05)) Format(Result, sizeof(Result), "%s - C", Result);
     else Format(Result, sizeof(Result), "%s - _", Result);
    
    if (Buttons & IN_JUMP || ((GetEngineTime() - g_fJumpButtonLastTimeUsed[target]) < 0.05)) Format(Result, sizeof(Result), "%s J", Result);
     else Format(Result, sizeof(Result), "%s _", Result);

    // Draw center panel
	if (g_js_bPlayerJumped[target])
	{
		Format
		(
			MessageText, 
			sizeof(MessageText), 
			"<b>%T:</b> %s\n<b>%T:</b> %.1f %T/s (%.0f)\n%s", 
			"HUD_Last", 
			client, 
			g_js_szLastJumpDistance[target], 
			"HUD_Speed", 
			client, 
			g_fLastSpeed[target], 
			"HUD_UnitSymbol",
			client, 
			g_js_fPreStrafe[target], 
			Result
		);

		PrintHintTextFix(client, MessageText);
		// pray for HTML hint fix  PrintHintText(client, "<b>%T:</b> %s\n<b>%T:</b> %.1f %T/s (%.0f)\n%s", "HUD_Last", client, g_js_szLastJumpDistance[target], "HUD_Speed", client, g_fLastSpeed[target], "HUD_UnitSymbol", client, g_js_fPreStrafe[target], Result);			
	}
	else
	{
		Format
		(
			MessageText, 
			sizeof(MessageText), 
 			"<b>%T</b>: %s\n<b>%T</b>: %.1f %T/s\n%s", 
			"HUD_Last", 
			client, 
			g_js_szLastJumpDistance[target], 
			"HUD_Speed", 
			client, 
			g_fLastSpeed[target], 
			"HUD_UnitSymbol", 
			client, 
			Result
		);

		PrintHintTextFix(client, MessageText);
		// pray for HTML hint fix PrintHintText(client, "<b>%T</b>: %s\n<b>%T</b>: %.1f %T/s\n%s", "HUD_Last", client, g_js_szLastJumpDistance[target], "HUD_Speed", client, g_fLastSpeed[target], "HUD_UnitSymbol", client, Result);
	}
}

// Speed cap
public void SpeedCap(int client)
{
	static bool IsOnGround[MAXPLAYERS];

    float CurVelVec[3];
    // float CurrentSpeed = GetSpeed(client);
	
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", CurVelVec);
	CurVelVec[2] = 0.0;

    // If jumpbugged, apply bhop Speed cap
	if (g_bJumpBugged[client])
	{
		g_bJumpBugged[client] = false; // Reset
		if (GetVectorLength(CurVelVec) > g_fBhopSpeedCap)
		{
			NormalizeVector(CurVelVec, CurVelVec);
			ScaleVector(CurVelVec, g_fBhopSpeedCap);

			NormalizeVector(g_fSlideVelocity[client], g_fSlideVelocity[client]);
			ScaleVector(g_fSlideVelocity[client], g_fBhopSpeedCap);

			DoValidTeleport(client, NULL_VECTOR, NULL_VECTOR, CurVelVec);
		}
	}

	if (g_bOnGround[client])
	{
		if (!IsOnGround[client])
		{
			if 
			(
				!g_bIsDucking[client] &&
				GetVectorLength(CurVelVec) > g_fBhopSpeedCap &&
				GetVectorLength(g_fSlideVelocity[client]) > g_fBhopSpeedCap
			)
			{
				NormalizeVector(CurVelVec, CurVelVec);
				ScaleVector(CurVelVec, g_fBhopSpeedCap);

				NormalizeVector(g_fSlideVelocity[client], g_fSlideVelocity[client]);
				ScaleVector(g_fSlideVelocity[client], g_fBhopSpeedCap);

				DoValidTeleport(client, NULL_VECTOR, NULL_VECTOR, CurVelVec);
			}
		}
	}
	else
    {
		IsOnGround[client] = false;
    }
}

// Double duck functionality
public void DoubleDuck(int client, int &buttons)
{
    if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))) return;

    static int Flags;
    Flags = GetEntityFlags(client);

    if (Flags & FL_ONGROUND)
    {
        static bool bAllowDoubleDuck[MAXPLAYERS];

        if (Flags & FL_DUCKING)
        {
            bAllowDoubleDuck[client] = false;
            return;
        }

        if (buttons & IN_DUCK)
        {
            bAllowDoubleDuck[client] = true;
            return;
        }

        if (GetEntProp(client, Prop_Data, "m_bDucking" ) && bAllowDoubleDuck[client])
        {
            float PlayerPosition[3];
            GetClientAbsOrigin(client, PlayerPosition);

            PlayerPosition[2] += 40.0;

            if (IsValidPlayerPos(client, PlayerPosition))
			{
				g_js_GroundFrames[client] = 0;
				DoValidTeleport(client, PlayerPosition,NULL_VECTOR,NULL_VECTOR);

				g_js_DuckCounter[client]++;
				g_fLastTimeDoubleDucked[client] = GetEngineTime();
			}
        }
    }
}

void DuckingCheck(int client)
{
	if
    (	
		!(g_LastButton[client] & IN_JUMP) &&
		!(g_CurrentButton[client] & IN_JUMP) &&
        !(g_LastButton[client] & IN_DUCK) &&
        g_CurrentButton[client] & IN_DUCK
    )
    {
        g_bIsDucking[client] = true;
    }

	if 
	(
		g_bOnGround[client] &&
		g_bIsDucking[client] &&
		g_CurrentButton[client] & IN_JUMP
	)	
	{
        g_bIsDucking[client] = false;	
			
		float CurVelVec[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", CurVelVec);

		if 
		(
			GetVectorLength(CurVelVec) > g_fBhopSpeedCap &&
			GetVectorLength(g_fSlideVelocity[client]) > g_fBhopSpeedCap
		)
		{
			NormalizeVector(CurVelVec, CurVelVec);
			ScaleVector(CurVelVec, g_fBhopSpeedCap);

			NormalizeVector(g_fSlideVelocity[client], g_fSlideVelocity[client]);
			ScaleVector(g_fSlideVelocity[client], g_fBhopSpeedCap);

			DoValidTeleport(client, NULL_VECTOR, NULL_VECTOR, CurVelVec);

			// PrintToChat(client, "DUCK-JUMP > 300.0");
		}
		
	}

}

void Slide(int client)
{ 
    if 
    (
        !g_bOnGround[client] ||
        (
            !(g_LastButton[client] & IN_DUCK) &&
            g_CurrentButton[client] & IN_DUCK
        )
    )
    {
        g_fSlideInitSpeed[client] = GetSpeed(client);
        GetEntPropVector(client, Prop_Data, "m_vecVelocity", g_fSlideVelocity[client]);

        // Remove z speed
        g_fSlideVelocity[client][2] = 0.0;

        // Slide speed fix
        ScaleVector(g_fSlideVelocity[client], 1.1);
    }

	// Ducking Cap
    if (g_bIsDucking[client] && GetVectorLength(g_fSlideVelocity[client]) > g_fBhopSpeedCap)
    {
        float TempVelocity[3];

        TempVelocity[0] = g_fSlideVelocity[client][0];
        TempVelocity[1] = g_fSlideVelocity[client][1];
        TempVelocity[2] = g_fSlideVelocity[client][2];

        ScaleVector(TempVelocity, 0.96);

        if (GetVectorLength(TempVelocity) > g_fBhopSpeedCap)
        {
            g_fSlideVelocity[client][0] = TempVelocity[0];
            g_fSlideVelocity[client][1] = TempVelocity[1];
            g_fSlideVelocity[client][2] = TempVelocity[2];
        }
        else
        {
            NormalizeVector(g_fSlideVelocity[client], g_fSlideVelocity[client]);
            ScaleVector(g_fSlideVelocity[client], g_fBhopSpeedCap);
        }
    }

    if 
    (
        g_bOnGround[client] &&
        !(g_CurrentButton[client] & IN_JUMP) &&
        GetVectorLength(g_fSlideVelocity[client]) > 10.0
    )
    {
		if (g_bResetNextSlide[client])
		{
			ResetSlide(client);
			g_bResetNextSlide[client] = false;
		}

        if (g_CurrentButton[client] & IN_DUCK)
        {
			float RotationOffset = GetClientMovingRotationOffset(client);
            if 
            (
                !(
                    (
                        g_CurrentButton[client] & IN_FORWARD ||
                        g_CurrentButton[client] & IN_BACK ||
                        g_CurrentButton[client] & IN_MOVELEFT ||
                        g_CurrentButton[client] & IN_MOVERIGHT
                    ) &&
                    GetVectorLength(g_fSlideVelocity[client]) <= 85.0
                ) &&
				!(
					(-45 < RotationOffset < 45) &&
					g_CurrentButton[client] & IN_BACK 
				) &&
				!(
					(-135 < RotationOffset < -45) &&
					g_CurrentButton[client] & IN_MOVELEFT 
				) &&
				!(
					(45 < RotationOffset < 135) &&
					g_CurrentButton[client] &  IN_MOVERIGHT
				) &&
				!(
					(RotationOffset < -135  || RotationOffset > 135) &&
					g_CurrentButton[client] & IN_FORWARD 
				)
            )
            {
                SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_fSlideInitSpeed[client] / 85.0);
                TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, g_fSlideVelocity[client]);
            }
			else
			{
			    ResetSlide(client);
			}
        }
        else
        {

			float RotationOffset = GetClientMovingRotationOffset(client);
	
			if 
			(
				!(
                    (
                        g_CurrentButton[client] & IN_FORWARD ||
                        g_CurrentButton[client] & IN_BACK ||
                        g_CurrentButton[client] & IN_MOVELEFT ||
                        g_CurrentButton[client] & IN_MOVERIGHT
                    ) &&
                    GetVectorLength(g_fSlideVelocity[client]) <= 225.0
                ) &&
				!(
					(-45 < RotationOffset < 45) &&
					g_CurrentButton[client] & IN_BACK 
				) &&
				!(
					(-135 < RotationOffset < -45) &&
					g_CurrentButton[client] & IN_MOVELEFT 
				) &&
				!(
					(45 < RotationOffset < 135) &&
					g_CurrentButton[client] &  IN_MOVERIGHT
				) &&
				!(
					(RotationOffset < -135  || RotationOffset > 135) &&
					g_CurrentButton[client] & IN_FORWARD 
				)
            )
            {
                SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_fSlideInitSpeed[client] / 225.0);
                TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, g_fSlideVelocity[client]);
            }
            else
            {
   				ResetSlide(client);
            }
        }	
    }

    if (GetVectorLength(g_fSlideVelocity[client]) > 10.0)
    {
        g_fSlideVelocity[client][0] -= g_fSlideVelocity[client][0] * 0.02;
        g_fSlideVelocity[client][1] -= g_fSlideVelocity[client][1] * 0.02;
        g_fSlideVelocity[client][2] -= g_fSlideVelocity[client][2] * 0.02;
    }
	else
	{
		ResetSlide(client);
	}
}

// Prestrafe functionality
public void Prestrafe(int client, float ang, int &buttons)
{
    if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client])) || !(GetEntityFlags(client) & FL_ONGROUND)) return;

    float DefaultGranadesSpeed = 1.0205;
	float MaxGranadesSpeed = 1.12653;

    float DefaultKnifeSpeed = 1.0;
	float MaxKnifeSpeed = 1.104;

	float DefaultUspSpeed = 1.041667;
	float MaxUspSpeed = 1.15;

	float UnarmedSpeed = 0.96154;
	bool TurningRight;
	bool TurningLeft;
	int MaxFrameCount;
	float IncSpeed;
    float DecSpeed;
	float Speed = GetSpeed(client);
	bool Forward;

	char ClassName[128];
	GetClientWeapon(client, ClassName, sizeof(ClassName));
	TrimString(ClassName);

	if 
    (
        !StrEqual(ClassName, "weapon_hkp2000") &&
        !StrEqual(ClassName, "weapon_usp_silencer") &&
        !StrEqual(ClassName, "weapon_bayonet") &&
        StrContains(ClassName, "weapon_knife") &&
        StrContains(ClassName, "weapon")
    )
	{
		if (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultUspSpeed);
		}
		else if ((StrContains(ClassName, "weapon_knife")) != -1 || (StrContains(ClassName, "weapon_bayonet") != -1))
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultKnifeSpeed);
		}
		else
		{
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", UnarmedSpeed);
		}

		return;
	}
	
	// get turning direction
	// Fixed for  -180 180 wrap
	float eye_angle_change = ang - g_fLastAngles[client][1];
	if(eye_angle_change < -180)  
    {
		eye_angle_change += 360;
    }

	if (eye_angle_change > 180)
    {
		eye_angle_change -= 360;
    }

	if(eye_angle_change < 0)
    {
		TurningRight = true;
    }
    else if(eye_angle_change > 0)
    {
		TurningLeft = true;
    }

	// get moving direction
	if (GetClientMovingDirection(client, false) > 0.0)
    {
		Forward = true;
    }

	float flVelMd = GetEntPropFloat(client, Prop_Send, "m_flVelocityModifier");
	if 
    (
        (
            StrEqual(ClassName, "weapon_knife") ||
            StrEqual(ClassName, "weapon_bayonet") ||
            StrContains(ClassName, "weapon_knife_") != -1

        ) &&
        flVelMd > MaxKnifeSpeed + 0.007
    )
	{
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", MaxKnifeSpeed - 0.001);
	}

	if 
    (
        (
            (StrEqual(ClassName, "weapon_decoy") || 
			StrEqual(ClassName, "weapon_flashbang"))

        ) &&
        flVelMd > MaxGranadesSpeed + 0.007
    )
	{
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", MaxGranadesSpeed - 0.001);
	}

	// No mouse movement?
	if (!TurningRight && !TurningLeft)
	{
		float diff;
		diff = GetEngineTime() - g_fVelocityModifierLastChange[client];
		if (diff > 0.2)
		{ 
			if (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
            {
				g_PrestrafeVelocity[client] = DefaultUspSpeed;
            }
			else if (StrEqual(ClassName, "weapon_decoy") || StrEqual(ClassName, "weapon_flashbang"))
            {
				g_PrestrafeVelocity[client] = DefaultGranadesSpeed;
            }
            else
            {
				g_PrestrafeVelocity[client] = DefaultKnifeSpeed;
            }
			
            g_fVelocityModifierLastChange[client] = GetEngineTime();
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_PrestrafeVelocity[client]);
		}
		return;
	}

	if (((buttons & IN_MOVERIGHT) || (buttons & IN_MOVELEFT)) && Speed > 248.9)
	{
		// Tickrate depending values
		if (g_Server_Tickrate == 64)
		{
			MaxFrameCount = 45;
			IncSpeed = 0.0015;
			if ((g_PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(g_PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer"))))
				IncSpeed = 0.001;
			DecSpeed = 0.0045;
		}

		if (g_Server_Tickrate == 102)
		{
			MaxFrameCount = 60;
			IncSpeed = 0.0011;
			if ((g_PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(g_PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer"))))
				IncSpeed = 0.001;
			DecSpeed = 0.0045;
		}

		if (g_Server_Tickrate == 128)
		{
			MaxFrameCount = 75;
			IncSpeed = 0.0009;
			if ((g_PrestrafeVelocity[client] > 1.08 && (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))) ||
				(g_PrestrafeVelocity[client] > 1.04 && (!StrEqual(ClassName, "weapon_hkp2000") || !StrEqual(ClassName, "weapon_usp_silencer")))
                )
				IncSpeed = 0.001;
			DecSpeed = 0.0045;
		}

		if (((buttons & IN_MOVERIGHT && TurningRight || TurningLeft && !Forward)) || ((buttons & IN_MOVELEFT && TurningLeft || TurningRight && !Forward)))
		{
			g_PrestrafeFrameCounter[client]++;
			// Add Speed if Prestrafe frames are less than max frame count
			if (g_PrestrafeFrameCounter[client] < MaxFrameCount)
			{
				// Increase Speed
				g_PrestrafeVelocity[client] += IncSpeed;

				// USP
				if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
				{
					if (g_PrestrafeVelocity[client] > MaxUspSpeed)
						g_PrestrafeVelocity[client] -= 0.007;
				}
				else if (StrEqual(ClassName, "weapon_decoy") || StrEqual(ClassName, "weapon_flashbang"))
				{
					if (g_PrestrafeVelocity[client] > MaxGranadesSpeed)
					{
						if (g_PrestrafeVelocity[client] > MaxGranadesSpeed + 0.007)
							g_PrestrafeVelocity[client] = MaxGranadesSpeed - 0.001;
						else
							g_PrestrafeVelocity[client] -= 0.007;
					}
				}
				else
				{
					if (g_PrestrafeVelocity[client] > MaxKnifeSpeed)
					{
						if (g_PrestrafeVelocity[client] > MaxKnifeSpeed + 0.007)
							g_PrestrafeVelocity[client] = MaxKnifeSpeed - 0.001;
						else
							g_PrestrafeVelocity[client] -= 0.007;
					}
				}
				g_PrestrafeVelocity[client] += IncSpeed;
			}
			else
			{
				// Decrease Speed
				g_PrestrafeVelocity[client]-= DecSpeed;
				g_PrestrafeFrameCounter[client] = g_PrestrafeFrameCounter[client] - 2;

				// USP reset 250.0 Speed
				if (StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
				{
					if (g_PrestrafeVelocity[client] < DefaultUspSpeed)
					{
						g_PrestrafeFrameCounter[client] = 0;
						g_PrestrafeVelocity[client] = DefaultUspSpeed;
					}
				}
				else if (StrEqual(ClassName, "weapon_decoy") || StrEqual(ClassName, "weapon_flashbang"))
				{
					// Granades reset 250.0 Speed
					if (g_PrestrafeVelocity[client] < DefaultGranadesSpeed)
					{
						g_PrestrafeFrameCounter[client] = 0;
						g_PrestrafeVelocity[client] = DefaultGranadesSpeed;
					}
				}
				else
                {
					// Knife reset 250.0 Speed
					if (g_PrestrafeVelocity[client] < DefaultKnifeSpeed)
					{
						g_PrestrafeFrameCounter[client] = 0;
						g_PrestrafeVelocity[client] = DefaultKnifeSpeed;
					}
                }
            }
		}
		else
		{
			g_PrestrafeVelocity[client] -= 0.04;
			if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
			{
				if (g_PrestrafeVelocity[client] < DefaultUspSpeed)
                {
					g_PrestrafeVelocity[client] = DefaultUspSpeed;
                }
            }
			else if (StrEqual(ClassName, "weapon_decoy") || StrEqual(ClassName, "weapon_flashbang"))
			{
				g_PrestrafeVelocity[client] = DefaultGranadesSpeed;
			}
			else if (g_PrestrafeVelocity[client] < DefaultKnifeSpeed)
            {
				g_PrestrafeVelocity[client] = DefaultKnifeSpeed;
            }
        }

		// Set VelocityModifier
		SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", g_PrestrafeVelocity[client]);
		g_fVelocityModifierLastChange[client] = GetEngineTime();
	}
	else
	{
		if(StrEqual(ClassName, "weapon_hkp2000") || StrEqual(ClassName, "weapon_usp_silencer"))
        {
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultUspSpeed);
        }
		else if (StrEqual(ClassName, "weapon_decoy") || StrEqual(ClassName, "weapon_flashbang"))
        {
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultGranadesSpeed);
        }
        else
        {
			SetEntPropFloat(client, Prop_Send, "m_flVelocityModifier", DefaultKnifeSpeed);
        }

		g_PrestrafeFrameCounter[client] = 0;
	}
}

// Booster
public void BoosterCheck(int client)
{
	float BaseVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecBaseVelocity", BaseVelocity);
	if (BaseVelocity[0] != 0.0 || BaseVelocity[1] != 0.0 || BaseVelocity[2] != 0.0 && g_js_bPlayerJumped[client])
	{
		g_bTouchedBooster[client]=true;
		ResetJump(client);
	}
}

public void SlopeBoostFix(int client)
{
	// g_bLastOnGround[client] 	= g_bOnGround[client];
	g_vLast[client][0]    		= g_vCurrent[client][0];
	g_vLast[client][1]    		= g_vCurrent[client][1];
	g_vLast[client][2]    		= g_vCurrent[client][2];
	g_vCurrent[client][0] 		= GetEntPropFloat(client, Prop_Send, "m_vecVelocity[0]");
	g_vCurrent[client][1] 		= GetEntPropFloat(client, Prop_Send, "m_vecVelocity[1]");
	g_vCurrent[client][2] 		= GetEntPropFloat(client, Prop_Send, "m_vecVelocity[2]");

	// Check if player landed on the ground
	if (g_bOnGround[client] == true && g_bLastOnGround[client] == false)
	{
		// PrintToChat(client, "On Ground");
		// Set up and do tracehull to find out if the player landed on a slope
		float vPos[3];
		GetEntPropVector(client, Prop_Data, "m_vecOrigin", vPos);

		float vMins[3];
		GetEntPropVector(client, Prop_Send, "m_vecMins", vMins);

		float vMaxs[3];
		GetEntPropVector(client, Prop_Send, "m_vecMaxs", vMaxs);

		float vEndPos[3];
		vEndPos[0] = vPos[0];
		vEndPos[1] = vPos[1];
		vEndPos[2] = vPos[2] - FindConVar("sv_maxvelocity").FloatValue;

		TR_TraceHullFilter(vPos, vEndPos, vMins, vMaxs, MASK_PLAYERSOLID_BRUSHONLY, TraceRayDontHitSelfSlope, client);

		if (TR_DidHit())
		{
			// PrintToChat(client, "Hit Ground");
			// Gets the normal vector of the surface under the player
			float vPlane[3];
			float vLast[3];
			TR_GetPlaneNormal(INVALID_HANDLE, vPlane);

			// Make sure it's not flat ground and not a surf ramp (1.0 = flat ground, < 0.7 = surf ramp)
			if (0.7 <= vPlane[2] && vPlane[2] < 1.0)
			{
				// PrintToChat(client, "Not flat and not surf");
				/*
				Copy the ClipVelocity function from sdk2013
				(https://mxr.alliedmods.net/hl2sdk-sdk2013/source/game/shared/gamemovement.cpp#3145)
				With some minor changes to make it actually work
				*/
				vLast[0]  = g_vLast[client][0];
				vLast[1]  = g_vLast[client][1];
				vLast[2]  = g_vLast[client][2];
				vLast[2] -= (FindConVar("sv_gravity").FloatValue * GetTickInterval() * 0.5);

				float fBackOff = GetVectorDotProduct(vLast, vPlane);

				float change, vVel[3];
				for (int i; i < 2; i++)
				{
					change  = vPlane[i] * fBackOff;
					vVel[i] = vLast[i] - change;
				}

				float fAdjust = GetVectorDotProduct(vVel, vPlane);
				if (fAdjust < 0.0)
				{
					for (int i; i < 2; i++)
					{
						vVel[i] -= (vPlane[i] * fAdjust);
					}
				}

				vVel[2] = 0.0;
				vLast[2] = 0.0;

				// Make sure the player is going down a ramp by checking if they actually will gain speed from the boost
				if (GetVectorLength(vVel) > GetVectorLength(vLast))
				{
					if (GetVectorLength(g_fSlideVelocity[client]) > 0.0)
					{
						ResetSlide(client);
					}

					// Teleport the player, also adds basevelocity
					if (GetEntityFlags(client) & FL_BASEVELOCITY)
					{
						float vBase[3];
						GetEntPropVector(client, Prop_Data, "m_vecBaseVelocity", vBase);
						AddVectors(vVel, vBase, vVel);
					}

					DoValidTeleport(client, NULL_VECTOR, NULL_VECTOR, vVel);
				}
			}
		}
	}
}

public bool TraceRayDontHitSelfSlope(int entity, int mask, any data)
{
	return entity != data && !(0 < entity <= MaxClients);
}

// Water check
public void WaterCheck(int client)
{
	if (GetEntProp(client, Prop_Data, "m_nWaterLevel") > 0)
    {
		if (g_js_bPlayerJumped[client])
		{
			ResetJump(client);
		}
		
		ResetJumpBug(client);
    }
}

public void JumpBugCheck(int client)
{
	if (g_JumpBugCooldown > 0) return;
	if (g_PlayerJumpbugCooldown[client] > 0) return;
	if (!IsPlayerAlive(client)) return;
	
	// Ground -> Air
	if (!g_bOnGround[client] && g_bLastOnGround[client])
	{
		float PlayerOrigin[3];
		GetClientAbsOrigin(client, PlayerOrigin);

		g_fHighestSpeed[client] = GetSpeedZ(client);
		g_fLastBugSpeed[client] = GetSpeedZ(client); 
		g_fStartGroundHeight[client] = PlayerOrigin[2];
		g_StartHealth[client] = GetEntProp(client, Prop_Send, "m_iHealth");
	}

	// In Air
	if (!g_bOnGround[client] && !g_bLastOnGround[client])
	{
		g_fLastBugSpeed[client] = GetSpeedZ(client); 
		
		if (g_fLastBugSpeed[client] > g_fHighestSpeed[client])
		{
			g_fHighestSpeed[client] = g_fLastBugSpeed[client];
		}	
	}

	// Air -> Ground
	if (g_bOnGround[client] && !g_bLastOnGround[client])
	{
		float PlayerOrigin[3];
		GetClientAbsOrigin(client, PlayerOrigin);

		g_fEndGroundHeight[client] = PlayerOrigin[2];
		g_EndHealth[client] = GetEntProp(client, Prop_Send, "m_iHealth");

		if (g_fStartGroundHeight[client] > g_fEndGroundHeight[client])
		{
			if (g_StartHealth[client] == g_EndHealth[client])
			{
				if (g_fHighestSpeed[client] - g_fLastBugSpeed[client] < 20.0)
				{
					float Distance = g_fStartGroundHeight[client] - g_fEndGroundHeight[client];

					if (Distance > g_dist_min_jbug)
					{
						g_bJumpHasBugged[client] = true;
						JumpBugRecord(client, Distance);
					}
				}
			}
		}
	}
}


// Ladder check
public void LadderCheck(int client, float speed)
{
	float PlayerPosition[3];
    float Distance;
	GetClientAbsOrigin(client, PlayerPosition);

	Distance = PlayerPosition[2] - g_fLastPosition[client][2];

	if (GetEntityMoveType(client) == MOVETYPE_LADDER)
	{
		ResetJumpBug(client);
	}

	if (GetEntityMoveType(client) == MOVETYPE_LADDER && Distance > 0.5)
	{
		g_js_AvgLadderSpeed[client] += speed;
		g_js_LadderFrames[client]++;
	}

	if(!(GetEntityFlags(client) & FL_ONGROUND) && GetEntityMoveType(client) == MOVETYPE_WALK && g_LastMoveType[client] == MOVETYPE_LADDER)
	{
		// Start ladder jump
		if (g_js_LadderFrames[client] > 20)
		{
			float AvgSpeed = g_js_AvgLadderSpeed[client] / g_js_LadderFrames[client];
			if (AvgSpeed < 100.0)
            {
			    Prethink(client, true);
            }
        }
	}

	if (g_js_LadderFrames[client] > 0 && GetEntityMoveType(client) != MOVETYPE_LADDER)
	{
		g_js_AvgLadderSpeed[client] = 0.0;
		g_js_LadderFrames[client] = 0;
	}
}

public void SurfCheck(int client)
{
	if (g_js_block_lj_valid[client]) return;

	if (g_js_bPlayerJumped[client] && WallCheck(client))
	{
		ResetJump(client);
	}
}

public void MovementCheck(int client)
{
	MoveType mt = GetEntityMoveType(client);
	if (mt == MOVETYPE_FLYGRAVITY)
	{
		if (g_js_bPlayerJumped[client])
		{
			ResetJump(client);
		}
	}
}

public bool WallCheck(int client)
{
	float Position[3];
	float EndPosition[3];
	float Angles[3];
	float Vectors[3];

	GetClientEyePosition(client, Position);
	GetClientEyeAngles(client, Angles);
	GetAngleVectors(Angles, Vectors, NULL_VECTOR, NULL_VECTOR);

	Angles[1] = -180.0;

	while (Angles[1] != 180.0)
	{
		Handle Trace = TR_TraceRayFilterEx(Position, Angles, MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
		if(TR_DidHit(Trace))
		{
            TR_GetEndPosition(EndPosition, Trace);
            float Distance = GetVectorDistance(EndPosition, Position, false);
            if (Distance <= 25.0)
            {
                CloseHandle(Trace);
                return true;
            }
		}
		CloseHandle(Trace);
		Angles[1] += 15.0;
	}
	return false;
}

public bool RampCheck(int client)
{
	float Position[3];
	float EndPosition[3];
	float Angles[3][3];
	//float Vectors[3];

	static int Tick = 1;

	GetClientAbsOrigin(client, Position);
	//GetClientEyeAngles(client, Angles);
	// GetAngleVectors(Angles, Vectors, NULL_VECTOR, NULL_VECTOR);

	Angles[0][0] = 90.0;
	Angles[0][1] = -180.0;
	Angles[0][2] = 0.0;

	Angles[1][0] = 45.0;
	Angles[1][1] = -180.0;
	Angles[1][2] = 0.0;

	Angles[2][0] = 0.0;
	Angles[2][1] = -180.0;
	Angles[2][2] = 0.0;

	float a = 0.0;
	float b = 0.0;
	float c = 0.0;

	Handle Trace = TR_TraceRayFilterEx(Position, Angles[0], MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
	if(TR_DidHit(Trace))
	{
		TR_GetEndPosition(EndPosition, Trace);
		float Distance = GetVectorDistance(EndPosition, Position, false);

		TE_SetupBeamPoints(Position, EndPosition, BeamSprite , 0, 0, 0, 0.5, 2.0, 2.0, 0, 0.0, {255, 0, 0, 255}, 1);
		TE_SendToAll();
		
		a = Distance;
	}
	CloseHandle(Trace);

	while (Angles[0][1] != 180.0)
	{
		Trace = TR_TraceRayFilterEx(Position, Angles[1], MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
		if(TR_DidHit(Trace))
		{
            TR_GetEndPosition(EndPosition, Trace);
            float Distance = GetVectorDistance(EndPosition, Position, false);
	
			TE_SetupBeamPoints(Position, EndPosition, BeamSprite , 0, 0, 0, 0.5, 2.0, 2.0, 0, 0.0, {0, 255, 0, 255}, 1);
			TE_SendToAll();
            
			c = Distance;
		}
		CloseHandle(Trace);

		Trace = TR_TraceRayFilterEx(Position, Angles[2], MASK_SHOT, RayType_Infinite, TraceEntityFilterPlayer);
		if(TR_DidHit(Trace))
		{
            TR_GetEndPosition(EndPosition, Trace);
            float Distance = GetVectorDistance(EndPosition, Position, false);
	
			TE_SetupBeamPoints(Position, EndPosition, BeamSprite , 0, 0, 0, 0.5, 2.0, 2.0, 0, 0.0, {0, 0, 255, 255}, 1);
			TE_SendToAll();

			b = Distance;

		}
		CloseHandle(Trace);

		Angles[0][1] += 15.0;
		Angles[1][1] += 15.0;
		Angles[2][1] += 15.0;
	}

	if (Tick % 10 == 0)
	{
		PrintToChat(client, "A: %f, B: %f, C: %f", a, b, c);
		PrintToChat(client, "~1/2D: %f, C: %f", SquareRoot(a*a + b*b)/2.0, c);
	}

	Tick++;
	if (Tick > 128) Tick = 1;

	return false;
}

public void WjJumpPreCheck(int client, int &buttons)
{
	if(g_bOnGround[client] && g_js_bPlayerJumped[client] == false && g_js_GroundFrames[client] > 11)
	{
		if (buttons & IN_JUMP || buttons & IN_DUCK)
        {
			g_bLastButtonJump[client] = true;
        }
        else
        {
			g_bLastButtonJump[client] = false;
        }
    }
}

public void CalcJumpMaxSpeed(int client, float speed)
{
	if (g_js_bPlayerJumped[client])
    {
		if (g_js_fMax_Speed[client] <= speed)
		{
			g_js_fMax_Speed[client] = speed;
		}
    }
}

public void CalcJumpHeight(int client)
{
	if (g_js_bPlayerJumped[client])
	{
		float PlayerOrigin[3];
		GetClientAbsOrigin(client, PlayerOrigin);
		
        if (PlayerOrigin[2] > g_js_fMax_Height[client])
		{
			g_js_fMax_Height[client] = PlayerOrigin[2];
		}

		if (PlayerOrigin[2] > g_js_fJump_JumpOff_Pos[client][2])
        {
			g_fFailedLandingPos[client] = PlayerOrigin;
        }
	}
}

public void CalcLastJumpHeight(int client, int &buttons, float origin[3])
{
	if (g_bOnGround[client] && g_js_bPlayerJumped[client] == false && g_js_GroundFrames[client] > 11)
	{
		float Position[3];
		GetClientAbsOrigin(client, Position);
		g_js_fJump_JumpOff_PosLastHeight[client] = Position[2];
	}

    float Distance;
	Distance = GetVectorDistance(g_fLastPosition[client], origin);

	// Is it booster
	if (Distance > 25.0)
	{
		if (g_js_bPlayerJumped[client])
        {
			g_js_bPlayerJumped[client] = false;
        }
	}
}

public void CalcJumpSync(int client, float speed, float ang, int &buttons)
{
	if (g_js_bPlayerJumped[client])
	{
		bool turning_right;
		turning_right = false;

		bool turning_left;
		turning_left = false;

		if (ang < g_fLastAngles[client][1])
		{
            turning_right = true;
        }
		else if (ang > g_fLastAngles[client][1])
        {
			turning_left = true;
        }

		// Strafe stats
		if(turning_left || turning_right)
		{
			if (!g_js_Strafing_AW[client] && ((buttons & IN_FORWARD) || (buttons & IN_MOVELEFT)) && !(buttons & IN_MOVERIGHT) && !(buttons & IN_BACK))
			{
				g_js_Strafing_AW[client] = true;
				g_js_Strafing_SD[client] = false;
				g_js_StrafeCount[client]++;

				int count = g_js_StrafeCount[client];
				if (count < 25)
				{
					g_js_Strafe_Good_Sync[client][g_js_StrafeCount[client]] = 0.0;
					g_js_Strafe_Frames[client][g_js_StrafeCount[client]] = 0;
					g_js_Strafe_Max_Speed[client][g_js_StrafeCount[client]] = speed;
					g_js_Strafe_Air_Time[client][g_js_StrafeCount[client]] = GetEngineTime();
				}
			}
			else if (!g_js_Strafing_SD[client] && ((buttons & IN_BACK) || (buttons & IN_MOVERIGHT)) && !(buttons & IN_MOVELEFT) && !(buttons & IN_FORWARD))
			{
				g_js_Strafing_AW[client] = false;
				g_js_Strafing_SD[client] = true;
				g_js_StrafeCount[client]++;

				if (g_js_StrafeCount[client] < 25)
				{
					g_js_Strafe_Good_Sync[client][g_js_StrafeCount[client]] = 0.0;
					g_js_Strafe_Frames[client][g_js_StrafeCount[client]] = 0;
					g_js_Strafe_Max_Speed[client][g_js_StrafeCount[client]] = speed;
					g_js_Strafe_Air_Time[client][g_js_StrafeCount[client]] = GetEngineTime();
				}
			}
		}

		// Sync
		if (g_fLastSpeed[client] < speed)
		{
			g_js_Good_Sync_Frames[client]++;
			if (0 <= g_js_StrafeCount[client] < 25)
			{
				g_js_Strafe_Good_Sync[client][g_js_StrafeCount[client]]++;
				g_js_Strafe_Gained[client][g_js_StrafeCount[client]] += (speed - g_fLastSpeed[client]);
			}
		}
		else
        {
			if (g_fLastSpeed[client] > speed)
			{
				if (0 <= g_js_StrafeCount[client] < 25)
                {
					g_js_Strafe_Lost[client][g_js_StrafeCount[client]] += (g_fLastSpeed[client] - speed);
                }
			}
        }


		// Strafe frames
		if (0 <= g_js_StrafeCount[client] < 25)
		{
			g_js_Strafe_Frames[client][g_js_StrafeCount[client]]++;
			if (g_js_Strafe_Max_Speed[client][g_js_StrafeCount[client]] < speed)
            {
				g_js_Strafe_Max_Speed[client][g_js_StrafeCount[client]] = speed;
            }
		}

		// Total frames
		g_js_Sync_Frames[client]++;
	}
}

public void LjBlockCheck(int client, float origin[3])
{
	if(g_bLJBlock[client])
	{
		TE_SendBlockPoint(client, g_fDestBlock[client][0], g_fDestBlock[client][1], g_Beam[0]);
		TE_SendBlockPoint(client, g_fOriginBlock[client][0], g_fOriginBlock[client][1], g_Beam[0]);
	}

	if (g_bOnGround[client])
	{
		// LJBlock Stuff
		if (!g_js_bPlayerJumped[client])
		{
			float temp[3];
			if(g_bLJBlock[client])
			{
				g_js_block_lj_valid[client]=true;
				g_js_block_lj_jumpoff_pos[client]=false;
				if(IsCoordInBlockPoint(origin,g_fDestBlock[client],false))
				{
					//block2
					GetEdgeOrigin2(client, origin, temp);
					g_fEdgeDistJumpOff[client] = GetVectorDistance(temp, origin);
					g_js_block_lj_jumpoff_pos[client]=true;
				}
				else
				{
					if (IsCoordInBlockPoint(origin,g_fOriginBlock[client],false))
					{
						//block1
						GetEdgeOrigin1(client, origin, temp);
						g_fEdgeDistJumpOff[client] = GetVectorDistance(temp, origin);
						g_js_block_lj_jumpoff_pos[client]=false;
					}
					else
					{
						g_js_block_lj_valid[client] = false;
					}
				}
			}
			else
			{
				g_js_block_lj_valid[client] = false;
			}
		}
	}
}

public void SetPlayerBeam(int client, float origin[3])
{
	if(!g_bBeam[client] || g_bOnGround[client] || !g_js_bPlayerJumped[client]) return;

	float Vector1[3];
    float Vector2[3];

	Vector1[0] = origin[0];
	Vector1[1] = origin[1];
	Vector1[2] = g_js_fJump_JumpOff_Pos[client][2];
	Vector2[0] = g_fLastPosition[client][0];
	Vector2[1] = g_fLastPosition[client][1];
	Vector2[2] = g_js_fJump_JumpOff_Pos[client][2];
	
    int Color[4] = {255, 255, 255, 100};
    TE_SetupBeamPoints(Vector1, Vector2, g_Beam[2], 0, 0, 0, 2.5, 3.0, 3.0, 10, 0.0, Color, 0);
	
    if (g_bJumpBeam[client])
    {
		TE_SendToClient(client);
    }
}

// Returns client move Speed
public float GetSpeed(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);

	return SquareRoot(Pow(Velocity[0],2.0) + Pow(Velocity[1], 2.0));
}

public float GetSpeedZ(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);

	return SquareRoot(Pow(Velocity[2], 2.0));
}

// Returns client velocity
public float GetVelocity(int client)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", Velocity);
	
    return SquareRoot(Pow(Velocity[0], 2.0) + Pow(Velocity[1], 2.0) + Pow(Velocity[2], 2.0));
}

// Returns Speed from velocity
public float GetVectorSpeed(float Velocity[3])
{
	return SquareRoot(Pow(Velocity[0],2.0) + Pow(Velocity[1], 2.0));
}

// Returns player moving direciton
stock float GetClientMovingDirection(int client, bool ladder)
{
	float Velocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecAbsVelocity", Velocity);

	float EyeAngles[3];
	GetClientEyeAngles(client, EyeAngles);

	if (EyeAngles[0] > 70.0) EyeAngles[0] = 70.0;
	if (EyeAngles[0] < -70.0) EyeAngles[0] = -70.0;

	float ViewDirection[3];

	if (ladder)
		GetEntPropVector(client, Prop_Send, "m_vecLadderNormal", ViewDirection);
	else
		GetAngleVectors(EyeAngles, ViewDirection, NULL_VECTOR, NULL_VECTOR);

	NormalizeVector(Velocity, Velocity);
	NormalizeVector(ViewDirection, ViewDirection);

	float Direction = GetVectorDotProduct(Velocity, ViewDirection);
	if (ladder)
    {
		Direction = Direction * -1;
    }

    return Direction;
}

stock float GetClientMovingRotationOffset(int client)
{
	float PlayerVelocity[3];
	GetEntPropVector(client, Prop_Data, "m_vecVelocity", PlayerVelocity);

	if (!GetVectorLength(PlayerVelocity)) return 0.0;

	float VelocityAngles[3];
	GetVectorAngles(PlayerVelocity, VelocityAngles);

	float EyesAngles[3];
	//GetClientAbsAngles(client, PlayerAngles);
	GetClientEyeAngles(client, EyesAngles);

	if (EyesAngles[1] < 0.0)
	{
		EyesAngles[1] = EyesAngles[1] + 360.0;
	}

	float RotationOffset = VelocityAngles[1] - EyesAngles[1];

	if (RotationOffset > 180.0)
	{
		RotationOffset -= 360.0;
	}

	if (RotationOffset < -180.0)
	{
		RotationOffset += 360.0;
	}

    return RotationOffset;
}

// Valid teleport
public void DoValidTeleport(int client, float origin[3], float angles[3], float vel[3])
{
	if (!IsValidClient(client)) return;

	g_fTeleportValidationTime[client] = GetEngineTime();
	TeleportEntity(client, origin, angles, vel);
}

// Get ground functionality
stock void GetGroundOrigin(int client, float pos[3])
{
	float Origin[3];
    float result[3];

	GetClientAbsOrigin(client, Origin);
	TraceClientGroundOrigin(client, result, 100.0);

	pos = Origin;
	pos[2] = result[2];
}

stock int TraceClientGroundOrigin(int client, float result[3], float offset)
{
	float temp[2][3];
	GetClientEyePosition(client, temp[0]);

	temp[1] = temp[0];
	temp[1][2] -= offset;

	float mins[] = {-16.0, -16.0, 0.0};
	float maxs[] =	{16.0, 16.0, 60.0};

	Handle trace = TR_TraceHullFilterEx(temp[0], temp[1], mins, maxs, MASK_SHOT, TraceEntityFilterPlayer);

	if(TR_DidHit(trace))
	{
		TR_GetEndPosition(result, trace);
		CloseHandle(trace);
		return 1;
	}

	CloseHandle(trace);
	return 0;
}

public bool TraceEntityFilterPlayer(int entity, int contentsMask)
{
    return entity > MaxClients;
}

// Is player on valid pos
public bool IsValidPlayerPos(int client, float vecPos[3])
{
    static const float vecMins[] = { -16.0, -16.0, 0.0 };
    static const float vecMaxs[] = { 16.0, 16.0, 72.0 };

    TR_TraceHullFilter(vecPos, vecPos, vecMins, vecMaxs, MASK_SOLID, TraceFilter_IgnorePlayer, client);

    return (!TR_DidHit(null));
}

public bool TraceFilter_IgnorePlayer(int ent, int mask, int ignore_me)
{
    return (ent != ignore_me);
}

// Sounds
stock void FakePrecacheSound(const char[] szPath)
{
	AddToStringTable(FindStringTable( "soundprecache" ), szPath);
}

public void PlayQuakeSound_Spec(int client, char buffer[255])
{
	int SpecMode;
	bool god;
	if (StrEqual("play *quake/godlike.mp3", buffer) ||
		StrEqual("play *quake/rampage.mp3", buffer) ||
		StrEqual("play *quake/dominating.mp3", buffer))
		god = true;

	for(int x = 1; x <= MaxClients; x++)
	{
		if (IsValidClient(x) && !IsPlayerAlive(x) && (g_EnableQuakeSounds[x] >= 1 && g_EnableQuakeSounds[x] <= 2))
		{
			SpecMode = GetEntProp(x, Prop_Send, "m_iObserverMode");
			if (SpecMode == 4 || SpecMode == 5)
			{
				int Target = GetEntPropEnt(x, Prop_Send, "m_hObserverTarget");
				if (Target == client)
				{
					if (((god == false && g_EnableQuakeSounds[x] == 1) || (god == true && (g_EnableQuakeSounds[x] >= 1 && g_EnableQuakeSounds[x] <= 2)))  && ((god == false && g_ColorChat[x] == 1) || (god == true && g_ColorChat[x] >= 1)))
						ClientCommand(x, buffer);
				}
			}
		}
	}
}


public void PlayLeetJumpSound(int client)
{
	char buffer[255];

	// All sound
	if (g_js_GODLIKE_Count[client] == 3 || g_js_GODLIKE_Count[client] == 5)
	{
		for (int i = 1; i <= MaxClients; i++)
		{
			if(IsValidClient(i) && i != client && g_ColorChat[i] >= 1 && (g_EnableQuakeSounds[i] >= 1 && g_EnableQuakeSounds[i] <= 2))
			{
				if (g_js_GODLIKE_Count[client]==3)
				{
					Format(buffer, sizeof(buffer), "play %s", GODLIKE_RAMPAGE_RELATIVE_SOUND_PATH);
					ClientCommand(i, buffer);
				}
				else if (g_js_GODLIKE_Count[client]==5)
				{
					Format(buffer, sizeof(buffer), "play %s", GODLIKE_DOMINATING_RELATIVE_SOUND_PATH);
					ClientCommand(i, buffer);
				}
			}
		}
	}

	// Client sound
	if 	(IsValidClient(client) && (g_EnableQuakeSounds[client] >= 1 && g_EnableQuakeSounds[client] <= 2))
	{
		if (g_js_GODLIKE_Count[client] != 3 && g_js_GODLIKE_Count[client] != 5 && g_EnableQuakeSounds[client])
		{
			Format(buffer, sizeof(buffer), "play %s", GODLIKE_RELATIVE_SOUND_PATH);
			ClientCommand(client, buffer);
		}
		else if (g_js_GODLIKE_Count[client]==3 && g_EnableQuakeSounds[client])
		{
			Format(buffer, sizeof(buffer), "play %s", GODLIKE_RAMPAGE_RELATIVE_SOUND_PATH);
			ClientCommand(client, buffer);
		}
		else if (g_js_GODLIKE_Count[client]==5 && g_EnableQuakeSounds[client])
		{
			Format(buffer, sizeof(buffer), "play %s", GODLIKE_DOMINATING_RELATIVE_SOUND_PATH);
			ClientCommand(client, buffer);
		}
	}
}

public void PlayOrangeJumpSound(int client)
{
	char buffer[255];

	//all sound
	for (int i = 1; i <= MaxClients; i++)
	{
		if 	(IsValidClient(i) && i != client && g_ColorChat[i] >= 1 && (g_EnableQuakeSounds[i] >= 1 && g_EnableQuakeSounds[i] <= 2))
		{
			Format(buffer, sizeof(buffer), "play %s", OWNAGE3_RELATIVE_SOUND_PATH);
			ClientCommand(i, buffer);
		}
	}

	//client sound
	if (IsValidClient(client) && (g_EnableQuakeSounds[client] >= 1 && g_EnableQuakeSounds[client] <= 2))
	{
		Format(buffer, sizeof(buffer), "play %s", OWNAGE3_RELATIVE_SOUND_PATH);
		ClientCommand(client, buffer);
	}
}

public void PlayGoldenJumpSound(int client)
{
	char buffer[255];

	//all sound
	for (int i = 1; i <= MaxClients; i++)
	{
		if 	(IsValidClient(i) && i != client && g_ColorChat[i] >= 1 && (g_EnableQuakeSounds[i] >= 1 && g_EnableQuakeSounds[i] <= 2))
		{
			Format(buffer, sizeof(buffer), "play %s", GOLDEN_RELATIVE_SOUND_PATH);
			ClientCommand(i, buffer);
		}
	}

	//client sound
	if (IsValidClient(client) && (g_EnableQuakeSounds[client] >= 1 && g_EnableQuakeSounds[client] <= 2))
	{
		Format(buffer, sizeof(buffer), "play %s", GOLDEN_RELATIVE_SOUND_PATH);
		ClientCommand(client, buffer);
	}
}

public void PlayTopOneSound(int client)
{
	char buffer[255];

	//all sound
	for (int i = 1; i <= MaxClients; i++)
	{
		if 	(IsValidClient(i) && i != client && g_ColorChat[i] >= 1 && (g_EnableQuakeSounds[i] >= 1 && g_EnableQuakeSounds[i] <= 2))
		{
			Format(buffer, sizeof(buffer), "play %s", W_HOLYSHIT_RELATIVE_SOUND_PATH);
			ClientCommand(i, buffer);
		}
	}

	//client sound
	if (IsValidClient(client) && (g_EnableQuakeSounds[client] >= 1 && g_EnableQuakeSounds[client] <= 2))
	{
		Format(buffer, sizeof(buffer), "play %s", W_HOLYSHIT_RELATIVE_SOUND_PATH);
		ClientCommand(client, buffer);
	}
}

public void PlayJumpBugGodlikeSound(int client)
{
	char buffer[255];

	//all sound
	for (int i = 1; i <= MaxClients; i++)
	{
		if 	(IsValidClient(i) && i != client && g_ColorChat[i] >= 1 && (g_EnableQuakeSounds[i] >= 1 && g_EnableQuakeSounds[i] <= 2))
		{
			Format(buffer, sizeof(buffer), "play %s", W_GODLIKE_RELATIVE_SOUND_PATH);
			ClientCommand(i, buffer);
		}
	}

	//client sound
	if (IsValidClient(client) && (g_EnableQuakeSounds[client] >= 1 && g_EnableQuakeSounds[client] <= 2))
	{
		Format(buffer, sizeof(buffer), "play %s", W_GODLIKE_RELATIVE_SOUND_PATH);
		ClientCommand(client, buffer);
	}
}

// LJ 
stock int TraceClientViewEntity(int client)
{
	float PlayerOrigin[3];
	float PlayerAngle[3];

	GetClientEyePosition(client, PlayerOrigin);
	GetClientEyeAngles(client, PlayerAngle);

	Handle Trace = TR_TraceRayFilterEx(PlayerOrigin, PlayerAngle, MASK_VISIBLE, RayType_Infinite, TRDontHitSelf, client);
	int  Entity = -1;
	
	if (TR_DidHit(Trace))
	{
		Entity = TR_GetEntityIndex(Trace);
		
		CloseHandle(Trace);
		return Entity;
	}
	
	CloseHandle(Trace);
	return -1;
}

public bool TRDontHitSelf(int entity, int mask, any data)
{
	if (entity == data)
	{
		return false;
	}
	return true;
}


// Measure
void GetPos(int client, int arg)
{
	float PlayerOrigin[3];
	float PlayerAngles[3];

	GetClientEyePosition(client, PlayerOrigin);
	GetClientEyeAngles(client, PlayerAngles);

	Handle trace = TR_TraceRayFilterEx(PlayerOrigin, PlayerAngles, MASK_SHOT, RayType_Infinite, TraceFilterPlayers, client);
	if (!TR_DidHit(trace))
	{
		CloseHandle(trace);
		PrintToChat(client, "%t", "Measure3",MOSSGREEN,WHITE);
		return;
	}

	TR_GetEndPosition(PlayerOrigin, trace);
	CloseHandle(trace);

	g_fvMeasurePos[client][arg][0] = PlayerOrigin[0];
	g_fvMeasurePos[client][arg][1] = PlayerOrigin[1];
	g_fvMeasurePos[client][arg][2] = PlayerOrigin[2];

	PrintToChat(client, "%t", "Measure4", MOSSGREEN, WHITE, arg + 1, PlayerOrigin[0], PlayerOrigin[1], PlayerOrigin[2]);

	if (arg == 0)
	{
		if (g_hP2PRed[client] != INVALID_HANDLE)
		{
			CloseHandle(g_hP2PRed[client]);
			g_hP2PRed[client] = INVALID_HANDLE;
		}

		g_bMeasurePosSet[client][0] = true;
		g_hP2PRed[client] = CreateTimer(1.0, Timer_P2PRed, client, TIMER_REPEAT);
		P2PXBeam(client, 0);
	}
	else
	{
		if (g_hP2PGreen[client] != INVALID_HANDLE)
		{
			CloseHandle(g_hP2PGreen[client]);
			g_hP2PGreen[client] = INVALID_HANDLE;
		}

		g_bMeasurePosSet[client][1] = true;
		P2PXBeam(client, 1);
		g_hP2PGreen[client] = CreateTimer(1.0, Timer_P2PGreen, client, TIMER_REPEAT);
	}
}

public Action Timer_P2PRed(Handle timer, any client)
{
	P2PXBeam(client, 0);
}

public Action Timer_P2PGreen(Handle timer, any client)
{
	P2PXBeam(client, 1);
}

void P2PXBeam(int client, int arg)
{
	float Origin0[3];
	float Origin1[3];
	float Origin2[3];
	float Origin3[3];

	Origin0[0] = (g_fvMeasurePos[client][arg][0] + 8.0);
	Origin0[1] = (g_fvMeasurePos[client][arg][1] + 8.0);
	Origin0[2] = g_fvMeasurePos[client][arg][2];
	Origin1[0] = (g_fvMeasurePos[client][arg][0] - 8.0);
	Origin1[1] = (g_fvMeasurePos[client][arg][1] - 8.0);
	Origin1[2] = g_fvMeasurePos[client][arg][2];
	Origin2[0] = (g_fvMeasurePos[client][arg][0] + 8.0);
	Origin2[1] = (g_fvMeasurePos[client][arg][1] - 8.0);
	Origin2[2] = g_fvMeasurePos[client][arg][2];
	Origin3[0] = (g_fvMeasurePos[client][arg][0] - 8.0);
	Origin3[1] = (g_fvMeasurePos[client][arg][1] + 8.0);
	Origin3[2] = g_fvMeasurePos[client][arg][2];

	if (arg == 0)
	{
		Beam(client, Origin0, Origin1, 0.97, 2.0, 255, 0, 0);
		Beam(client, Origin2, Origin3, 0.97, 2.0, 255, 0, 0);
	}
	else
	{
		Beam(client, Origin0, Origin1, 0.97, 2.0, 0, 255, 0);
		Beam(client, Origin2, Origin3, 0.97, 2.0, 0, 255, 0);
	}
}


void Beam(int client, float vecStart[3], float vecEnd[3], float life, float width, int r, int g, int b)
{
	TE_Start("BeamPoints");
	TE_WriteNum("m_nModelIndex",g_Beam[2]);
	TE_WriteNum("m_nHaloIndex",0);
	TE_WriteNum("m_nStartFrame",0);
	TE_WriteNum("m_nFrameRate",0);
	TE_WriteFloat("m_fLife",life);
	TE_WriteFloat("m_fWidth",width);
	TE_WriteFloat("m_fEndWidth",width);
	TE_WriteNum("m_nFadeLength",0);
	TE_WriteFloat("m_fAmplitude",0.0);
	TE_WriteNum("m_nSpeed",0);
	TE_WriteNum("r",r);
	TE_WriteNum("g",g);
	TE_WriteNum("b",b);
	TE_WriteNum("a",255);
	TE_WriteNum("m_nFlags",0);
	TE_WriteVector("m_vecStartPoint",vecStart);
	TE_WriteVector("m_vecEndPoint",vecEnd);
	TE_SendToClient(client);
}

void ResetPos(int client)
{
	if (g_hP2PRed[client] != INVALID_HANDLE)
	{
		CloseHandle(g_hP2PRed[client]);
		g_hP2PRed[client] = INVALID_HANDLE;
	}
	if (g_hP2PGreen[client] != INVALID_HANDLE)
	{
		CloseHandle(g_hP2PGreen[client]);
		g_hP2PGreen[client] = INVALID_HANDLE;
	}

	g_bMeasurePosSet[client][0] = false;
	g_bMeasurePosSet[client][1] = false;

	g_fvMeasurePos[client][0][0] = 0.0; 
	g_fvMeasurePos[client][0][1] = 0.0;
	g_fvMeasurePos[client][0][2] = 0.0;
	g_fvMeasurePos[client][1][0] = 0.0;
	g_fvMeasurePos[client][1][1] = 0.0;
	g_fvMeasurePos[client][1][2] = 0.0;
}

public bool TraceFilterPlayers(int entity, int contentsMask)
{
	return (entity > MaxClients) ? true : false;
}

// Anti Cheat
public void BhopPatternCheck(int client)
{
	if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client])) || g_fafAvgPerfJumps[client] < 0.6 || g_fafAvgSpeed[client] < 300.0)
		return;

	int pattern_array[50];
	int pattern_sum;
	int jumps;

	// Analyse the last jumps
	for (int i = 0; i < 30; i++)
	{
		int value = g_aaiLastJumps[client][i];
		if (1 < value < 50)
		{
			pattern_sum += value;
			jumps++;
			pattern_array[value]++;
		}
	}

	// pattern check #1
	float avg_scroll_pattern = float(pattern_sum) / float(jumps);
	if (avg_scroll_pattern > 30.0 && g_fafAvgSpeed[client] > 330.0)
	{
		char Message[128];
		char name[64];
		char auth[64];
		
		GetClientName(client, name,64);
		GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

		Format(Message, sizeof(Message), "%s[%s] %T", name, auth, "Macro_Suspected", client);
		PrintWarningToAdmins(Message);

		LogToFile(AntiCheatLogFile, "[AUTO] %s[%s] - is suspected, pattern: %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", name, auth,
			g_aaiLastJumps[client][0],
			g_aaiLastJumps[client][1],
			g_aaiLastJumps[client][2],
			g_aaiLastJumps[client][3],
			g_aaiLastJumps[client][4],
			g_aaiLastJumps[client][5],
			g_aaiLastJumps[client][6],
			g_aaiLastJumps[client][7],
			g_aaiLastJumps[client][8],
			g_aaiLastJumps[client][9],
			g_aaiLastJumps[client][10],
			g_aaiLastJumps[client][11],
			g_aaiLastJumps[client][12],
			g_aaiLastJumps[client][13],
			g_aaiLastJumps[client][14],
			g_aaiLastJumps[client][15],
			g_aaiLastJumps[client][16],
			g_aaiLastJumps[client][17],
			g_aaiLastJumps[client][18],
			g_aaiLastJumps[client][19],
			g_aaiLastJumps[client][20],
			g_aaiLastJumps[client][21],
			g_aaiLastJumps[client][22],
			g_aaiLastJumps[client][23],
			g_aaiLastJumps[client][24],
			g_aaiLastJumps[client][25]
		);

		return;
	}
    
	// pattern check #2
	for (int j = 2; j < 50; j++)
	{
		if (pattern_array[j] >= 22)
		{
			char Message[128];
			char name[64];
			char auth[64];
			
			GetClientName(client, name,64);
            GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

			Format(Message, sizeof(Message), "%s[%s] %T", name, auth, "Macro_Suspected", client);
			PrintWarningToAdmins(Message);

			LogToFile(AntiCheatLogFile, "[AUTO] %s[%s] - is suspected, pattern: %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i", name, auth,
			g_aaiLastJumps[client][0],
			g_aaiLastJumps[client][1],
			g_aaiLastJumps[client][2],
			g_aaiLastJumps[client][3],
			g_aaiLastJumps[client][4],
			g_aaiLastJumps[client][5],
			g_aaiLastJumps[client][6],
			g_aaiLastJumps[client][7],
			g_aaiLastJumps[client][8],
			g_aaiLastJumps[client][9],
			g_aaiLastJumps[client][10],
			g_aaiLastJumps[client][11],
			g_aaiLastJumps[client][12],
			g_aaiLastJumps[client][13],
			g_aaiLastJumps[client][14],
			g_aaiLastJumps[client][15],
			g_aaiLastJumps[client][16],
			g_aaiLastJumps[client][17],
			g_aaiLastJumps[client][18],
			g_aaiLastJumps[client][19],
			g_aaiLastJumps[client][20],
			g_aaiLastJumps[client][21],
			g_aaiLastJumps[client][22],
			g_aaiLastJumps[client][23],
			g_aaiLastJumps[client][24],
			g_aaiLastJumps[client][25]
			);

			return;
		}
	}
}

public void PrintWarningToAdmins(char[] Message)
{
	for (int client = 0; client < MAXPLAYERS; client++)
	{
		if (IsValidClient(client) && (GetUserFlagBits(client) & ADMFLAG_KICK))
		{
			PrintToChat(client,"[%c%T%c] %s", RED, "Admin_Warning", client, WHITE, Message);
		}
	}
}

public void PerformStats(int client, int target, bool console_only)
{
	if (IsValidClient(client) && IsValidClient(target))
	{
		char banstats[512];

		float perf = g_fafAvgPerfJumps[target] * 100;
		char map[128];
		char szName[64];

		GetClientName(target, szName,64);
		GetCurrentMap(map, 128);
		
		// First 
		Format(banstats, 512, "%c%T%c: %c%s%c",
			LIMEGREEN,
			"BHopStats_Player",
			client,
			WHITE,
			GREEN,
			szName,
			WHITE
		);

		if (!console_only)
        {
			PrintToChat(client, "[%cHNS%c] %s", MOSSGREEN, WHITE, banstats);
        }
		PrintToConsole(client, "[HNS] %s", banstats);

		Format(banstats, 512, "%c%T%c: %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i",
			LIMEGREEN,
			"BHopStats_Pattern",
			client,
			WHITE,
			g_aaiLastJumps[target][0],
			g_aaiLastJumps[target][1],
			g_aaiLastJumps[target][2],
			g_aaiLastJumps[target][3],
			g_aaiLastJumps[target][4],
			g_aaiLastJumps[target][5],
			g_aaiLastJumps[target][6],
			g_aaiLastJumps[target][7],
			g_aaiLastJumps[target][8],
			g_aaiLastJumps[target][9],
			g_aaiLastJumps[target][10],
			g_aaiLastJumps[target][11],
			g_aaiLastJumps[target][12],
			g_aaiLastJumps[target][13],
			g_aaiLastJumps[target][14],
			g_aaiLastJumps[target][15],
			g_aaiLastJumps[target][16],
			g_aaiLastJumps[target][17],
			g_aaiLastJumps[target][18],
			g_aaiLastJumps[target][19],
			g_aaiLastJumps[target][20],
			g_aaiLastJumps[target][21],
			g_aaiLastJumps[target][22],
			g_aaiLastJumps[target][23],
			g_aaiLastJumps[target][24],
			g_aaiLastJumps[target][25]
		);

		if (!console_only)
        {
			PrintToChat(client, "[%cHNS%c] %s", MOSSGREEN, WHITE, banstats);
        }
		PrintToConsole(client, "[HNS] %s", banstats);

		Format(banstats, 512, "%c%T%c: %.1f/%.1f",
			LIMEGREEN,
			"BHopStats_AVG",
			client,
			WHITE,
			g_fafAvgJumps[target],
			g_fafAvgSpeed[target]
		);

		if (!console_only)
        {
			PrintToChat(client, "[%cHNS%c] %s", MOSSGREEN, WHITE, banstats);
        }
		PrintToConsole(client, "[HNS] %s", banstats);

		Format(banstats, 512, "%c%T%c: %.2f%c",
			LIMEGREEN,
			"BHopStats_PJR",
			client,
			WHITE,
			perf,
			PERCENT
		);

		if (!console_only)
        {
			PrintToChat(client, "[%cHNS%c] %s", MOSSGREEN, WHITE, banstats);
        }
		PrintToConsole(client, "[HNS] %T: %i, %T: %i", "BHopStats_FPSM", client, g_fps_max[target], "BHopStats_TR", client, g_Server_Tickrate);
	}
}

public void FPSCheck(QueryCookie cookie, int client, ConVarQueryResult result, const char[] cvarName, const char[] cvarValue)
{
	if (IsValidClient(client))
	{
		g_fps_max[client] = StringToInt(cvarValue);    	
	}
}


/*
#define LIFE_ALIVE		0
#define LIFE_DYING		1
#define LIFE_DEAD		2
#define LIFE_RESPAWNABLE 	3
*/
void Redie(int client)
{
	if (IsValidClient(client))
	{
		if (!IsPlayerAlive(client))
		{
			if (GetClientTeam(client) > 1)
			{
				if (g_bPlayerGhostMode[client] == false)
				{
					CS_RespawnPlayer(client);

					/*
						int weaponIndex;
						for (int i = 0; i <= 3; i++)
						{
							if ((weaponIndex = GetPlayerWeaponSlot(client, i)) != -1)
							{
								RemovePlayerItem(client, weaponIndex);
								RemoveEdict(weaponIndex);
							}
						}
					*/

					SetEntProp(client, Prop_Send, "m_lifeState", 1);
					SetEntData(client, FindSendPropInfo("CBaseEntity", "m_CollisionGroup"), 2, 4, true);
					SetEntProp(client, Prop_Data, "m_ArmorValue", 0);
					SetEntProp(client, Prop_Send, "m_bHasDefuser", 0);
					g_bPlayerGhostMode[client] = true;

					PrintToChat(client, "\x01[\x03Redie\x01] GhostMode: \x04ON");
				}
			}
		}
	}
}

// No Clip
void ToggleNoClip(int client)
{
	if (IsValidClient(client, true))
	{
		int Team = GetClientTeam(client);
		if (Team == 3 || Team == 2)
		{
			MoveType PlayerMove = GetEntityMoveType(client);
			if (PlayerMove == MOVETYPE_WALK)
			{
				if (g_js_bPlayerJumped[client])
				{
					ResetJump(client);
				}
				ResetJumpBug(client);

				SetEntityMoveType(client, MOVETYPE_NOCLIP);
				// SetEntityRenderMode(client, RENDER_NONE);
			}
			else if (PlayerMove == MOVETYPE_NOCLIP)
			{
				if (g_js_bPlayerJumped[client])
				{
					ResetJump(client);
				}
				ResetJumpBug(client);

				SetEntityMoveType(client, MOVETYPE_WALK);
				// SetEntityRenderMode(client, RENDER_TRANSCOLOR);
			}
		}
	}
}

void NoClipCheck(int client)
{
	MoveType PlayerMove = GetEntityMoveType(client);

	if (PlayerMove == MOVETYPE_NOCLIP)
	{
		if (g_js_bPlayerJumped[client])
		{
			ResetJump(client);
		}

		ResetJumpBug(client);
		ResetSlide(client);
	}
}

void ToggleWeaponModels(int client)
{
	if (!g_bViewWeaponModels[client])
	{
		g_bViewWeaponModels[client] = true;
		Client_SetDrawViewModel(client, true);
	}
	else
	{
		g_bViewWeaponModels[client] = false;
		Client_SetDrawViewModel(client, false);
	}
}

void Client_SetDrawViewModel(int client, bool drawViewModel)
{
	SetEntProp(client, Prop_Send, "m_bDrawViewmodel", drawViewModel);
}
	