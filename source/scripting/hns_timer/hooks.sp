void InitHooks()
{
    // Hooks
	HookEvent("round_start", 	OnRoundStart, EventHookMode_Post);	
    HookEvent("round_end", 	    OnRoundEnd, EventHookMode_Post);	
    
    HookEvent("player_spawn",   OnPlayerSpawn, EventHookMode_Post);
	HookEvent("player_jump",    OnPlayerJump, EventHookMode_Pre);
    HookEvent("player_jump",    OnJumpMacroDox, EventHookMode_Post);

	HookEntityOutput("trigger_teleport", "OnStartTouch", Teleport_OnStartTouch);
	HookEntityOutput("trigger_multiple", "OnStartTouch", Teleport_OnStartTouch);
	HookEntityOutput("trigger_teleport", "OnEndTouch", Teleport_OnEndTouch);
	HookEntityOutput("trigger_multiple", "OnEndTouch", Teleport_OnEndTouch);
	HookEntityOutput("trigger_gravity", "OnStartTouch", Trigger_GravityTouch);
	HookEntityOutput("trigger_gravity", "OnEndTouch", Trigger_GravityTouch);

	//Hooks normal sounds
	AddNormalSoundHook(OnNormalSoundPlayed);
}


public Action OnRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	g_JumpBugCooldown = 15;

	//hook ent stuff
	int ent = -1;
	SDKHook(0, SDKHook_Touch, Touch_Wall);	

	while((ent = FindEntityByClassname(ent,"func_breakable")) != -1)
    {
		SDKHook(ent, SDKHook_Touch, Touch_Wall);
    }

	ent = -1;
	while((ent = FindEntityByClassname(ent,"func_illusionary")) != -1)
    {
		SDKHook(ent, SDKHook_Touch, Touch_Wall);
    }

	ent = -1;
	while((ent = FindEntityByClassname(ent,"func_wall")) != -1)
    {
		SDKHook(ent, SDKHook_Touch, Touch_Wall);
    }

	ent = -1;
	while((ent = FindEntityByClassname(ent, "trigger_push")) != -1)
    {
		SDKHook(ent, SDKHook_Touch, Push_Touch);
    }

	ent = -1;	
	while((ent = FindEntityByClassname(ent, "trigger_gravity")) != -1)
    {
		SDKHook(ent, SDKHook_Touch, Push_Touch);		
    }

	// diaable helicopter rotors to prevent abusing
	ent = -1;
	while((ent = FindEntityByClassname(ent, "func_rotating")) != -1)
	{
		SDKHook(ent,SDKHook_Touch,Push_Touch);
		
        char iname[64];
		GetEntPropString(ent, Prop_Data, "m_iName", iname, sizeof(iname));	
		
        if (!StrEqual(iname,""))
		{
			for (int y = 0; y < GetEntityCount(); y++)
			{
				char classname[32];
				if (IsValidEdict(y) && GetEntityClassname(y, classname, 32))
				{
					GetEntPropString(y, Prop_Data, "m_iName", iname, sizeof(iname));	
					if (StrContains(iname,"rotor") != -1)
						if (IsValidEntity(ent))
							SetEntProp(ent, Prop_Send, "m_nSolidType", 2);
				}
			}		
		}
	}

    for (int client = 0; client < MAXPLAYERS; client++)
    {
        if (IsValidClient(client))
        {
            // Ghost Mode OFF
            g_bPlayerGhostMode[client] = false;

            // Set MVP
            CS_SetMVPCount(client, GetClientRecordsAmount(client));
        }
    }

    // Update activity
    if (g_LastUpdateTime <= 0)
    {
        g_LastUpdateTime = GetTime(); 
    }
    else if (g_LastUpdateTime < GetTime())
    {
        int TimeToAdd = RoundFloat((GetTime() - g_LastUpdateTime) / 60.0);

        if (TimeToAdd > 0)
        {
            for (int client = 0; client < MAXPLAYERS; client++)
            {
                if (IsValidClient(client))
                {
                    int CurrActivity = GetClientActivity(client);
                    if (CurrActivity >= 0)
                    {
                        SetClientActivity(client, CurrActivity + TimeToAdd);
                    }
                }
            }

            g_LastUpdateTime = GetTime(); 
        } 
    }

	// OnPluginPauseChange(false);
	return Plugin_Continue; 
}


public Action OnRoundEnd(Event event, const char[] name, bool dontBroadcast)
{
    // Set MVP
    for (int client = 0; client < MAXPLAYERS; client++)
    {
        if (IsValidClient(client))
        {
            CS_SetMVPCount(client, GetClientRecordsAmount(client));
        }
    }

	// OnPluginPauseChange(false);
	return Plugin_Continue; 
}

/*
public Action:Event_OnRoundEnd(Handle:event, const String:name[], bool:dontBroadcast)
{
	g_bRoundEnd=true;
			
	//Unhook ent stuff
	new ent = -1;
	SDKUnhook(0,SDKHook_Touch,Touch_Wall);	
	while((ent = FindEntityByClassname(ent,"func_breakable")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Touch_Wall);
	ent = -1;
	while((ent = FindEntityByClassname(ent,"func_illusionary")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Touch_Wall);
	ent = -1;
	while((ent = FindEntityByClassname(ent,"func_wall")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Touch_Wall);
	ent = -1;
	while((ent = FindEntityByClassname(ent, "trigger_push")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Push_Touch);
	ent = -1;
	while((ent = FindEntityByClassname(ent, "trigger_gravity")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Push_Touch);	
	ent = -1;	
	while((ent = FindEntityByClassname(ent, "func_rotating")) != -1)
		SDKUnhook(ent,SDKHook_Touch,Push_Touch);	
	return Plugin_Continue;
}


public Action:Event_OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{

}
*/

public Action OnPlayerSpawn(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
   
    if (IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))
    {  
        // Defaults
        g_js_bPlayerJumped[client] = false;
        g_fSpawnTime[client] = GetEngineTime();
	    g_fLastSpeed[client] = GetSpeed(client);

        // Reset Slide
        ResetSlide(client);
        ResetJump(client);
        ResetJumpBug(client);
        
        QueryClientConVar(client, "fps_max", view_as<ConVarQueryFinished>(FPSCheck), client);	

        g_PlayerJumpbugCooldown[client] = 5;

        if (g_bViewWeaponModels[client])
        {
            Client_SetDrawViewModel(client, true);
        }
        else
        {
            Client_SetDrawViewModel(client, false);
        }   

        SDKHook(client, SDKHook_SetTransmit, OnSetTransmit);  
    }
}

public Action OnPlayerJump(Handle event, const char[] name, bool dontBroadcast)
{	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));

    if (IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))
    {  
        g_JumpCheck2[client]++;
        g_bBeam[client] = true;	

        if (!g_bOnGround[client]) // Detect jumpbug
        {
            g_bJumpBugged[client] = true;
        }

        if (!WallCheck(client))
        {
            Prethink(client, false);
        }
    }
}

public Action OnJumpMacroDox(Handle Event3, const char[] Name, bool Broadcast)
{
	int client = GetClientOfUserId(GetEventInt(Event3, "userid"));	
	
    if (IsValidClient(client))
	{	
		g_fafAvgJumps[client] = (g_fafAvgJumps[client] * 9.0 + float(g_aiJumps[client])) / 10.0;	
		
        float vec_vel[3];
		GetEntPropVector(client, Prop_Data, "m_vecVelocity", vec_vel);
		vec_vel[2] = 0.0;
		
        float speed;
		speed = GetVectorLength(vec_vel);

		g_fafAvgSpeed[client] = (g_fafAvgSpeed[client] * 9.0 + speed) / 10.0;
		g_aaiLastJumps[client][g_NumberJumpsAbove[client]] = g_aiJumps[client];
		g_NumberJumpsAbove[client]++;

		if (g_NumberJumpsAbove[client] == 30)
		{
			g_NumberJumpsAbove[client] = 0;
		}			

		g_aiJumps[client] = 0;		
		if (g_fafAvgPerfJumps[client] >= 0.9)
		{
		    char Message[128];
			char name[64];
			char auth[64];
			
			GetClientName(client, name, 64);
            GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

			Format(Message, sizeof(Message), "%s[%s] %T", name, auth, "Macro_Suspected", client);
			PrintWarningToAdmins(Message);
		}
	}
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
    // if (!IsValidClient(client, true)) return Plugin_Continue;

    if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))) return Plugin_Continue;

	static int g_LastFlags[MAXPLAYERS] = 0;
	// static bool g_bLastOnGround[MAXPLAYERS] = false;

    // Prestrafe
    if (g_js_fPreStrafe[client] > g_fBhopSpeedCap)
    {
        g_js_fPreStrafe[client] = g_fBhopSpeedCap;
    }		

    // Timer bases
    float PlayerSpeed;
    float PlayerOrigin[3];
    float PlayerAngles[3];

    // Player current buttons
    g_CurrentButton[client] = buttons;

    // Player current state
    GetClientAbsOrigin(client, PlayerOrigin);
    GetClientEyeAngles(client, PlayerAngles);

    MoveType PlayerMoveType = GetEntityMoveType(client);
    PlayerSpeed = GetSpeed(client);		

    // Is player on ground
    if (GetEntityFlags(client) & FL_ONGROUND)	
    {	
        g_bOnGround[client] = true;
    }
    else
    {
        g_bOnGround[client] = false;		
    }

    // Player last jump time
    if (buttons & IN_JUMP)
    {
        g_fJumpButtonLastTimeUsed[client] = GetEngineTime();
    }

    // Player last duck time
    if (buttons & IN_DUCK)
    {
        g_fCrouchButtonLastTimeUsed[client] = GetEngineTime();
    }

    // Is it a perfect jump
    if (g_bOnGround[client])
    {	
        if (buttons & IN_JUMP &&  !(buttons & IN_DUCK))
        {
            if ((g_LastButton[client] & IN_FORWARD) && !(buttons & IN_FORWARD))
                g_js_bPerfJumpOff2[client] = true;
        }

        if (!(g_LastButton[client] & IN_DUCK) && !(g_LastButton[client] & IN_JUMP) && (g_LastButton[client] & IN_FORWARD))
        {
            if ((buttons & IN_JUMP) && (buttons & IN_DUCK))
                g_js_bPerfJumpOff[client] = true;	
            if (!(buttons & IN_FORWARD))
                g_js_bPerfJumpOff2[client] = true;	
        }
    }

    // Checks is player uses left-right script
    if ((buttons & IN_LEFT) || (buttons & IN_RIGHT))
    {
        if ((GetEngineTime() - g_fSpawnTime[client]) > 3.0)
        {
            PrintToChat(client, "You have been slayed for using a strafe/turn bind!");
            ForcePlayerSuicide(client);
            ResetJump(client);
        }
    }

    // Counts ground frames
    if (!g_js_bPlayerJumped[client] && g_bOnGround[client] && (((buttons & IN_MOVERIGHT) || (buttons & IN_MOVELEFT) || (buttons & IN_BACK) || (buttons & IN_FORWARD))))
    {
        g_js_GroundFrames[client]++;
    }

    // Count duck frames
    if (g_js_GroundFrames[client] > 18 && g_js_DuckCounter[client] > 0)
    {
        g_js_DuckCounter[client]--;
    }

    /*
    float fLastUndo = GetEngineTime() - g_fLastUndo[client];
    if (fLastUndo < 1.0 && g_bOnBhopPlattform[client])
    {
        EmitSoundToClient(client,"buttons/button10.wav",client);
        PrintToChat(client,"[%cHNS%c] %cUndo-TP is not allowed on bhop blocks!",MOSSGREEN,WHITE,RED);
        g_bOnBhopPlattform[client] = false;
        DoTeleport(client,0);		
        float f3pos[3];
        float f3ang[3];
        GetClientAbsAngles(client,f3ang);
        GetClientAbsOrigin(client,f3pos);
        g_fPlayerCordsUndoTp[client] = f3pos;
        g_fPlayerAnglesUndoTp[client] =f3ang;			
    }	
    */

    // Physics
    SpeedCap(client);
    DoubleDuck(client, buttons);
    Prestrafe(client, PlayerAngles[1], buttons);
    NoClipCheck(client);
    WaterCheck(client);
    LadderCheck(client, PlayerSpeed);
    DuckingCheck(client);
    JumpBugCheck(client);

    // Booster ???
	BoosterCheck(client);

    // Ramp boost
    SlopeBoostFix(client);
    
    // Slide
    Slide(client);

    // Jumpstats
	WjJumpPreCheck(client, buttons);

    // Calculations
    CalcJumpMaxSpeed(client, PlayerSpeed);
	CalcJumpHeight(client);
	CalcJumpSync(client, PlayerSpeed, PlayerAngles[1], buttons);
	CalcLastJumpHeight(client, buttons, PlayerOrigin);

    // Block check
    LjBlockCheck(client, PlayerOrigin);

    // Check binds
	// BindCheck(client, buttons);

    // Draw beam
	SetPlayerBeam(client, PlayerOrigin);

    // After
    static bool bHoldingJump[MAXPLAYERS];
    static bLastOnGround[MAXPLAYERS];

    // Duck speed fix
    /*
    float DuckSpeed = GetEntPropFloat(client, Prop_Data, "m_flDuckSpeed");
    if (!bLastOnGround[client] && (GetEntityFlags(client) & FL_ONGROUND))
    {
        if(DuckSpeed < 7)
        {
            SetEntPropFloat(client, Prop_Send, "m_flDuckSpeed", 7.0, 0);
        }
    }*/

    // New duck speed fix
    SetEntPropFloat(client, Prop_Send, "m_flDuckSpeed", 10.0, 0);

    // AntyCheat - BunnyHop
    if(buttons & IN_JUMP)
    {
        if(!bHoldingJump[client])
        {
            bHoldingJump[client] = true;
            g_aiJumps[client]++;

            if (bLastOnGround[client] && (GetEntityFlags(client) & FL_ONGROUND))
            {
                g_fafAvgPerfJumps[client] = ( g_fafAvgPerfJumps[client] * 9.0 + 0 ) / 10.0;
            }
            else
            { 
                if (!bLastOnGround[client] && (GetEntityFlags(client) & FL_ONGROUND))
                {
                    g_fafAvgPerfJumps[client] = ( g_fafAvgPerfJumps[client] * 9.0 + 1 ) / 10.0;
                }
            }
        }
    }
    else 
    {
        if(bHoldingJump[client]) 
        {
            bHoldingJump[client] = false;
        }
        bLastOnGround[client] = GetEntityFlags(client) & FL_ONGROUND;
    }

    if (g_bOnGround[client])
    {
        g_bBeam[client] = false;
        
        // JumpStats -- Landing
        if(!g_js_bInvalidGround[client] && !g_bLastInvalidGround[client] && g_js_bPlayerJumped[client] == true && weapon != -1 && IsValidEntity(weapon) && GetEntProp(client, Prop_Data, "m_nWaterLevel") < 1)
        {
            GetGroundOrigin(client, g_js_fJump_Landing_Pos[client]);
            g_fLandingTime[client] = GetEngineTime();
            g_fAirTime[client] = g_fLandingTime[client] - g_fJumpOffTime[client];

            if (!g_bKickStatus[client])
            {
                Postthink(client);
            }
        }	
        g_fLastPositionOnGround[client] = PlayerOrigin;
        g_bLastInvalidGround[client] = g_js_bInvalidGround[client];	
    }
    else
    {
        if (!g_js_bPlayerJumped[client])
        {
            g_js_GroundFrames[client] = 0;			
        }
    }		

    g_fLastAngles[client] = PlayerAngles;
	g_LastMoveType[client] = PlayerMoveType;
	g_fLastSpeed[client] = PlayerSpeed;
	g_fLastPosition[client] = PlayerOrigin;
	g_LastButton[client] = buttons;
	g_LastFlags[client] = GetEntityFlags(client);
	g_bLastOnGround[client] = g_bOnGround[client];

    return Plugin_Continue;
}

// Touch and Teleports (??)
public void Trigger_GravityTouch(char[] output, int bhop_block, int client, float delay)
{
	if (!(IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client])))
    {
		return;
    }

	ResetJump(client);	
    ResetJumpBug(client);
}

// Start touching teleport
public void Teleport_OnStartTouch(char[] output, int bhop_block, int client, float delay)
{
	if (!IsValidClient(client))
    {
		return;
    }

	// checkpoints on bhop plattforms allowed?
	if (g_bOnGround[client])
	{
		g_bOnBhopPlattform[client] = true;
	}
	
	// Jumpstats / Failstats: ljblock with teleport trigger
	if (g_js_block_lj_valid[client] && g_js_bPlayerJumped[client])
	{
		g_js_bPlayerJumped[client] = false;
		GetGroundOrigin(client, g_js_fJump_Landing_Pos[client]);
		g_fLandingTime[client] = GetEngineTime();
		g_fAirTime[client] = g_fLandingTime[client] - g_fJumpOffTime[client];
		Postthink(client);
		g_fLastPositionOnGround[client] = g_fLastPosition[client];
		g_bLastInvalidGround[client] = g_js_bInvalidGround[client];	
	}	

	// PrintToChat(client,"touched");
	g_fTeleportValidationTime[client] = GetEngineTime();
} 

// End touching teleport
public void Teleport_OnEndTouch(char[] output, int caller, int client, float delay)
{
	if (IsValidClient(client) && g_bOnBhopPlattform[client])
	{
		g_fTeleportValidationTime[client] = GetEngineTime();
		g_bOnBhopPlattform[client] = false;
		// g_fLastTimeBhopBlock[client] = GetEngineTime();
	}	

    ResetJumpBug(client);
}  

// Pushing touch (??)
public Action Push_Touch(int ent, int client)
{
	if (IsValidClient(client))
    {
        ResetJump(client);	
        ResetJumpBug(client);
    }

	return Plugin_Continue;
}

// Touching wall
public Action Touch_Wall(int ent, int client)
{
	if (IsValidClient(client))
	{
		if (GetEntityMoveType(client) != MOVETYPE_LADDER && !(GetEntityFlags(client)&FL_ONGROUND)  && g_js_bPlayerJumped[client])
		{
			float origin[3];
            float temp[3];

			GetGroundOrigin(client, origin);
			GetClientAbsOrigin(client, temp);

			if(temp[2] - origin[2] <= 0.2)
			{
				ResetJump(client);
                ResetJumpBug(client);
			}
		}
	}
	return Plugin_Continue;
}

// Standard touch
public void Hook_OnTouch(int client, int touched_ent)
{
	if (IsValidClient(client, true) || (IsValidClient(client) && g_bPlayerGhostMode[client]))
	{
		char classname[32];
		if (IsValidEdict(touched_ent))
        {
			GetEntityClassname(touched_ent, classname, 32);		
        }

		if (StrEqual(classname,"func_movelinear"))
		{
			g_js_bFuncMoveLinear[client] = true;
			return;
		}
        
		if (g_js_block_lj_valid[client])
        {
			return;
        }

		
		if (GetEntityMoveType(client) != MOVETYPE_LADDER && !(GetEntityFlags(client) & FL_ONGROUND) || touched_ent != 0)
        {
            ResetJump(client);	
            ResetJumpBug(client);
        }		
	}
}  

// Redie
public Action OnNormalSoundPlayed(int clients[64], int &numClients, char sample[PLATFORM_MAX_PATH], int &entity, int &channel, float &volume, int &level, int &pitch, int &flags)
{
	if(IsValidClient(entity) && g_bPlayerGhostMode[entity])
	{
		return Plugin_Handled;
	}
	return Plugin_Continue;
}

public Action OnTraceAttack(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &ammotype, int hitbox, int hitgroup)
{
	if(IsValidClient(victim))
	{
		if(g_bPlayerGhostMode[victim])
		{
			return Plugin_Handled;
		}
	}

	return Plugin_Continue;
}

public Action OnSetTransmit(int entity, int client)
{
	if(IsValidClient(entity) && g_bPlayerGhostMode[entity] && entity != client)
	{
		return Plugin_Handled;
	}

	return Plugin_Continue;
}

public Action OnWeaponCanUse(int client, int weapon)
{
	if(IsValidClient(client) && g_bPlayerGhostMode[client])
    {
		return Plugin_Handled;
    }

	return Plugin_Continue;
}


