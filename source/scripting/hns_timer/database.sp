void InitDatabase()
{
    // Connect to database
	PluginDatabase = ConnectToDatabase();
}

/* ======================================================= */
/* =================== Database ========================== */
/* ======================================================= */
Database ConnectToDatabase()
{
	char dbError[128];
	Database db;
	db = SQL_Connect("hns_timer", false, dbError, sizeof(dbError));
		
	if (db == null) PrintToServer("[HNS TIMER]: Could not connect to Database: %s", dbError);
		else PrintToServer("[HNS TIMER]: Database Connected. "); 		

	// PrintToServer("[IC FREE VIP]: Database connected. ");
	// SQL_FastQuery(conn, "CREATE TABLE IF NOT EXISTS `hns_records` (`steamid` VARCHAR(32) NOT NULL, `status` INT(11), PRIMARY KEY (`steamid`));");
	// PrintToServer("[IC FREE VIP]: Table creating if not exist. "); 	

	/*
    if (SQL_FastQuery(db, "SET NAMES \"UTF8\"")) PrintToServer("[POKEMOD]: Names changed to UTF-8.", dbError);
		else PrintToServer("[POKEMOD]: Could not change names."); 
    */

	return db;
}

/* ======================================================= */
/* =================== JumpStats ========================= */
/* ======================================================= */
void OpenStatsMenu(int client, char auth[32])
{
    if (!IsValidClient(client) || !strlen(auth)) return;

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET STATS]: Could not connect."); 
		return;
	}

    char error[256];
	char query[1024];

    DBResultSet hQuery;

	// GetClientName(target, nick, sizeof(nick));
	// GetClientAuthId(target, AuthId_Steam2, auth, sizeof(auth), true);

    /*
        "SELECT hr.TYPE, hr.BLOCK, hr.DISTANCE, hr.STRAFES, hr.PRE, hr.MAX, hr.HEIGHT, hr.SYNC, IF(hr.TYPE = 3, (SELECT COUNT(*) FROM hns_records WHERE BLOCK >= hr.BLOCK AND TYPE = hr.TYPE), (SELECT COUNT(*) FROM hns_records WHERE DISTANCE >= hr.DISTANCE AND TYPE = hr.TYPE)) as \"RANK\" FROM hns_records hr WHERE STEAM_ID = '%s';", 
        auth
    */

    Format
    (
        query, 
        sizeof(query), 
        "SELECT hp.NICKNAME, hr.TYPE, hr.BLOCK, hr.DISTANCE, hr.STRAFES, hr.PRE, hr.MAX, hr.HEIGHT, hr.SYNC FROM hns_records hr, hns_players hp WHERE hr.STEAM_ID = hp.STEAM_ID AND hr.STEAM_ID = '%s';", 
        auth
    );
	hQuery = SQL_Query(PluginDatabase, query);

	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][GET STATS]: Failed to query (error: %s)", error);
		PrintToServer("[QUERY]: %s", query);

		delete hQuery;
		return;
	}

    char nick[64];
    char MenuText[128];
    Menu menu = new Menu(mStatsMenu);

    while (SQL_FetchRow(hQuery))
	{
        SQL_FetchString(hQuery, 0, nick, sizeof(nick));
        
        int type        = SQL_FetchInt(hQuery, 1);
        int block       = SQL_FetchInt(hQuery, 2);
        float distance  = SQL_FetchFloat(hQuery, 3);
        int strafes     = SQL_FetchInt(hQuery, 4);
        float pre       = SQL_FetchFloat(hQuery, 5);
        float max       = SQL_FetchFloat(hQuery, 6);
        float height    = SQL_FetchFloat(hQuery, 7);
        float sync      = SQL_FetchFloat(hQuery, 8);

        switch (type)
        {
            case JUMPSTATS_LADDER:  { Format(MenuText, sizeof(MenuText), "%T         %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Ladderjump", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_LJ:      { Format(MenuText, sizeof(MenuText), "%T           %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Longjump", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }    
            case JUMPSTATS_BLOCKLJ: { Format(MenuText, sizeof(MenuText), "%T  %d [%d]    %d     %d   %.2f   %.1f   %d%%", "Stats_BlockLongjump", client, block, RoundFloat(distance), strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_MULTIBH: { Format(MenuText, sizeof(MenuText), "%T %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Multibhop", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_CJ:      { Format(MenuText, sizeof(MenuText), "%T           %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Countjump", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_WJ:      { Format(MenuText, sizeof(MenuText), "%T         %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Weirdjump", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_DROPBH:  { Format(MenuText, sizeof(MenuText), "%T  %.2f         %d     %d   %.2f   %.1f    %d%%", "Stats_DropBhop", client, distance, strafes, RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_BHOP:    { Format(MenuText, sizeof(MenuText), "%T          %.2f         %d     %d   %.2f   %.1f   %d%%", "Stats_Bhop", client, distance, strafes,RoundFloat(pre), max, height, RoundFloat(sync)); }
            case JUMPSTATS_JUMPBUG:    { Format(MenuText, sizeof(MenuText), "%T          %.2f", "Stats_JumpBug", client, distance); }
        }
        menu.AddItem("stats", MenuText);
    }

    if (!strlen(nick))
    {
        Format(nick, sizeof(nick), "UNKNOWN");
    }

    menu.SetTitle("%T:\n    %T       %T     %T    %T     %T    %T %T", "Stats_Title", client, nick, "Stats_Type", client, "Stats_Distance", client, "Stats_Strafes", client, "Stats_Pre", client, "Stats_Max", client, "Stats_Height", client, "Stats_Sync", client);

    menu.ExitBackButton = true;
    
    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);

	delete hQuery;
	return;
}

void OpenRankMenu(int client)
{
    if (!IsValidClient(client)) return;

    int CurrActivity = GetClientActivity(client);

    if (CurrActivity < 180)
    {
		PrintToChat(client, "%t", "Minimal_Hours",MOSSGREEN, WHITE, 180 - CurrActivity);
        return;
    }

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET RANK]: Could not connect."); 
		return;
	}

    char auth[32];
    char error[256];
	char query[1024];

    DBResultSet hQuery;
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

    Format
    (
        query, 
        sizeof(query), 
        "SELECT hr.TYPE, IF(hr.TYPE = 3, (SELECT COUNT(*) FROM hns_records WHERE BLOCK >= hr.BLOCK AND TYPE = hr.TYPE), (SELECT COUNT(*) FROM hns_records WHERE DISTANCE >= hr.DISTANCE AND TYPE = hr.TYPE)) as \"RANK\" FROM hns_records hr WHERE hr.STEAM_ID = '%s';", 
        auth
    );
	hQuery = SQL_Query(PluginDatabase, query);

	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][GET RANK]: Failed to query (error: %s)", error);
		PrintToServer("[QUERY]: %s", query);

		delete hQuery;
		return;
	}

    // Last menu info
    g_LastMenu[client] = MENU_RANK;

    char MenuText[128];
    Menu menu = new Menu(mRankMenu);
    menu.SetTitle("%T:\n    %T     %T", "Rank_Title", client, "Rank_Type", client, "Rank_Rank", client);

    while (SQL_FetchRow(hQuery))
	{
        int type       = SQL_FetchInt(hQuery, 0);
        int rank       = SQL_FetchInt(hQuery, 1);

        switch (type)
        {
            case JUMPSTATS_LADDER:  { Format(MenuText, sizeof(MenuText), "%T         %d", "Stats_Ladderjump", client, rank); }
            case JUMPSTATS_LJ:      { Format(MenuText, sizeof(MenuText), "%T           %d", "Stats_Longjump", client, rank); }    
            case JUMPSTATS_BLOCKLJ: { Format(MenuText, sizeof(MenuText), "%T  %d", "Stats_BlockLongjump", client, rank); }
            case JUMPSTATS_MULTIBH: { Format(MenuText, sizeof(MenuText), "%T %d", "Stats_Multibhop", client, rank); }
            case JUMPSTATS_CJ:      { Format(MenuText, sizeof(MenuText), "%T           %d", "Stats_Countjump", client, rank); }
            case JUMPSTATS_WJ:      { Format(MenuText, sizeof(MenuText), "%T         %d", "Stats_Weirdjump", client, rank); }
            case JUMPSTATS_DROPBH:  { Format(MenuText, sizeof(MenuText), "%T  %d", "Stats_DropBhop", client, rank); }
            case JUMPSTATS_BHOP:    { Format(MenuText, sizeof(MenuText), "%T          %d", "Stats_Bhop", client, rank); }            
            case JUMPSTATS_JUMPBUG: { Format(MenuText, sizeof(MenuText), "%T          %d", "Stats_JumpBug", client, rank); }
        }
        menu.AddItem("rank", MenuText);
    }

    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);

	delete hQuery;
	return;
}


int GetRank(int client, int type)
{
    if (!IsValidClient(client)) return -2;

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET RANK]: Could not connect."); 
		return -2;
	}

    char auth[32];
    char error[256];
	char query[1024];

    DBResultSet hQuery;
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

    Format
    (
        query, 
        sizeof(query), 
        "SELECT IF(hr.TYPE = 3, (SELECT COUNT(*) FROM hns_records WHERE BLOCK >= hr.BLOCK AND TYPE = hr.TYPE), (SELECT COUNT(*) FROM hns_records WHERE DISTANCE >= hr.DISTANCE AND TYPE = hr.TYPE)) as \"RANK\" FROM hns_records hr WHERE hr.STEAM_ID = '%s' and hr.TYPE = %d LIMIT 1;", 
        auth,
        type
    );
	hQuery = SQL_Query(PluginDatabase, query);

	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][GET RANK]: Failed to query (error: %s)", error);

		delete hQuery;
		return -1;
	}

    if (SQL_FetchRow(hQuery))
	{
        int Result = SQL_FetchInt(hQuery, 0);

        delete hQuery;
	    return Result;
    }

	delete hQuery;
	return 0;
}


void OpenTop20Menu(int client, int type)
{
    if (!IsValidClient(client) || !type) return;

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET RANK]: Could not connect."); 
		return;
	}
	
    char error[256];
	char query[256];
	DBResultSet hQuery;

    if (type == JUMPSTATS_BLOCKLJ)
    {
        Format
        (
            query, 
            sizeof(query), 
            "SELECT hp.STEAM_ID, hp.NICKNAME, hr.BLOCK, hr.DISTANCE, hr.STRAFES FROM hns_records hr, hns_players hp WHERE hp.STEAM_ID = hr.STEAM_ID AND hr.TYPE = %d ORDER BY hr.BLOCK DESC, hr.DISTANCE DESC LIMIT 20;", 
            type
        );
    }
    else
    {
        Format
        (
            query, 
            sizeof(query), 
            "SELECT hp.STEAM_ID, hp.NICKNAME, hr.BLOCK, hr.DISTANCE, hr.STRAFES FROM hns_records hr, hns_players hp WHERE hp.STEAM_ID = hr.STEAM_ID AND hr.TYPE = %d ORDER BY hr.DISTANCE DESC LIMIT 20;", 
            type
        );
    }

	hQuery = SQL_Query(PluginDatabase, query);
	
	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][GET TOP 20]: Failed to query (error: %s)", error);
		
		delete hQuery;
		return;
	}

    // Last menu info
    g_LastMenu[client] = MENU_TOP;
    
    char MenuText[128];
    Menu menu = new Menu(mTopCategoryMenu);

    switch (type)
    {
        case JUMPSTATS_LJ:      { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Longjump", client,       "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_BLOCKLJ: { menu.SetTitle("%T:\n    %T    %T   %T           %T      %T",       "Top20_BlockLongjump", client,    "Top20_Rank", client, "Top20_Block", client,       "Top20_Distance", client,   "Top20_Strafes", client,    "Top20_Player", client);  }
        case JUMPSTATS_MULTIBH: { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Multibhop", client,       "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_CJ:      { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Countjump", client,      "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_WJ:      { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Weirdjump", client,      "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_DROPBH:  { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_DropBhop", client,       "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_BHOP:    { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Bhop", client,           "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    } 
        case JUMPSTATS_LADDER:  { menu.SetTitle("%T:\n    %T    %T           %T      %T",            "Top20_Ladderjump", client,      "Top20_Rank", client, "Top20_Distance", client,    "Top20_Strafes", client,    "Top20_Player", client);    }
        case JUMPSTATS_JUMPBUG:  { menu.SetTitle("%T:\n    %T    %T                 %T",                  "Top20_JumpBug", client,        "Top20_Rank", client,   "Top20_Distance", client,   "Top20_Player", client); }
        default:                { menu.SetTitle("Unknown TOP 20");  }
    }

    int index = 1;
    while (SQL_FetchRow(hQuery))
	{
        char auth[32];
        char nickname[MAX_NAME];

        SQL_FetchString(hQuery, 0, auth, sizeof(auth));
        SQL_FetchString(hQuery, 1, nickname, sizeof(nickname));

        int block       = SQL_FetchInt(hQuery, 2);
        float distance  = SQL_FetchFloat(hQuery, 3);
        int strafes     = SQL_FetchInt(hQuery, 4);

        switch (type)
        {
            case JUMPSTATS_LJ:      { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_BLOCKLJ: { Format(MenuText, sizeof(MenuText), "[%s%i.]    %i     %.3f %T       %d      » %s", index < 10 ? "0":"", index, block, distance, "Top20_Units", client, strafes, nickname);  }
            case JUMPSTATS_MULTIBH: { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_CJ:      { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_WJ:      { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_DROPBH:  { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_BHOP:    { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); } 
            case JUMPSTATS_LADDER:  { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      %d      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, strafes, nickname); }
            case JUMPSTATS_JUMPBUG: { Format(MenuText, sizeof(MenuText), "[%s%i.]    %.3f %T      » %s", index < 10 ? "0":"", index, distance, "Top20_Units", client, nickname); }
        
        }
        menu.AddItem(auth, MenuText);
        index++;
    }

    menu.ExitBackButton = true;
    menu.ExitButton = true;
    menu.Display(client, MENU_TIME_FOREVER);

	delete hQuery;
	return;
}

int GetClientRecordsAmount(int client)
{
    if (PluginDatabase == null)	
    {
        PrintToServer("[HNS TIMER][GET RECORDS AMOUNT]: Could not connect."); 
        return 0;
    }
    
    char auth[32];
    char nick[64];
    char error[256];
    char query[1024];

    GetClientName(client, nick, sizeof(nick));
    GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

    Format
    (
        query, 
        sizeof(query), 
        "SELECT COUNT(*) FROM (SELECT IF(hr.TYPE = 3, (SELECT COUNT(*) FROM hns_records WHERE BLOCK >= hr.BLOCK AND TYPE = hr.TYPE), (SELECT COUNT(*) FROM hns_records WHERE DISTANCE >= hr.DISTANCE AND TYPE = hr.TYPE)) as \"RANK\" FROM hns_records hr WHERE hr.STEAM_ID = '%s' HAVING RANK = 1) as t; ", 
        auth
    );
    
    DBResultSet hQuery = SQL_Query(PluginDatabase, query);
    if (hQuery == null)
    {
        SQL_GetError(PluginDatabase, error, sizeof(error));
        PrintToServer("[HNS TIMER][GET RECORDS AMOUNT]: Failed to query (error: %s)(%s)[%s]", error, nick, auth);

        delete hQuery;
        return 0;
    }

    if (SQL_FetchRow(hQuery))
    {
        int Result = SQL_FetchInt(hQuery, 0);

        delete hQuery;
        return Result;
    }

    delete hQuery;
    return 0;
}

int GetClientActivity(int client)
{
	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET ACTIVITY]: Could not connect."); 
		return -1;
	}
	
	char auth[32];
	char nick[64];
	char error[256];
	char query[256];

	DBResultSet hQuery;

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

	Format
	(
		query, 
		sizeof(query), 
		"SELECT VALUE FROM hns_activity WHERE STEAM_ID LIKE '%s';", 
		auth
	);
	hQuery = SQL_Query(PluginDatabase, query);
	
	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][GET ACTIVITY]: Failed to query (error: %s)(%s)[%s]", error, nick, auth);
		
		delete hQuery;
		return -1;
	}

	if (SQL_FetchRow(hQuery))
	{
        int Result = SQL_FetchInt(hQuery, 0); 

        delete hQuery;
        return Result;
	}
	
	delete hQuery;
	return 0;
}

void SetClientActivity(int client, int value)
{
    if (!IsValidClient(client))
    {
        return;
    }

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][GET ACTIVITY]: Could not connect."); 
		return;
	}

	char auth[32];
	char query[512];

	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
	
	Format
	(
		query,
		sizeof(query), 
		"INSERT INTO hns_activity VALUES ('%s', %d) ON DUPLICATE KEY UPDATE VALUE = %d;",
		auth,  // steamid
		value,
        // ----------- UPDATE --------- //
		value
	);
    SQL_FastQuery(PluginDatabase, query);	

}

void LoadClientSettings(int client)
{
	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][PLAYER SETTINGS LOADING]: Could not connect."); 
		return;
	}
	
	char auth[32];
	char nick[64];
	char error[256];
	char query[256];

	DBResultSet hQuery;

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

	Format
	(
		query, 
		sizeof(query), 
		"SELECT JUMP_BEAM, COLOR_CHAT, QUAKE_SOUNDS, CENTER_PANEL, SPEC_LIST FROM hns_settings WHERE STEAM_ID LIKE '%s';", 
		auth
	);
	hQuery = SQL_Query(PluginDatabase, query);
	
	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][PLAYER LOADING]: Failed to query (error: %s)(%s)[%s]", error, nick, auth);
		
		delete hQuery;
		return;
	}

	if (SQL_FetchRow(hQuery))
	{
        g_bJumpBeam[client] = SQL_FetchInt(hQuery, 0) ? true:false; 
        g_ColorChat[client] = SQL_FetchInt(hQuery, 1); 
        g_EnableQuakeSounds[client] = SQL_FetchInt(hQuery, 2);
        g_bCenterPanel[client] = SQL_FetchInt(hQuery, 3);
        g_bEnableSpecList[client] = SQL_FetchInt(hQuery, 4);
	}

	PrintToServer("[HNS TIMER][PLAYER SETTINGS LOADING]: Player %s(%s) has been loaded.", nick, auth);
	
	delete hQuery;
	return;
}

void SaveClientSettings(int client)
{
    if (!IsValidClient(client))
    {
        return;
    }

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][PLAYER SETTINGS SAVING]: Could not connect."); 
		return;
	}

	char auth[32];
	char query[512];

	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
	
	Format
	(
		query,
		sizeof(query), 
		"INSERT INTO hns_settings VALUES ('%s', %d, %d, %d, %d, %d, %d) ON DUPLICATE KEY UPDATE JUMP_BEAM = %d, COLOR_CHAT = %d, QUAKE_SOUNDS = %d, CHAT_SYNC = %d, CENTER_PANEL = %d, SPEC_LIST = %d;",
		auth,                           // steamid
		g_bJumpBeam[client],            // beam
        g_ColorChat[client],            // color
        g_EnableQuakeSounds[client],    // quake
    	g_bStrafeSync[client],          // sync
        g_bCenterPanel[client],         // center panel
        g_bEnableSpecList[client],              // spec list
        // ----------- UPDATE --------- //
		g_bJumpBeam[client],            // beam
        g_ColorChat[client],            // color
        g_EnableQuakeSounds[client],    // quake
    	g_bStrafeSync[client],            // sync
        g_bCenterPanel[client],         // center panel
        g_bEnableSpecList[client]       // spec list
	);
    SQL_FastQuery(PluginDatabase, query);	
}

void LoadClientRecords(int client)
{
	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][PLAYER RECORDS LOADING]: Could not connect."); 
		return;
	}
	
	char auth[32];
	char nick[64];
	char error[256];
	char query[256];

	DBResultSet hQuery;

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);

	Format
	(
		query, 
		sizeof(query), 
		"SELECT TYPE, BLOCK, DISTANCE FROM hns_records WHERE STEAM_ID LIKE '%s';", 
		auth
	);
	hQuery = SQL_Query(PluginDatabase, query);
	
	if (hQuery == null)
	{
		SQL_GetError(PluginDatabase, error, sizeof(error));
		PrintToServer("[HNS TIMER][PLAYER RECORDS LOADING]: Failed to query (error: %s)(%s)[%s]", error, nick, auth);
		
		delete hQuery;
		return;
	}

	while (SQL_FetchRow(hQuery))
	{
        int type = SQL_FetchInt(hQuery, 0);
        int block = SQL_FetchInt(hQuery, 1); 
        float distance = SQL_FetchFloat(hQuery, 2);

        switch (type)
        {
            case JUMPSTATS_LADDER:  { g_js_fPersonal_LadderJump_Record[client] = distance; }
            case JUMPSTATS_LJ:      { g_js_fPersonal_Lj_Record[client] = distance; }
            case JUMPSTATS_BLOCKLJ: 
            {  
                g_js_Personal_LjBlock_Record[client] = block;
                g_js_fPersonal_LjBlockRecord_Dist[client] = distance; 
            }
            case JUMPSTATS_MULTIBH: { g_js_fPersonal_MultiBhop_Record[client] = distance; }
            case JUMPSTATS_CJ:      { g_js_fPersonal_CJ_Record[client] = distance; }
            case JUMPSTATS_WJ:      { g_js_fPersonal_Wj_Record[client] = distance; }
            case JUMPSTATS_DROPBH:  { g_js_fPersonal_DropBhop_Record[client] = distance; }
            case JUMPSTATS_BHOP:    { g_js_fPersonal_Bhop_Record[client] = distance; } 
            case JUMPSTATS_JUMPBUG: { g_js_fPersonal_JumpBug_Record[client] = distance; } 
        }
	}

	PrintToServer("[HNS TIMER][PLAYER RECORDS LOADING]: Player %s(%s) has been loaded.", nick, auth);
	
	delete hQuery;
	return;
}

void SaveClientRecordType(int client, int type)
{
    if (!IsValidClient(client, true) || !type)
    {
        return;
    }

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][PLAYER RECORD SAVING]: Could not connect."); 
		return;
	}

	char auth[32];
	char nick[64];
	char query[512];

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
	
	StringFilter(nick);

    float distance = -1.0;
    int block = 0;
    switch(type)
    {
        case JUMPSTATS_LADDER:  { distance = g_js_fPersonal_LadderJump_Record[client]; }
        case JUMPSTATS_LJ:      { distance = g_js_fPersonal_Lj_Record[client]; }
        case JUMPSTATS_BLOCKLJ: 
        {  
            distance = g_js_fPersonal_LjBlockRecord_Dist[client]; 
            block = g_js_Personal_LjBlock_Record[client]; 
        }
        case JUMPSTATS_MULTIBH: { distance = g_js_fPersonal_MultiBhop_Record[client]; }
        case JUMPSTATS_CJ:      { distance = g_js_fPersonal_CJ_Record[client]; }
        case JUMPSTATS_WJ:      { distance = g_js_fPersonal_Wj_Record[client]; }
        case JUMPSTATS_DROPBH:  { distance = g_js_fPersonal_DropBhop_Record[client]; }
        case JUMPSTATS_BHOP:    { distance = g_js_fPersonal_Bhop_Record[client]; }
        case JUMPSTATS_JUMPBUG:    { distance = g_js_fPersonal_JumpBug_Record[client]; }
    }
    
    if (distance < 0.0 || block < 0) return;

	Format
	(
		query,
		sizeof(query), 
		"INSERT INTO hns_records VALUES ('%s', %d, %d, %f, %d, %f, %f, %f, %d) ON DUPLICATE KEY UPDATE TYPE = %d, BLOCK = %d, DISTANCE = %f, STRAFES = %d, PRE = %f, MAX = %f, HEIGHT = %f, SYNC = %d;",
		auth,                           // steamid
        type,                           // type
        block,                          // block
        distance,                       // distance
        g_js_Strafes_Final[client],     // strafes
        g_js_fPreStrafe[client],        // pre
        g_js_fMax_Speed_Final[client],  // max
        g_flastHeight[client],          // height
        g_js_Sync_Final[client],        // sync
        // ----------- UPDATE --------- //
        type,                           // type
        block,                          // block
        distance,                       // distance
        g_js_Strafes_Final[client],     // strafes
        g_js_fPreStrafe[client],        // pre
        g_js_fMax_Speed_Final[client],  // max
        g_flastHeight[client],          // height
        g_js_Sync_Final[client]         // sync
	);
    SQL_FastQuery(PluginDatabase, query);	

}

void UpdatePlayerNickname(int client)
{
    if (!IsValidClient(client))
    {
        return;
    }

	if (PluginDatabase == null)	
	{
		PrintToServer("[HNS TIMER][PLAYER NICKNAME UPDATING]: Could not connect."); 
		return;
	}

	char auth[32];
    char nick[64];
	char query[512];

	GetClientName(client, nick, sizeof(nick));
	GetClientAuthId(client, AuthId_Steam2, auth, sizeof(auth), true);
	
    StringFilter(nick);

	Format
	(
		query,
		sizeof(query), 
		"INSERT INTO hns_players VALUES ('%s', '%s') ON DUPLICATE KEY UPDATE NICKNAME = '%s';",
		auth,   // steamid
        nick,   // nickname
        nick    // nickname
	);
    SQL_FastQuery(PluginDatabase, query);	
}


stock void StringFilter(char[] text, int filter_type = 1)
{
	if (filter_type == 1)
	{		
		int charInAscii = 0;
		int i = 0;
	
		while (text[i] != '\0')
		{
			charInAscii = text[i];
            
            if 
            (
                (charInAscii < 46) ||
                (charInAscii > 122) ||
                (charInAscii == 47) ||
                (charInAscii > 57 && charInAscii < 64) ||
                (charInAscii > 90 && charInAscii < 97)
            )
            {
                text[i] = ' ';
            }
            
			i++;
		}
	}
}